#pragma once

template<typename KEY, typename VALUE>
class CSparse : public CMap<KEY, KEY, VALUE, VALUE&> {
protected:
	LONG m_nLast;
	CList<LONG> m_nSpare;
	CMutex m_Mutex;

public:
	friend CSparse<KEY, VALUE>  operator+(const CSparse<KEY, VALUE>& current, const VALUE & item);

	CSparse() : m_Mutex(FALSE, NULL, NULL) {}

	BOOL Lookup(KEY key, VALUE& rValue) 
	{
		m_Mutex.Lock();
		BOOL ret = CMap::Lookup(key, rValue);
		m_Mutex.Unlock();
		return ret;
	}

	KEY Append(VALUE item)
	{
		KEY key;
		m_Mutex.Lock();
		if (m_nSpare.IsEmpty()) key = ++m_nLast;
		else key = m_nSpare.RemoveHead();
		CMap::SetAt(key, item);
		m_Mutex.Unlock();
		return key;
	}

	CSparse & operator+=(const VALUE & item)
	{
		KEY key;
		m_Mutex.Lock();
		if (m_nSpare.IsEmpty()) key = ++m_nLast;
		else key = m_nSpare.RemoveHead();
		CMap::operator[](key) = item;
		m_Mutex.Unlock();
		return *this;
	}

	VALUE & operator[](KEY key)
	{
		m_Mutex.Lock();
		POSITION pos = m_nSpare.Find(key);
		if (pos) m_nSpare.RemoveAt(pos);
		VALUE val = CMap::[key];
		m_Mutex.Unlock();
		return val;
	}

	void SetAt(KEY key, VALUE item)
	{
		m_Mutex.Lock();
		POSITION pos = m_nSpare.Find(key);
		if (pos) m_nSpare.RemoveAt(pos);
		CMap::SetAt(key, item);
		m_Mutex.Unlock();
	}
	
	VALUE operator[](KEY key) const
	{
		m_Mutex.Lock();
		VALUE val = CMap::[key];
		m_Mutex.Unlock();
		return VALUE;
	}
	
	BOOL RemoveKey(KEY key)
	{
		m_Mutex.Lock();
		BOOL ret = CMap::RemoveKey(key);
		m_nSpare.AddTail(key);
		m_Mutex.Unlock();
		return ret;
	}
};

template <typename KEY, typename VALUE> 
CSparse<KEY, VALUE> operator+(const CSparse<KEY, VALUE>& current, const VALUE & item)
{
	current += item;
	return current;
}
