
// XTGUIDoc.cpp : implementation of the CXTGUIDoc class
//

#include "stdafx.h"
#include "XTGUI.h"
#include "XTGUIDoc.h"
#include "XTAPI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CXTGUIDoc

IMPLEMENT_DYNCREATE(CXTGUIDoc, CDocument)

BEGIN_MESSAGE_MAP(CXTGUIDoc, CDocument)
END_MESSAGE_MAP()

// CXTGUIDoc construction/destruction

BOOL CXTGUIDoc::OnNewDocument()
{
	return CDocument::OnNewDocument();
}

// CXTGUIDoc serialization

CSparse<LONG, CXTGUIDoc::Item> CXTGUIDoc::m_items;

const wchar_t* CXTGUIDoc::GetName(LONG nItem)
{
	Item item;
	if(m_items.Lookup(nItem,item)) return item.name;
	else return NULL;
}

const wchar_t* CXTGUIDoc::GetType(LONG nItem)
{
	Item item;
	if (m_items.Lookup(nItem, item)) return item.type;
	else return NULL;
}

void CXTGUIDoc::PostLogItem(const wchar_t * message, DWORD nFlags)
{
	if (nFlags & 0x04) theApp.m_wndOutput->AddString(CString((char*)message));
	else theApp.m_wndOutput->AddString(message);
	theApp.m_wndOutput->SetTopIndex(theApp.m_wndOutput->GetCount() - 1);
}

void CXTGUIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())  {
		ar.Write(m_strHtml,m_strHtml.GetLength());
	}
	else  {

		XWF_Init(GetName, GetType, PostLogItem);

		HMODULE Hdl;
		fptr_XT_Init XT_Init;
		fptr_XT_View XT_View;
		fptr_XT_ReleaseMem XT_ReleaseMem;
		fptr_XT_Done XT_Done;

		Hdl = LoadLibraryW(theApp.m_strDLL);
		if (!Hdl) {
			PostLogItem(_T("Error: failed to load ")+ theApp.m_strDLL);
			return;
		}

		XT_Init = (fptr_XT_Init)GetProcAddress(Hdl, "XT_Init");
		XT_View = (fptr_XT_View)GetProcAddress(Hdl, "XT_View");
		XT_ReleaseMem = (fptr_XT_ReleaseMem)GetProcAddress(Hdl, "XT_ReleaseMem");
		if (!XT_Init || !XT_View || !XT_ReleaseMem) {
			PostLogItem(_T("Error: at least one required export (either XT_Init, XT_View or XT_ReleaseMem) was not found in ") + theApp.m_strDLL);
			FreeLibrary(Hdl);
			return;
		}

		LONG lret;
		INT64 size;
		LicenseInfo licence = { sizeof(LicenseInfo),0x31,1,{ 0,0 },{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 } };

		lret = XT_Init(18, XT_INIT_XWF, NULL, &licence);
		if (lret != 1) {
			PostLogItem(_T("Error: XT_Init did not return 1"));
			FreeLibrary(Hdl);
			return;
		}

		CFile *file = ar.GetFile();
		Item item;
		item.name = file->GetFilePath();
		item.type = theApp.m_strFileType;
		LONG id = m_items.Append(item);
		char * buffer = (char*)XT_View(*file, id, 0, 0, NULL, &size);
		m_items.RemoveKey(id);

		if (!buffer && size > 0)  {
			PostLogItem(_T("Error: XT_View returned a null pointer but size > 0"));
			FreeLibrary(Hdl);
			return;
		}

		m_strHtml = buffer;

		XT_ReleaseMem(buffer);

		XT_Done = (fptr_XT_Done)GetProcAddress(Hdl, "XT_Done");
		if (XT_Done) {
			lret = XT_Done(NULL);
			if (lret) PostLogItem(_T("Warning: XT_Done did not return 0"));
		}
		FreeLibrary(Hdl);
	}
}

// CXTGUIDoc diagnostics

#ifdef _DEBUG
void CXTGUIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CXTGUIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CXTGUIDoc commands
