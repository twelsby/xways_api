#pragma once

// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file XTAPI.h Fake X-Ways implementation header file

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define XT_API extern "C" __declspec(dllexport)

#pragma pack(push,2)

struct SearchInfo {
	LONG iSize;
	HANDLE hVolume;
	LPWSTR lpSearchTerms;
	DWORD nFlags;
	DWORD nSearchWindow;
};

struct CodePages {
	LONG iSize;
	WORD nCodePage1;
	WORD nCodePage2;
	WORD nCodePage3;
	WORD nCodePage4;
	WORD nCodePage5;
};

struct EventInfo {
	LONG iSize;
	HANDLE hEvidence;
	DWORD nEvtType;
	DWORD nFlags;
	FILETIME TimeStamp;
	LONG nItemID;
	INT64 nOfs;
	LPSTR lpDescr;
};

struct CallerInfo {
	BYTE lang, ServiceRelease;
	WORD version;
};

struct PrepareSearchInfo {
	LONG iSize;
	LPWSTR lpSearchTerms;
	DWORD nBufLen;
	DWORD nFlags;
};

struct LicenseInfo {
	DWORD nSize;
	DWORD nLicFlags;
	DWORD nUsers;
	FILETIME nExpDate;
	BYTE nLicID[16];
};

#pragma pack(pop)

#define XT_INIT_XWF 0x00000001 // X-Ways Forensics
#define XT_INIT_WHX 0x00000002 // WinHex
#define XT_INIT_XWI 0x00000004 // X-Ways Investigator
#define XT_INIT_BETA 0x00000008 // beta version
#define XT_INIT_QUICKCHECK 0x00000020 // called just to check whether the API accepts the calling application (used by v16.5 and later)
#define XT_INIT_ABOUTONLY 0x00000040 // called just to prepare for XT_About (used by v16.5 and later)

#define XT_ACTION_RUN 0 // simply run directly from the main menu, not for any particular volume, since v16.6
#define XT_ACTION_RVS 1 // volume snapshot refinement starting
#define XT_ACTION_LSS 2 // logical simultaneous search starting
#define XT_ACTION_PSS 3 // physical simultaneous search starting
#define XT_ACTION_DBC 4 // directory browser context menu command invoked
#define XT_ACTION_SHC 5 // search hit context menu command invoked

#define XT_PREPARE_CALLPI 0x01 // call for each item in the volume snapshot
#define XT_PREPARE_CALLPILATE 0x02 // call for each item in the volume snapshot after other refinement operations
#define XT_PREPARE_EXPECTMOREITEMS 0x04 // signal that more items may be added to the volume snapshot
#define XT_PREPARE_DONTOMIT 0x08 // don't omit files that the user wants to omit
#define XT_PREPARE_TARGETDIRS 0x10 // target directories not just files

#define XWF_ITEM_INFO_ORIG_ID 1
#define XWF_ITEM_INFO_ATTR 2
#define XWF_ITEM_INFO_FLAGS 3
#define XWF_ITEM_INFO_DELETION 4
#define XWF_ITEM_INFO_CLASSIFICATION 5 // e.g. extracted e-mail message, alternate data stream, etc.
#define XWF_ITEM_INFO_LINKCOUNT 6 // hard-link count
#define XWF_ITEM_INFO_FILECOUNT 11 // how many child objects exist recursively that are files
#define XWF_ITEM_INFO_CREATIONTIME 32
#define XWF_ITEM_INFO_MODIFICATIONTIME 33
#define XWF_ITEM_INFO_LASTACCESSTIME 34
#define XWF_ITEM_INFO_ENTRYMODIFICATIONTIME 35
#define XWF_ITEM_INFO_DELETIONTIME 36
#define XWF_ITEM_INFO_INTERNALCREATIONTIME 37
#define XWF_ITEM_INFO_FLAGS_SET 64 // indicates only flags that should be set, others remain unchanged
#define XWF_ITEM_INFO_FLAGS_REMOVE 65 // indicates flags that should be removed, others remain unchanged

#define XWF_SEARCH_LOGICAL 0x00000001 // logical search instead of physical search (only logical search currently available)
#define XWF_SEARCH_TAGGEDOBJ 0x00000004 // tagged objects in volume snapshot only
#define XWF_SEARCH_MATCHCASE 0x00000010 // match case
#define XWF_SEARCH_WHOLEWORDS 0x00000020 // whole words only
#define XWF_SEARCH_GREP 0x00000040 // GREP syntax
#define XWF_SEARCH_OVERLAPPED 0x00000080 // allow overlapping hits
#define XWF_SEARCH_COVERSLACK 0x00000100 // cover slack space
#define XWF_SEARCH_COVERSLACKEX 0x00000200 // cover slack/free space transition
#define XWF_SEARCH_DECODETEXT 0x00000400 // decode text in standard file types
#define XWF_SEARCH_DECODETEXTEX 0x00000800 // decode text in specified file types // not yet supported 
#define XWF_SEARCH_1HITPERFILE 0x00001000 // 1 hit per file needed only
#define XWF_SEARCH_OMITIRRELEVANT 0x00010000 // omit files classified as irrelevant
#define XWF_SEARCH_OMITHIDDEN 0x00020000 // omit hidden files
#define XWF_SEARCH_OMITFILTERED 0x00040000 // omit files that are filtered out
#define XWF_SEARCH_DATAREDUCTION 0x00080000 // recommendable data reduction
#define XWF_SEARCH_OMITDIRS 0x00100000 // omit directories
#define XWF_SEARCH_CALLPSH 0x01000000 // see below
#define XWF_SEARCH_DISPLAYHITS 0x04000000 // display search hit list when the search completes

#define XWF_CTR_OPEN 0x00000001 // opens an existing container, all other flags ignored
#define XWF_CTR_XWFS2 0x00000002 // use new XWFS2 file system
#define XWF_CTR_SECURE 0x00000004 // mark this container as to be filled indirectly/secure
#define XWF_CTR_TOPLEVEL 0x00000008 // include evidence object names as top directory level
#define XWF_CTR_INCLDIRDATA 0x00000010 // include directory data
#define XWF_CTR_FILEPARENTS 0x00000020 // allow files as parents of files
#define XWF_CTR_USERREPORTTABLES 0x00000100 // export associations with user-created report table
#define XWF_CTR_SYSTEMREPORTTABLES 0x00000200 // export associations with system-created report tables (currently requires 0x100)
#define XWF_CTR_ALLCOMMENTS 0x00000800 // pass on comments
#define XWF_CTR_OPTIMIZE1 0x00001000 // optimize for > 1,000 items
#define XWF_CTR_OPTIMIZE2 0x00002000 // optimize for > 50,000 items
#define XWF_CTR_OPTIMIZE3 0x00004000 // optimize for > 250,000 items
#define XWF_CTR_OPTIMIZE4 0x00008000 // optimize for > 1 million items

#define XWF_CASEPROP_TITLE 0x01
#define XWF_CASEPROP_EXAMINER 0x03
#define XWF_CASEPROP_FILE 0x05
#define XWF_CASEPROP_DIR 0x06

typedef LONG(__stdcall *fptr_XT_Init)(DWORD nVersion, DWORD nFlags, HANDLE hMainWnd, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_Done)(void* lpReserved);
typedef LONG(__stdcall *fptr_XT_About)(HANDLE hParentWnd, void* lpReserved);
typedef PVOID(__stdcall *fptr_XT_View)(HANDLE hItem, LONG nItemID, HANDLE hVolume, HANDLE hEvidence, PVOID lpReserved, PINT64 nResSize);
typedef BOOL(__stdcall *fptr_XT_ReleaseMem)(PVOID lpBuffer);
typedef LONG(__stdcall *fptr_XT_Prepare)(HANDLE hVolume, HANDLE hEvidence, DWORD nOpType, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_Finalize)(HANDLE hVolume, HANDLE hEvidence, DWORD nOpType, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_ProcessItemEx)(LONG nItemID, HANDLE hItem, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_ProcessItem)(LONG nItemID, void* lpReserved);

void XWF_Init(const wchar_t *(*name)(LONG), const wchar_t *(*type)(LONG), void(*log)(const wchar_t *, DWORD));
