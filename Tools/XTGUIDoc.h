
// XTGUIDoc.h : interface of the CXTGUIDoc class
//


#pragma once

#include "Sparse.h"

class CXTGUIDoc : public CDocument
{
protected: // create from serialization only
	DECLARE_DYNCREATE(CXTGUIDoc)

// Attributes
public:
	CStringA m_strHtml;

protected:
	struct Item {
		CString name, type;
	};
	static CSparse<LONG, Item> m_items;

// Operations
protected:
	static const wchar_t* GetName(LONG nItem);
	static const wchar_t* GetType(LONG nItem);
	static void PostLogItem(const wchar_t * message, DWORD nFlags = 0);

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};
