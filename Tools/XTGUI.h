
// XTGUI.h : main header file for the XTGUI application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "OutputWnd.h"
#include "resource.h"       // main symbols


// CXTViewerMApp:
// See XTViewerM.cpp for the implementation of this class
//

class CXTGUIApp : public CWinAppEx
{
public:
	CXTGUIApp();

	CString m_strDLL;
	CString m_strFileType;
	COutputList *m_wndOutput = NULL;

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();
	virtual void OnFileOpen();
	virtual void OnOpenRecentFile(UINT nID);

	afx_msg void OnAppAbout();
	afx_msg void OnXTDLL();
	afx_msg void OnXTFileType();
	DECLARE_MESSAGE_MAP()
};

extern CXTGUIApp theApp;
