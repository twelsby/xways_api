
// XTGUIView.cpp : implementation of the CXTGUIView class
//

#include "stdafx.h"
#include "XTGUI.h"
#include "XTGUIDoc.h"
#include "XTGUIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CXTGUIView

IMPLEMENT_DYNCREATE(CXTGUIView, CHtmlView)

BEGIN_MESSAGE_MAP(CXTGUIView, CHtmlView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, OnFilePrintDirect)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_COMMAND(ID_FILE_PRINT_SETUP, OnFilePrintSetup)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
END_MESSAGE_MAP()

// CXTGUIView construction/destruction

CXTGUIView::CXTGUIView()
{
}

CXTGUIView::~CXTGUIView()
{
}

BOOL CXTGUIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CHtmlView::PreCreateWindow(cs);
}

// CXTGUIView printing

BOOL CXTGUIView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CXTGUIView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CXTGUIView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CXTGUIView::OnFilePrintSetup()
{
	//CHtmlView::OnFilePrintSetup();
	ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER, NULL, NULL);
}

void CXTGUIView::OnFilePageSetup()
{
	//CHtmlView::OnFilePageSetup();
	ExecWB(OLECMDID_PAGESETUP, OLECMDEXECOPT_PROMPTUSER, NULL, NULL);
}

void CXTGUIView::OnFilePrint()
{
	//CHtmlView::OnFilePrint();
	ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT, NULL, NULL);
}

void CXTGUIView::OnFilePrintDirect()
{
	//CHtmlView::OnFilePrintDirect();
	ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER, NULL, NULL);
}

void CXTGUIView::OnFilePrintPreview()
{
//	AFXPrintPreview(this);
	ExecWB(OLECMDID_PRINTPREVIEW, OLECMDEXECOPT_PROMPTUSER, NULL, NULL);
//	CHtmlView::OnFilePrintPreview();
}

void CXTGUIView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	m_bDirty = true;
	Navigate(_T("about:blank"));
}

void CXTGUIView::OnDocumentComplete(LPCTSTR lpszURL)
{
	CHtmlView::OnDocumentComplete(lpszURL);

	if (!m_bDirty) return;  // Ensure this is an unserviced OnUpdate() 
	m_bDirty = false;

	CXTGUIDoc *pDoc = GetDocument();
	if (!pDoc) return;

	int nSize = pDoc->m_strHtml.GetLength();
	if (!nSize) return;

	pDoc->SetPathName(CString(pDoc->GetPathName()) + _T(".htm"),false);

	IPersistStreamInit* pPersistStreamInit = NULL;
	HRESULT hr;
	HGLOBAL hMemPage = GlobalAlloc(GPTR, nSize + 1);
	LPSTR   pMemPage = (LPSTR)GlobalLock(hMemPage);
	memcpy(pMemPage, pDoc->m_strHtml, nSize + 1);
	GlobalUnlock(hMemPage);

	LPSTREAM pStm;
	hr = CreateStreamOnHGlobal(hMemPage, TRUE, &pStm);
	if (SUCCEEDED(hr)) {
		LPDISPATCH pHtmlDoc = GetHtmlDocument();
		if (pHtmlDoc) {
			hr = pHtmlDoc->QueryInterface(IID_IPersistStreamInit, (void**)&pPersistStreamInit);
			hr = pPersistStreamInit->InitNew();  // Initialize it with the storage we created 
			hr = pPersistStreamInit->Load(pStm);
		}
	}

	if (pPersistStreamInit) {
		pPersistStreamInit->Release();
	}

	GlobalFree(hMemPage);
}

void CXTGUIView::OnDestroy()
{
	CHtmlView::OnDestroy();
	CView::OnDestroy();		// Fixes CHtmlView bug
}

// CXTGUIView diagnostics

#ifdef _DEBUG
void CXTGUIView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CXTGUIView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CXTGUIDoc* CXTGUIView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXTGUIDoc)));
	return (CXTGUIDoc*)m_pDocument;
}
#endif //_DEBUG

// CXTGUIView message handlers
