// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file XTAPI.cpp Fake X-Ways implementation source file

#include "XTAPI.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

LONG lastItem = 0;

const wchar_t *(*GetName)(LONG);
const wchar_t *(*GetType)(LONG);
void(*PostLogItem)(const wchar_t * message, DWORD nFlags);

inline bool verify(const char * message, bool condition);

void XWF_Init(const wchar_t *(*name)(LONG), const wchar_t *(*type)(LONG), void(*log)(const wchar_t *, DWORD))
{
	GetName = name;
	GetType = type;
	PostLogItem = log;
}

XT_API DWORD XWF_Read(HANDLE hVolumeOrItem, INT64 Offset, BYTE* lpBuffer, DWORD nNumberOfBytesToRead)
{
	if(!verify("Error: XWF_Read called with NULL lpBuffer", lpBuffer != NULL)) return 0;

	DWORD nRead, nTotalRead = 0;

	LARGE_INTEGER pos;
	pos.QuadPart = Offset;
	
	verify("Warning: SetFilePointerEx failed (in XWF_Read)", SetFilePointerEx(hVolumeOrItem, pos, NULL, FILE_BEGIN) == TRUE);

	while (nNumberOfBytesToRead) {
		if (!verify("Warning: ReadFile failed (in XWF_Read)", ReadFile(hVolumeOrItem, lpBuffer, nNumberOfBytesToRead, &nRead, NULL) == TRUE)) return nTotalRead;
		if (!nRead) return nTotalRead;
		nNumberOfBytesToRead -= nRead;
		nTotalRead += nRead;
	}
	return nTotalRead;
}

XT_API const wchar_t* XWF_GetItemName(LONG nItemID)
{
	const wchar_t * start = GetName(nItemID);
	const wchar_t * pos = start;
	while (*pos) { ++pos; }
	while (pos != start && *pos != '\\') --pos;
	if(pos != start) pos++;
	return pos;
}

XT_API INT64 XWF_GetSize(HANDLE hVolumeOrItem, BYTE* lpOptional)
{
	verify("Warning: XWF_GetSize called with unexpected lpOptional (!= 0, 1 or 2)", lpOptional == (BYTE*)0 || lpOptional == (BYTE*)1 || lpOptional == (BYTE*)2);

	LARGE_INTEGER size;
	if (!verify("Warning: GetFileSizeEx failed (in XWF_GetSize)", GetFileSizeEx(hVolumeOrItem,&size) == TRUE)) return 0;

	return size.QuadPart;
}

XT_API DWORD XWF_GetItemType(LONG nItemID, wchar_t*lpTypeDescr, LONG nBufferLen)
{
	if(!verify("Error: XWF_GetItemType called with nBufferLen <=0", nBufferLen > 0)) return -1;
	if (!verify("Error: XWF_GetItemType called with NULL lpTypeDescr", lpTypeDescr != NULL)) return -1;

	wcscpy_s((wchar_t*)lpTypeDescr, nBufferLen, GetType(nItemID));

	return 3;
}

XT_API void XWF_OutputMessage(const wchar_t* lpMessage, DWORD nFlags)
{
	if(!verify("Error: XWF_OutputMessage called with NULL lpMessage", lpMessage != NULL)) return;
	
	PostLogItem(lpMessage, nFlags);
}

XT_API long XWF_CreateFile(wchar_t* lpName, DWORD flags, LONG nParentItemID, PVOID pSourceInfo)
{
	verify("XWF_CreateFile called with NULL lpName", lpName != NULL);
	verify("XWF_CreateFile called with unexpected flags (!= 0x04)", flags == 0x04);
	verify("XWF_CreateFile called with NULL pSourceInfo", pSourceInfo != NULL);

	DWORD dwAttrib = GetFileAttributes((wchar_t*)pSourceInfo);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY)) ? 0x88 : -1;
}

XT_API INT64 XWF_GetCaseProp(LPVOID pReserved, LONG nPropType, PVOID pBuffer, LONG nBufSize)
{
	verify("XWF_GetCaseProp pReserved not NULL", pReserved == NULL);
	if(!verify("XWF_GetCaseProp called with NULL pBuffer", pBuffer != NULL)) return -1;
	if(!verify("XWF_GetCaseProp called with nBufSize < 1", nBufSize > 0)) return -1;

	const wchar_t * path, * pos;
	size_t size, name_size;

	switch (nPropType) {
	case XWF_CASEPROP_TITLE:
		wcscpy_s((wchar_t*)pBuffer, nBufSize, L"Test Case");
		size = sizeof(L"Test Case");
	case XWF_CASEPROP_EXAMINER:
		wcscpy_s((wchar_t*)pBuffer, nBufSize, L"John User");
		size = sizeof(L"John User");
	case XWF_CASEPROP_DIR:
	case XWF_CASEPROP_FILE:
		pos = path = GetName(lastItem);
		if (!verify("GetName() returned a NULL pointer (in XWF_GetCaseProp)", path != 0)) return -1;

		while (*pos) { ++pos; }                            // Walk to end of string
		while (pos != path && *pos != '\\') { --pos; }     // Walk back to last '\'
		size = pos - path + 1;                             // Size includes null terminator
		verify("XWF_GetCaseProp called with insufficient buffer", (size_t)nBufSize > size);

		if (size > (size_t)nBufSize) size = nBufSize;
		if (size - 1 > 0) memcpy(pBuffer, path, size - 1); // Don't copy null terminator in case
		                                                   // ...string needs to be truncated
		*((wchar_t*)pBuffer + size - 1) = 0;                  // Add null terminator

		if (nPropType == XWF_CASEPROP_FILE) {
			name_size = sizeof(L"\\Test.xfc") - 1;         // name_size is not including null terminator
			                                               // because size allows for the terminator
			verify("XWF_GetCaseProp called with insufficient buffer", (size_t)nBufSize > size + name_size);
			if (size + name_size > (size_t)nBufSize) name_size = nBufSize - size;

			// Subtract 2 in order to overwrite the null terminator previously added
			if(name_size > 0) memcpy((char*)pBuffer + size - 2, L"\\Test.xfc", name_size);
			size += name_size;
			*((wchar_t*)pBuffer + size - 1) = 0;              // Add null terminator back
		}
		break;
	default:
		verify("XWF_GetCaseProp called with bad property type (nPropType)", 0);
		return -1;
	}
	return size;
}

XT_API void XWF_GetVolumeName(HANDLE hVolume, wchar_t* lpString, DWORD nType) { verify("XWF_GetVolumeName not implemented", 0); }
XT_API void XWF_GetVolumeInformation(HANDLE hVolume, LPLONG lpFileSystem, DWORD* nBytesPerSector, DWORD* nSectorsPerCluster, INT64* nClusterCount, INT64* nFirstClusterSectorNo) { verify("XWF_GetVolumeInformation not implemented", 0); }
XT_API BOOL XWF_GetSectorContents(HANDLE hVolume, INT64 nSectorNo, wchar_t* lpDescr, LPLONG lpItemID) { verify("XWF_GetSectorContents not implemented", 0); return 0; }
XT_API void XWF_SelectVolumeSnapshot(HANDLE hVolume) { verify("XWF_SelectVolumeSnapshot not implemented", 0); };
XT_API DWORD XWF_GetItemCount(HANDLE hVolume) { verify("XWF_GetItemCount not implemented", 0); return 0; }
XT_API long int XWF_CreateItem(wchar_t* lpName, DWORD flags) { verify("XWF_CreateItem not implemented", 0); return 0; }
XT_API INT64 XWF_GetItemSize(LONG nItemID) { verify("XWF_GetItemSize not implemented", 0); return 0; }
XT_API void XWF_SetItemSize(LONG nItemID, INT64 size) { verify("XWF_SetItemSize not implemented", 0); }
XT_API void XWF_GetItemOfs(LONG nItemID, INT64* lpDefOfs, INT64* lpStartSector) { verify("XWF_GetItemOfs not implemented", 0); }
XT_API void XWF_SetItemOfs(LONG nItemID, INT64 nDefOfs, INT64 nStartSector) { verify("XWF_SetItemOfs not implemented", 0); }
XT_API INT64 XWF_GetItemInformation(LONG nItemID, LONG InfoType, LPBOOL lpSuccess) { verify("XWF_GetItemInformation not implemented", 0); return 0; }
XT_API BOOL XWF_SetItemInformation(LONG nItemID, LONG InfoType, INT64 nInfoValue) { verify("XWF_SetItemInformation not implemented", 0); return 0; }
XT_API void XWF_SetItemType(LONG nItemID, wchar_t*lpTypeDescr, LONG nTypeStatus) { verify("XWF_SetItemType not implemented", 0); }
XT_API LONG XWF_GetItemParent(LONG nItemID) { verify("XWF_GetItemParent not implemented", 0); return 0; }
XT_API void XWF_SetItemParent(LONG nChildItemID, LONG nParentItemID) { verify("XWF_SetItemParent not implemented", 0); }
XT_API LONG XWF_GetReportTableAssocs(LONG nItemID, wchar_t* lpBuffer, LONG nBufferLen) { verify("XWF_GetReportTableAssocs not implemented", 0); return 0; }
XT_API LONG XWF_AddToReportTable(LONG nItemID, wchar_t* lpReportTableName, DWORD nFlags) { verify("XWF_AddToReportTable not implemented", 0); return 0; }
XT_API wchar_t* XWF_GetComment(LONG nItemID) { verify("XWF_GetComment not implemented", 0); return 0; }
XT_API BOOL XWF_AddComment(LONG nItemID, wchar_t* lpComment, DWORD nFlags) { verify("XWF_AddComment not implemented", 0); return 0; }
XT_API void XWF_ShowProgress(wchar_t* lpCaption, DWORD nFlags) { verify("XWF_ShowProgress not implemented", 0); }
XT_API void XWF_SetProgressPercentage(DWORD nPercent) { verify("XWF_SetProgressPercentage not implemented", 0); }
XT_API void XWF_SetProgressDescription(wchar_t* lpStr) { verify("XWF_SetProgressDescription not implemented", 0); }
XT_API BOOL XWF_ShouldStop(void) { verify("XWF_ShouldStop not implemented", 0); return 0; }
XT_API void XWF_HideProgress(void) { verify("XWF_HideProgress not implemented", 0); }
XT_API HANDLE XWF_OpenItem(HANDLE hVolume, LONG nItemID, DWORD nFlags) { verify("XWF_OpenItem not implemented", 0); return 0; }
XT_API void XWF_Close(HANDLE hVolumeOrItem) { verify("XWF_Close not implemented", 0); }
XT_API HANDLE XWF_CreateEvObj(DWORD nType, LONG nDiskID, LPWSTR lpPath, PVOID pReserved) { verify("XWF_CreateEvObj not implemented", 0); return 0; }
XT_API INT64 XWF_GetVSProp(LONG nPropType, PVOID pBuffer) { verify("XWF_GetVSProp not implemented", 0); return 0; }
XT_API LONG XWF_Search(SearchInfo* SInfo, CodePages* CPages) { verify("XWF_Search not implemented", 0); return 0; }
XT_API HANDLE XWF_CreateContainer(LPWSTR lpFileName, DWORD nFlags, LPVOID pReserved) { verify("XWF_CreateContainer not implemented", 0); return 0; }
XT_API LONG XWF_CopyToContainer(HANDLE hContainer, HANDLE hItem, DWORD nFlags, DWORD nMode, INT64 nStartOfs, INT64 nEndOfs, LPVOID pReserved) { verify("XWF_CopyToContainer not implemented", 0); return 0; }
XT_API LONG XWF_CloseContainer(HANDLE hContainer, LPVOID pReserved) { verify("XWF_CloseContainer not implemented", 0); return 0; }
XT_API BOOL XWF_GetBlock(HANDLE hVolume, INT64* lpStartOfs, INT64* lpEndOfs) { verify("XWF_GetBlock not implemented", 0); return 0; }
XT_API BOOL XWF_SetBlock(HANDLE hVolume, INT64 nStartOfs, INT64 nEndOfs) { verify("XWF_SetBlock not implemented", 0); return 0; }
XT_API HANDLE XWF_GetFirstEvObj(LPVOID pReserved) { verify("XWF_GetFirstEvObj not implemented", 0); return 0; }
XT_API HANDLE XWF_GetNextEvObj(HANDLE hPrevEvidence, LPVOID pReserved) { verify("XWF_GetNextEvObj not implemented", 0); return 0; }
XT_API HANDLE XWF_OpenEvObj(HANDLE hEvidence, DWORD nFlags) { verify("XWF_OpenEvObj not implemented", 0); return 0; }
XT_API VOID XWF_CloseEvObj(HANDLE hEvidence) { verify("XWF_CloseEvObj not implemented", 0); }
XT_API INT64 XWF_GetEvObjProp(HANDLE hEvidence, DWORD nPropType, PVOID pBuffer) { verify("XWF_GetEvObjProp not implemented", 0); return 0; }
XT_API LPWSTR XWF_GetExtractedMetadata(LONG nItemID) { verify("XWF_GetExtractedMetadata not implemented", 0); return 0; }
XT_API LPWSTR XWF_GetMetadata(LONG nItemID, HANDLE hItem) { verify("XWF_GetMetadata not implemented", 0); return 0; }
XT_API BOOL XWF_AddExtractedMetadata(LONG nItemID, LPWSTR lpComment, DWORD nFlagsHowToAdd) { verify("XWF_AddExtractedMetadata not implemented", 0); return 0; }
XT_API BOOL XWF_GetHashValue(LONG nItemID, LPVOID lpBuffer) { verify("XWF_GetHashValue not implemented", 0); return 0; }
XT_API LONG XWF_AddEvent(struct EventInfo* Evt) { verify("XWF_AddEvent not implemented", 0); return 0; }
XT_API LPVOID XWF_GetReportTableInfo(LPVOID pReserved, LONG nReportTableID, PLONG lpOptional) { verify("XWF_GetReportTableInfo not implemented", 0); return 0; }
XT_API LPVOID XWF_GetEvObjReportTableAssocs(HANDLE hEvidence, LONG nFlags, PLONG lpValue) { verify("XWF_GetEvObjReportTableAssocs not implemented", 0); return 0; }

inline bool verify(const char * message, bool condition) {
	if (!condition) {
		XWF_OutputMessage((wchar_t*)message, 0x04);
	}
	return condition;
}
