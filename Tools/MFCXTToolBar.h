#pragma once
#include "afxtoolbar.h"


class CMFCXTToolBar :
	public CMFCToolBar
{
	DECLARE_DYNAMIC(CMFCXTToolBar)

public:
	virtual BOOL LoadState(LPCTSTR lpszProfileName, int nIndex, UINT uiID);
};

