
// XTGUI.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "XTGUI.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "XTGUIDoc.h"
#include "XTGUIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CXTGUIApp

BEGIN_MESSAGE_MAP(CXTGUIApp, CWinAppEx)
	ON_COMMAND(ID_XT_DLL, OnXTDLL)
	ON_COMMAND(ID_XT_FILETYPE, OnXTFileType)
	ON_CBN_SELENDOK(ID_XT_DLL, OnXTDLL)
	ON_CBN_SELENDOK(ID_XT_FILETYPE, OnXTFileType)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND_RANGE(ID_FILE_MRU_FILE1, ID_FILE_MRU_FILE16, OnOpenRecentFile)
	// Standard print setup command
END_MESSAGE_MAP()

// CXTGUIApp construction

CXTGUIApp::CXTGUIApp()
{
	m_bHiColorIcons = TRUE;

	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// If the application is built using Common Language Runtime support (/clr):
	//     1) This additional setting is needed for Restart Manager support to work properly.
	//     2) In your project, you must add a reference to System.Windows.Forms in order to build.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// Application ID string: CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("Purgatory.XTViewerM"));
}

// The one and only CXTGUIApp object
CXTGUIApp theApp;


// CXTGUIApp initialization

BOOL CXTGUIApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();


	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	EnableTaskbarInteraction();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored

	SetRegistryKey(_T("Purgatory"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)
	SetRegistryBase(_T("Settings"));

	InitContextMenuManager();
	InitKeyboardManager();
	InitTooltipManager();

	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_HTMLFILE,
		RUNTIME_CLASS(CXTGUIDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CXTGUIView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;

	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Don't display a new MDI child window during startup
	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew) {
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	}

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// The main window has been initialized, so show and update it
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}

int CXTGUIApp::ExitInstance()
{
	return CWinAppEx::ExitInstance();
}

// CXTGUIApp message handlers

void CXTGUIApp::OnFileOpen()
{
	OnXTDLL();
	OnXTFileType();

	if (!m_strDLL.GetLength()) {
		AfxMessageBox(_T("No X-Tensions found in the X-Tension Viewer directory"));
		return;
	}
	else if (!m_strFileType.GetLength()) {
		AfxMessageBox(_T("Please select a file type"));
		return;
	}
	CWinAppEx::OnFileOpen();
}

void CXTGUIApp::OnOpenRecentFile(UINT nID)
{
	OnXTDLL();
	OnXTFileType();

	if (!m_strDLL.GetLength()) {
		AfxMessageBox(_T("No X-Tensions found in the X-Tension Viewer directory"));
		return;
	}
	else if (!m_strFileType.GetLength()) {
		AfxMessageBox(_T("Please select a file type"));
		return;
	}
	CWinAppEx::OnOpenRecentFile(nID);
}

void CXTGUIApp::OnXTDLL()
{
	CMFCToolBarComboBoxButton* pComboButton = NULL;

	CObList listButtons;
	if (CMFCToolBar::GetCommandButtons(ID_XT_DLL, listButtons) > 0) {
		for (POSITION posCombo = listButtons.GetHeadPosition(); pComboButton == NULL && posCombo != NULL;) {
			pComboButton = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, listButtons.GetNext(posCombo));
			if (pComboButton != NULL)  break;
		}

	}
	if (pComboButton != NULL) {
		ASSERT_VALID(pComboButton);

		LPCTSTR lpszSelItem = pComboButton->GetItem();
		m_strDLL = (lpszSelItem == NULL) ? _T("") : lpszSelItem;
	}
}

void CXTGUIApp::OnXTFileType()
{
	CMFCToolBarComboBoxButton* pComboButton = NULL;

	CObList listButtons;
	if (CMFCToolBar::GetCommandButtons(ID_XT_FILETYPE, listButtons) > 0) {
		for (POSITION posCombo = listButtons.GetHeadPosition(); pComboButton == NULL && posCombo != NULL;) {
			pComboButton = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, listButtons.GetNext(posCombo));
			if (pComboButton != NULL)	break;
		}
	}

	if (pComboButton == NULL) return;
	ASSERT_VALID(pComboButton);

	CComboBox * pCombo = pComboButton->GetComboBox();
	if (pCombo == NULL) return;
	ASSERT_VALID(pCombo);

	pCombo->GetWindowText(m_strFileType);

	// Add to combo list if not already there, but ensure max 10 entries
		if (pComboButton->FindItem(m_strFileType) == CB_ERR) {
		pComboButton->AddItem(m_strFileType);
		if (pComboButton->GetCount() > 10) {
			LPCTSTR lpszDelItem = pComboButton->GetItem(0);
			if (lpszDelItem) {
				pComboButton->DeleteItem(lpszDelItem);
				pComboButton->SelectItem(m_strFileType);
			}
		}
	}
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// App command to run the dialog
void CXTGUIApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CXTGUIApp customization load/save methods

void CXTGUIApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
}

void CXTGUIApp::LoadCustomState()
{
}

void CXTGUIApp::SaveCustomState()
{
}


