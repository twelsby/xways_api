#include "stdafx.h"
#include "XTGUI.h"
#include "MFCXTToolBar.h"
#include "XTAPI.h"

IMPLEMENT_DYNAMIC(CMFCXTToolBar, CMFCToolBar)

//BEGIN_MESSAGE_MAP(CMFCXTToolBar, CMFCToolBar)
//END_MESSAGE_MAP()

BOOL CMFCXTToolBar::LoadState(LPCTSTR lpszProfileName, int nIndex, UINT uiID) {
	CMFCToolBar::LoadState(lpszProfileName, nIndex, uiID);

	CMFCToolBarComboBoxButton* pSrcCombo = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, GetButton(CommandToIndex(ID_XT_DLL)));

	if (pSrcCombo) {
		pSrcCombo->RemoveAllItems();

		WIN32_FIND_DATA fd;
		HMODULE Hdl;
		fptr_XT_Init XT_Init;

		CString strSearch;
		GetModuleFileNameW(NULL, strSearch.GetBufferSetLength(MAX_PATH), MAX_PATH);
		strSearch.ReleaseBuffer();
		strSearch.Truncate(strSearch.ReverseFind('\\'));
		strSearch.Append(_T("\\*.dll"));

		HANDLE hFind = ::FindFirstFile(strSearch, &fd);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
					Hdl = LoadLibraryW(fd.cFileName);
					if (Hdl) {
						XT_Init = (fptr_XT_Init)GetProcAddress(Hdl, "XT_Init");
						if (XT_Init) {
							CString str(fd.cFileName);
							str = str.Left(str.ReverseFind('.'));
							pSrcCombo->AddItem(str);
						}
						FreeLibrary(Hdl);
					}
				}
			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
		if(!pSrcCombo->SelectItem(_T("XT_VIEW_64"))) pSrcCombo->SelectItem(0);
	}
	return 0;
}

