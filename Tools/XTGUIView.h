
// XTGUIView.h : interface of the CXTGUIView class
//

#pragma once

class CXTGUIView : public CHtmlView
{
protected: // create from serialization only
	CXTGUIView();
	DECLARE_DYNCREATE(CXTGUIView)

	BOOL m_bDirty = false;

// Attributes
public:
	CXTGUIDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnDocumentComplete(LPCTSTR lpszURL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);

// Implementation
public:
	virtual ~CXTGUIView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintDirect();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnFilePageSetup();
	afx_msg void OnFilePrintSetup();
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in XTViewerMView.cpp
inline CXTGUIDoc* CXTViewView::GetDocument() const
   { return reinterpret_cast<CXTGUIDoc*>(m_pDocument); }
#endif

