// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file XTCLI.cpp Entry point for command line utility XTCLI

#include "XTAPI.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include <iostream>
#include <fstream>

#define XT_API extern "C" __declspec(dllexport)

using namespace std;

wstring name;
wstring type;

void usage()
{
	cout << "Usage: RunXT.exe dll [filename] [type] [operation]\n";
	cout << "   dll       X-Tension to run e.g. XT_VIEW_64 (if the other arguments are\n";
	cout << "             omitted then the X-Tension's XT_About export will be called)\n";
	cout << "   filename  Name of the file to examine\n";
	cout << "   type      Type to be reported to the X-Tension\n";
	cout << "   operation Operation to perform, which may be any of the following:\n";
	cout << "                  view     preview\n";
	cout << "                  run      execute from main menu\n";
	cout << "                  rvs      refine volume snapshot\n";
//	cout << "                  lss      logical simultaneous search\n";
//	cout << "                  pss      physical simultaneous search\n";
	cout << "                  dbc      directory browser context menu\n";
//	cout << "                  shc      search hit context menu\n";
}

inline void verify(const string message, bool condition) {
	if (!condition) {
		cout << "Fatal error: " << message << "\n";
		exit(0);
	}
}

static const wchar_t * GetName(LONG) 
{
	return name.c_str();
}

static const wchar_t * GetType(LONG)
{ 
	return type.c_str();
}

void PostLogItem(const wchar_t * message, DWORD dwFlags)
{
	if(dwFlags & 0x04) cout << (char*)message << "\n";
	else wcout << (wchar_t*)message << L"\n";
}

int wmain(int argc, wchar_t *argv[])
{
	if (argc < 5 && argc != 2) {
		usage();
		return 0;
	}

	fptr_XT_Init XT_Init;
	fptr_XT_View XT_View;
	fptr_XT_ReleaseMem XT_ReleaseMem;
	fptr_XT_About XT_About;
	fptr_XT_Done XT_Done;
	fptr_XT_Prepare XT_Prepare;
	fptr_XT_Finalize XT_Finalize;
	fptr_XT_ProcessItemEx XT_ProcessItemEx;
	fptr_XT_ProcessItem XT_ProcessItem;

	INT64 size;
	LONG lret;
	PVOID pret;
	BOOL bret;

	HMODULE Hdl = LoadLibraryW(argv[1]);
	if (!Hdl) {
		wcout << L"Failed to load dll: " << argv[1] << L"\n\n";
		usage();
		return 0;
	}

	XWF_Init(GetName, GetType, PostLogItem);

	XT_Init = (fptr_XT_Init)GetProcAddress(Hdl, "XT_Init");
	if (!XT_Init) {
		wcout << L"Failed to find mandatory export XT_Init in: " << argv[1] << L"\n\n";
		usage();
		return 0;
	}
	LicenseInfo licence = { sizeof(LicenseInfo),0x31,1,{ 0,0 },{ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 } };
	lret = XT_Init(18, XT_INIT_XWF, NULL, &licence);
	verify("XT_Init did not return 1", lret == 1);

	if (argc == 2) {
		XT_About = (fptr_XT_About)GetProcAddress(Hdl, "XT_About");
		if (!XT_About) {
			wcout << L"No export XT_About in: " << argv[1] << L", exiting...\n\n";
			return 0;
		}
		lret = XT_About(NULL, NULL);
		verify("XT_About did not return 1", lret == 0);
		return 0;
	}

	name = argv[2];
	type = argv[3];

	HANDLE file = CreateFile(name.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		wcout << L"Could not open input file: " << argv[2] << L"\n\n";
		usage();
		return 0;
	}

	LONG item = 0xAA;
	HANDLE volume = (HANDLE)0xBB;
	HANDLE evidence = (HANDLE)0xDD;
	uint32_t operation = -1;

	if (wstring(L"view") == argv[4]) {
		XT_View = (fptr_XT_View)GetProcAddress(Hdl, "XT_View");
		if (!XT_View) {
			wcout << L"No export XT_View in: " << argv[1] << L", exiting...\n\n";
			return 0;
		}
		XT_ReleaseMem = (fptr_XT_ReleaseMem)GetProcAddress(Hdl, "XT_ReleaseMem");
		if (!XT_ReleaseMem) {
			wcout << L"No export XT_ReleaseMem in: " << argv[1] << L", exiting...\n\n";
			return 0;
		}

		pret = XT_View(file, item, volume, evidence, NULL, &size);
		verify("XT_View returned a null pointer but size > 0", pret != 0 || (int64_t)size<0);

		if (pret) {
			ofstream viewfile(wstring(name) + L".htm");
			viewfile << (char*)pret;
			bret = XT_ReleaseMem(pret);
		}
	}
	else if (wstring(L"run") == argv[4]) operation = XT_ACTION_RUN;
	else if (wstring(L"rvs") == argv[4]) operation = XT_ACTION_RVS;
	else if (wstring(L"lss") == argv[4]) operation = XT_ACTION_LSS;
	else if (wstring(L"pss") == argv[4]) operation = XT_ACTION_PSS;
	else if (wstring(L"dbc") == argv[4]) operation = XT_ACTION_DBC;
	else if (wstring(L"shc") == argv[4]) operation = XT_ACTION_SHC;

	if(operation != -1)  {
		XT_ProcessItem = (fptr_XT_ProcessItem)GetProcAddress(Hdl, "XT_ProcessItem");
		XT_ProcessItemEx = (fptr_XT_ProcessItemEx)GetProcAddress(Hdl, "XT_ProcessItemEx");
		if (!XT_ProcessItemEx && !XT_ProcessItem) {
			wcout << L"No export XT_ProcessItem or XT_ProcessItemEx in: " << argv[1] << L", exiting...\n\n";
			return 0;
		}

		XT_Prepare = (fptr_XT_Prepare)GetProcAddress(Hdl, "XT_Prepare");
		if (XT_Prepare) {
			lret = XT_Prepare(volume, evidence, operation, 0);
			verify("XT_Prepare (RVS) returned a negative number", lret >= 0);
		}
		if (XT_ProcessItem) {
			lret = XT_ProcessItem(item, 0);
			verify("XT_ProcessItem did not return 0", lret == 0);
		}
		if (XT_ProcessItemEx) {
			lret = XT_ProcessItemEx(item, file, 0);
			verify("XT_ProcessItemEx did not return 0", lret == 0);
		}
		XT_Finalize = (fptr_XT_Finalize)GetProcAddress(Hdl, "XT_Finalize");
		if (XT_Finalize) {
			lret = XT_Finalize(file, evidence, operation, 0);
			verify("XT_Finalise (RVS) did not return 0 or 1", lret == 0 || lret == 1);
		}
	}

	XT_Done = (fptr_XT_Done)GetProcAddress(Hdl, "XT_Done");
	if (XT_Done) {
		lret = XT_Done(NULL);
		verify("XT_Done did not return 0", lret == 0);
	}
	return 0;
}
