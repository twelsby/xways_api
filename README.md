# X-Ways API C++ Wrapper

## Design goals

X-Ways Forensics is a comprehensive forensic tool which comes with a relatively powerful API.  Unfortunately the current official (?)  [C/C++ resources](https://www.x-ways.net/forensics/x-tensions/api.html) to access the API have not kept pace with the developing API.  There are also a number of prominent bugs/inadequacies in the resources:

- **Badly behaving struct packing** that fails to preserve the original packing

- **Typographical errors** such as commas instead of semicolons in struct definitions

- **Missing struct and constant definitions** of which there are many

- **Unusual and unexpected floating point behavior** such as floating point exceptions being enabled before the X-Tension is called by X-Ways

- **No C++ features** and the resources are essentially C code with a .cpp extension

Rather than simply addressing these inadequacies the aim of this project is to extend the API to simplify the development of X-Tensions with the following primary design goals:

- **Full API support**. The full X-Ways API should be implemented, include all available exports and the data structures and constant definitions that are necessary to access them.

- **Trivial integration**. The basic API wrapper can be included in a project with a single header file `xwaysapi.hpp` - no library files, source code files or other dependencies are required. Advanced functionality can be included with two more header files `xtension.hpp` and a required external dependency `rapidXML.hpp`.

- **Advanced C++ features**. The basic API wrapper provides modern C++ features such as STL like formatted stream access to X-Ways files and scoped type-safe `enum`'s that also support transparent bitwise operations (unlike standard C++).  The advanced header files provide classes that use STL objects and object oriented principals to greatly simplify the development of X-Tensions.

See the [contribution guidelines](CONTRIBUTING.md) for more information.

## Supported compilers

Currently, the following compilers are known to work:

- Microsoft Visual C++ 14.0

## Documentation

Full documentation is provided in the source code and has also been generated from the source code using Doxygen.  The Doxygen generated documentation is available online in both [html](http://twelsby.bitbucket.org/XWAYS_API/index.html) and [PDF](http://twelsby.bitbucket.org/PDF/XwAYS_API.pdf).

## Basic API use

For basic API access simply include `xwaysapi.hpp` and then create an `xways::api` object:

```cpp
#include "xwaysapi.hpp"
#include <iostream>
#include <string>

xways::api xways_api;

// Get the item type
char buffer[10];
xways_api.get_item_type(id, buffer, sizeof(buffer));
std::cout << "The item type description is: " << std::string(buffer) << std::endl;
```

You may then call any of the X-Ways API exports, which are all public methods of `xways::api`.

For formatted stream access to X-Ways files, create an `xways::item_stream` object and use it like any STL stream:

```cpp
#include "xwaysapi.hpp"
#include <iostream>
#include <string>

xways::api xways_api;
xways::item_stream item(xways_api, id);

// Seek to 10 bytes from the current position
item.seekg(10, std::ios_base::cur);

// Parse an int
int val;
item >> val;
std::cout << "Read an integer, it was: " << std::to_string(val) << std::endl;

// Read 10 bytes
char buffer[10];
item.read(buffer, sizeof(buffer));
std::cout << "Read 10 bytes, it was: " << std::string(buffer) << std::endl;

```

Do not forget to set the necessary switches to enable C++11 (e.g., `-std=c++11` for GCC and Clang).

## Using the existing project as a template for a new X-Tension

The Visual Studio project located in the `XWAYS_API` directory can be used as a template for developing an X-Tension.  The steps to set it up for a new X-Tension are:

1. Create a new empty Visual Studio solution (hint: File->New->Project...->Other Project Types->Visual Studio Solutions->Blank Solution)

2. Copy the `XWAYS_API` directory to a new directory with the desired name in the newly created solution directory

3. Rename the `XWAYS_API.vcxproj` as desired and delete the `XWAYS_API.vcxproj.user` and `XWAYS_API.vcxproj.filters` files, if present (they will be recreated automatically)

4. Import the new project into the solution (hint: File->Add->Existing Project, then navigate to the renamed `.vcxproj` file)

5. Rename the project in the 'Solution Explorer' window (note that the project name will be used to name the target and will also be used in the messages displayed in the X-Ways messages window)

6. Update the `Resources.rc` with copyright and description information as required

You should now be able to build the project and produce a DLL with the name of your project with "_64" appended (32 bit builds, while included in the project, do not currently work). You will receive some warnings about commands not being found but these can be ignored; they are due to the fact that the project template is setup to use *git* version control software, which is optional but requires further setup - see below.

This X-Tension won't actually do anything and so must be customized.  To customize the X-Tension you have three options (from most difficult to least):

- Create an X-Tension that uses only the basic API via `xwaysapi.hpp`

- Create an X-Tension that implements a class derived from `xtension::view` and generates any representation of a file that is supported by X-Ways (e.g. a raster image) as a "Viewer" or "Regular" X-Tension

- Create an X-Tension that implements a class derived from `xtension::html_view` and generates a html representation of a file as a "Viewer" or "Regular" X-Tension

Whatever approach you adopt you will still be able to access the full X-Ways API and implement any type of X-Tension supported by X-Ways.

## Create an X-Tension that only uses the basic API

Start by creating a project using the existing project as a template as described above.  Then:

1. Open `dllmain.cpp`

2. Delete the line `xtension::html_view<> view(xways_api)`, that is towards the top of the file (at or around line 36)

3. Delete all lines that involve the variable `view` (after the above step they should now be marked by red squared at the right edge of the page and red wavy lines under the text)

4. Implement your desired functionality using the `xways::api`, `xways::item_stream` and `xways::item_streambuf` classes as described above.  For example you may implement a "Viewer X-Tension" by using `xways::item_stream` to read the file and based on this data fill a buffer you allocate with `new` with the representation data, which you then return to X-Ways.  Then in the `XT_ReleaseMem()` export call `delete` on the pointer supplied.

## Create an X-Tension that uses a class derived from `xtension::view`

First you need to decide if you need custom options in the options file.  If you do then you need to subclass `xtension::options`, otherwise skip to Step 6.

1. Add a new header file to the project

2. Subclass `xtension::options` like this (you don't have to call it `custom_options`):

        #include "xtension.hpp"

        class custom_options : public xtension::options {
        public:
        	// Use constructor from options
        	using xtension::options::options;

        	// Overload options::load()
        	void load();
        private:
        	// Test attribute
        	std::string test;
        };

3. Add any member attributes (like `test` above) or helper methods that you need

4. Add a new source file to the project

5. Implement the overloaded `xtension::options::load()` method using the classes from `rapidXML`; see the documentation for `xtension::options` for a detailed example but this is an abbreviated example:

        #include "xtension.hpp"
        #include "rapidxml.hpp"

        void custom_options::load()
        {
        	// Important! Pass load call up the chain
        	options::load();

       		// load() failed somewhere so abort
        	if (!success) return;

        	// Look for a tag named "test_tag"
        	rapidxml::xml_node<> * node = root->first_node("test_tag", 0, false);

        	// If found save its value
        	if (node) test = node->value();
        }


6. Add a new header file to your project (or you can add to the header file you created at Step 1.)

7. Subclass `xtension::view` (you don't have to call it `custom_view`), making sure to call the `xtension::view::view()` constructor with suitable text for the second parameter (this will be used to generate the options file name in the form "XT_<text>.XML") and to override `xtension::view::create()`, like this (if you skipped to Step 6. replace both `<custom_options>` with `<>`):

        #include "xtension.hpp"

        class custom_view : public xtension::view<custom_options> {
        public:
        	custom_view(xways::api &xwf) : view<custom_options>(xwf, L"CUSTOM") {}
        	void* create(int32_t id, HANDLE handle, int64_t& size);
        };


8. Add any member attributes or helper methods that you need

9. Add a new source file to your project (or you can add to the header file you created at Step 4.)

10. Implement the custom `create()` method using API calls to check if you should handle the file at all and to render it to a buffer (which *must* be allocated with `new`), like this:

        #include "xwaysapi.hpp"
        #include "xtension.hpp"

        void* custom_view::create(int32_t id, HANDLE handle, int64_t& size)
        {
        	// Check the options to see if we should handle this operation (e.g. RVS)
        	if (!check()) return 0;

        	// Get the file type via an API call
        	wchar_t buffer[10];
        	if (xways.get_item_type(id, buffer, sizeof(buffer)/sizeof(wchar_t)) == -1)
         		return 0;

        	// Check the file type (in this case for "xml")
        	std::wstring type(buffer);
        	if (type != L"xml") return 0;

        	// Important! Pass the call up the chain
        	xtension::view<xtension::options>::create(id, handle, size);

        	// Assume we already know the buffer needs to be 1024 bytes
        	size = 1024;

        	// Allocate the buffer
        	char* buffer = new char(size);

        	// TODO: Fill the buffer with data using API calls

        	// Return the buffer
        	return (void*)buffer;
        }


11. Replace the `xtension::view` class in `dllmain.cpp` with the new class e.g. replace `xtension::html_view<> view(xways_api);` with `xtension::custom_view<custom_options> view(xways_api);`, or if you skipped to Step 6., with `xtension::custom_view<> view(xways_api);`

## Create an X-Tension that uses a class derived from xtension::html_view

This is the easiest way to implement a "Viewer" or "Regular" X-Tension that renders to html. The steps are identical as for deriving from the `xtension::view` class except that you should replace all instances of the `xtension::view` class with `xtension::html_view` and all instances of the `xtension::options` class with `xtension::html_options`.  Then, in your `create()` overload method you may use the additional methods of the `xtension::html_view` class, `xtension::html_view::add_table()` and `xtension::html_view::finalize()` and the [`std::stringstream`](http://www.cplusplus.com/reference/sstream/stringstream/) collection `xtension::html_view::tables`.

In Step 10. above, after calling `xtension::view::create(int32_t id, void* handle, int64_t& size)`, instead of allocating a buffer and filling it directly instead call `xtension::html_view::add_table()` to add at least one [`std::stringstream`](http://www.cplusplus.com/reference/sstream/stringstream/) to the `xtension::html_view::tables` collection. Then add some html text to the [`std::stringstream`](http://www.cplusplus.com/reference/sstream/stringstream/) objects using the stream operator `<<`.  When you are finished, call `xtension::html_view::finalize()` to combine the [`std::stringstream`](http://www.cplusplus.com/reference/sstream/stringstream/) objects into a buffer that you can return to X-Ways.  A fragment demonstrating this might be:

```cpp

// Add a table; index is 0 based so in this case will be 0
size_t index = add_table();

// Add some text using the stream operator; as this is the first table it will
// already have the html header and opening <body> tag
tables[index] << "<table>";
tables[index] << "<tr><td>A1</td><td>A2</td></tr>";
tables[index] << "<tr><td>B1</td><td>B2</td></tr>";
tables[index] << "</table>";

// Assemble and return the buffer (also adds the closing <\body> and <\html> tags)
return finalize(size);

```

## Git support and automatic copying to X-Ways

The existing project template was set up to use the *git* version tracking system to automatically generate the version string that is used extensively in the library.  If the project is built outside of a *git* repository, or if you don't have *git* installed, or you haven't committed any tags yet, then you will get compiler warnings related to failed pre-build commands.  The command in question is a call to execute a *PowerShell* script named `VER.PS1`, that in turn executes `git.exe` to generate the version number based on the latest commit tag.

You can either:

- Ignore the warning

- Set up a *git* repository for the project

- Edit the project settings to remove the pre-build command and, optionally, remove `VER.PS1` from the project and delete the file

There is also a post-build command to copy the newly created X-Tension to the X-Ways program directory - presumed to be `C:\Program Files (x86)\X-Ways Forensics\x64\`.  This requires that Visual Studio be opened with administrative privileges and that the X-Tension DLL not already be loaded in X-Ways.  Again, the warning can be safely ignored or the project settings changed to remove the post-build step if desired.
