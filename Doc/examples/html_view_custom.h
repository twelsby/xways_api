#include "xtension.hpp"

class custom_view : public xtension::html_view<custom_options> {
public:
	custom_view(xways::api &xwf) : html_view<custom_options>(xwf, L"CUSTOM") {}
	void* create(int32_t id, HANDLE handle, int64_t& size);
};

