#include "xtension.hpp"

class custom_view : public xtension::view<custom_options> {
public:
	custom_view(xways::api &xwf) : view<custom_options>(xwf, L"CUSTOM") {}
	void* create(int32_t id, HANDLE handle, int64_t& size);
};

