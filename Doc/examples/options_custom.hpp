#include "xtension.hpp"
#include "rapidxml.hpp"

class custom_options : public xtension::options {
public:
	// Use constructor from options
	using xtension::options::options;

	// Overload options::load()
	void custom_options::load()
	{
		// Important! Pass load call up the chain
		options::load();

		// load() failed somewhere so abort
		if (!success) return;

		// rapidxml can throw exceptions
		try {
			// Look for a tag named "test_tag"
			rapidxml::xml_node<> * node =
				root->first_node("test_tag", 0, false);

			// Check if one was found
			if (node) {

				// Success, dump its value
				std::cout << "test_tag value:" << node->value() << std::endl;

				// Look for an attribute named "test_attrib"
				rapidxml::xml_attribute<> * attrib;
				attrib = node->first_attribute("test_attrib", 0, false);

				// Check if one was found
				if (attrib) {

					// Success, dump its value
					std::cout << attrib->value() << std::endl;
				}
			}
		}
		catch (...) {}
	}
};

class custom_view : public xtension::view<custom_options> {
public:
	custom_view(xways::api &xwf) : view<custom_options>(xwf, L"CUSTOM") {}
	void* create(int32_t id, HANDLE handle, int64_t& size);
};
