#include "xtension.hpp"

class custom_view : public xtension::view<> {
public:
	custom_view(xways::api &xways) : view<xtension::options>(xways, L"OPTIONS") {}
	void* create(int32_t id, HANDLE handle, int64_t& size);
};