#include <iostream>
#include "xwaysapi.hpp"

// Create the API object (mandatory for any X-Tension)
xways::api xways_api;

// Create a stream object from a given item id
xways::item_stream item(xways_api, id);

// Seek to 10 bytes from the current position
item.seekg(10, std::ios_base::cur);

// Parse an integer string
int int_val;
item >> int_val;
std::cout << "Parsed an integer, it is: " << std::to_string(int_val) << std::endl;

// Parse an double string
double double_val;
item >> double_val;
std::cout << "Parsed a double, it is: " << std::to_string(double_val) << std::endl;

// Read a line of text (assumed <80 chars)
char buffer[80];
item.getline(buffer, sizeof(buffer));
std::cout << "Read a line, it is: " << std::to_string(double_val) << std::endl;

// Read a comma delimited field (assumed <80 chars)
item.getline(buffer, sizeof(buffer), ',');
std::cout << "Read a field, it is: " << std::to_string(double_val) << std::endl;

// Read 10 bytes
item.read(buffer, 10);
std::cout << "Read 10 bytes, it is: " << std::string(buffer) << std::endl;

// Read an integer as a binary number
item.read(int_val, sizeof(int));
std::cout << "Read an integer, it is: " << std::to_string(int_val) << std::endl;

// Get the size of the item
int_val = item.size();
std::cout << "The size of the item, is: " << std::to_string(int_val) << std::endl;

// Get the current position in the item
int_val = item.position();
std::cout << "The current position in the item is: " << std::to_string(int_val) << std::endl;
