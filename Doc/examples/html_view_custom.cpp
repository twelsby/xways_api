#include "xwaysapi.hpp"
#include "xtension.hpp"

void* custom_view::create(int32_t id, HANDLE handle, int64_t& size)
{
	// Check the options to see if we should handle this operation (e.g. RVS)
	if (!check()) return 0;

	// Get the file type via an API call
	wchar_t buffer[10];
	if (xways.get_item_type(id, buffer, sizeof(buffer) / sizeof(wchar_t)) == -1)
		return 0;

	// Check the file type (in this case for "xml")
	std::wstring type(buffer);
	if (type != L"xml") return 0;

	// Important! Pass the call up the chain
	xtension::view<xtension::options>::create(id, handle, size);

	// TODO: Fill the buffer by: creating some tables with add_table(),
	// reading data from the item using API calls, adding it to the
	// html_view::tables collection using the stream operator `<<` and
	// finally calling finalize(); the following basic example just
	// creates a single table, reads the name of the item and writes it to
	// the html file

	// Add a table; index is 0 based so in this case will be 0
	size_t index = add_table();

	// Read the item name
	std::string name(xways_.get_item_name(id);

	// Write the name to the table
	tables[index] << name;

	// Assemble and return the buffer (also adds the closing <\body> and <\html> tags)
	return finalize(size);
}