#include "xtension.hpp"

class custom_view : public xtension::html_view<> {
public:
	custom_view(xways::api &xwf) : html_view<xtension::html_options>(xwf, L"CUSTOM") {}
	void* create(int32_t id, void* handle, int64_t& size);
};