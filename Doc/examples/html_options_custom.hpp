#include "xtension.hpp"
#include "rapidxml.hpp"

class custom_options : public xtension::html_options {
public:
	// Use constructor from html_options
	using xtension::html_options::html_options;

	// Overload html_options::load()
	void load()
	{
		// Important! Pass load call up the chain
		html_options::load();

		// load() failed somewhere so abort
		if (!success) return;

		// rapidxml can throw exceptions
		try {
			// Look for a tag named "test_tag"
			rapidxml::xml_node<> * node =
				root->first_node("test_tag", 0, false);

			// Check if one was found
			if (node) {

				// Success, dump its value
				std::cout << "test_tag value:" << node->value() << std::endl;

				// Look for an attribute named "test_attrib"
				rapidxml::xml_attribute<> * attrib;
				attrib = node->first_attribute("test_attrib", 0, false);

				// Check if one was found
				if (attrib) {

					// Success, dump its value
					std::cout << attrib->value() << std::endl;
				}
			}
		}
		catch (...) {}
	}
};


class custom_view : public xtension::html_view<custom_options> {
public:
	custom_view(xways::api &xwf) : html_view<custom_options>(xwf, L"CUSTOM") {}
	void* create(int32_t id, void* handle, int64_t& size);
};
