#include "xwaysapi.hpp"

// Create the API object (mandatory for any X-Tension)
xways::api xways_api;

char buffer[10];

// Given an item id get the item type using the API (assumed <10 chars)
xways_api.get_item_type(id, buffer, sizeof(buffer));
std::cout << "The item type description is: " << std::string(buffer) << std::endl;

// Given a handle to an item, read a string from the start of an item 
// using the API (assumed null terminated and <10 chars)
xways_api.read(handle, 0, buffer, sizeof(buffer));
std::cout << "The string is: " << std::string(buffer) << std::endl;

// Given a volume handle, get the name of the volume using the API 
// (assumed <10 chars)
xways_api.get_volume_name(volume, buffer, sizeof(buffer));
std::cout << "The volume name is: " << std::string(buffer) << std::endl;
