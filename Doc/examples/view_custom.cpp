#include "xwaysapi.hpp"
#include "xtension.hpp"

void* custom_view::create(int32_t id, HANDLE handle, int64_t& size)
{
	// Check the options to see if we should handle this operation (e.g. RVS)
	if (!check()) return 0;

	// Get the file type via an API call
	wchar_t buffer[10];
	if (xways.get_item_type(id, buffer, sizeof(buffer) / sizeof(wchar_t)) == -1)
		return 0;

	// Check the file type (in this case for "xml")
	std::wstring type(buffer);
	if (type != L"xml") return 0;

	// Important! Pass the call up the chain
	xtension::view<xtension::options>::create(id, handle, size);

	// Assume we already know the buffer needs to be 1024 bytes
	size = 1024;

	// Allocate the buffer
	char* buffer = new char(size);

	// TODO: Fill the buffer by reading data from the item using API calls
	// and converting it into into the desired form in the output buffer;
	// this example simply reads bytes 1024 bytes straight into the buffer
	// and does nothing else
	item.read(buffer, size);

	// Return the buffer
	return (void*)buffer;
}