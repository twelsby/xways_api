# How to Contribute

This project grew out of the development of a single X-Ways X-Tension, that was created to solve a particular analysis problem.  Over time a need arose to develop further X-Tensions so, rather than starting from scratch, I set about trying to create a general purpose toolkit that would simplify the process of developing new X-Tensions in the future.  I have also simultaneously been learning to use the new C++11 features and so have tried to incorporate these new features into this library whereever possible.

Due to the adhoc nature of the development of this library it is inherently incomplete, and largely untested, as to date all development has been focused on the functionality necessary to solve particular problems - while striving to keep the library as generic as possible.  There is consequently a significant opportunity for others to contribute to future development.

To make it as easy as possible to contribute and for me to keep the project to its main objectives, here are a few guidelines which should help avoid probelms.

## Prerequisites

Please [create an issue](https://bitbucket.org/twelsby/xways_api/issues/new), assuming one does not already exist, and describe your concern. Note you need a [BitBucket account](https://bitbucket.org/account/signup/) for this.

## Describe your issue

Clearly describe the issue:

- If it is a bug, please describe how to **reproduce** it. If possible, attach a complete example which demonstrates the error. Please also state what you **expected** to happen instead of the error.
- If you propose a change or addition, try to give an **example** how the improved code could look like or how to use it.
- If you found a compilation error, please tell us which **compiler** (version and operating system) you used and paste the (relevant part of) the error messages to the ticket.

## Submit modifications

To submit modifications to the code you will need to clone the repository, make the necessary changes and test the changes to your satisfaction.  When you are satisfied commit and push the changes to your cloned repository and submit a pull request.

## Wanted

The following areas really need contribution:

- Testing, testing, testing.  Most of the methods in the `api` class are untested and even the `view` based wrapper classes have not seen significant testing.  To date all testing that has been done has been with VS2015 and there is no formal unit test. 
- Extend the `view` wrapper classes to cater for a broader range of use scenarios
- Investigate wrapper classes that may be useful for other operations such as sector IO and search functionality.
- Check the documentation and add more examples and better explainations of class usage
