// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file dllmain.cpp DLL entry point and exports

#include "xwaysapi.hpp"
#include "xtension.hpp"
#include "VER.H"

//! X-Tension DLL exports macro
#define XT_API extern "C" __declspec(dllexport)

//! Default X-Tension version string (if not defined in VER.H)
#if !defined(XT_VERSION)
#define XT_VERSION L"0.0.0.0"
#endif

//! Platform string (used in the X=Ways messages window and about box)
#ifdef _WIN64
#define XT_PLATFORM L" (64 bit)"
#else
#define XT_PLATFORM L" (32 bit)"
#endif

//! Version prefix (used in the X-Ways messages window and about box)
#define VERSION_TEXT L": Version "

//! Introductory text to display in the X-Ways messages log and about
//! @note XT_PREFIX should be defined via the project itself
#define XT_ABOUT XT_PREFIX VERSION_TEXT XT_VERSION XT_PLATFORM

//! The one and only X-Ways API object for the X-Tension
xways::api xways_api;

//! The view object for this X-Tension
xtension::html_view<> view(xways_api);

/*!
@brief Entry point to initialize the X-Tension

This function, which is mandatory to export, will be called by X-Ways
before anything else happens, to inform the DLL of the version of
X-Ways Forensics that is loading the DLL and permit any initialization
that may be required.

@param[in] version  an @ref xways::caller_info_t struct containing
information about the X-Ways version

@param[in] flags  one of @ref xways::init_flag_t

@param[in] main_window  WIN32 handle to the main X-Ways window

@param[in] licence  pointer to a @ref xways::licence_info_t struct with
information about the licence

@return -1 to prevent further use of the DLL by X-Ways (e.g. if the
version is not supported), otherwise 1
*/
XT_API int32_t XT_Init(
	xways::caller_info_t version,
	xways::init_flag_t flags,
	void* main_window,
	xways::licence_info_t* licence)
{
	xways_api.output_message(XT_ABOUT, xways::msg_flag_t::UTF16);

	view.init();

	return 1;
}

/*!
@brief Entry point to clean up the X-Tension before being unloaded

This function, which is optional to export, will be called just before the
DLL is unloaded to give you a chance to dispose any allocated memory, save
certain data permanently etc.

@param[in] reserved  currently always NULL

@return always return 0
*/
XT_API int32_t XT_Done(void* reserved)
{
	return 0;
}

/*!
@brief Entry point to show information about the X-Tension to the user

This function, which is optional to export, will be called when the user
requests to see information about the DLL. Things that an X-Tension might
choose to display include:
- copyright notices
- a version number
- a brief description of the exported functionality
- extensive help on how to use it
- from where in X-Ways Forensics to call it
- with what settings

An X-Tension might also display a dialog window where the user can change
settings for this X-Tension, which it then stores in the Windows registry
or in a permanent file.

@param[in] parent_window  WIN32 handle to the parent window

@param[in] reserved  currently always NULL

@return always return 0
*/
XT_API int32_t XT_About(void* parent_window, void* reserved)
{
	xways_api.output_message(XT_PREFIX": About", xways::msg_flag_t::UTF16);
	MessageBoxW((HWND)parent_window, XT_ABOUT, L"About " XT_PREFIX, 0);

	return 0;
}

/*!
@brief Entry point to implement a "Viewer X-Tension"

This function, which is optional to export, then the X-Tension is a
"Viewer X-Tension".  It will be called for each file that is to be viewed
in a separate window or in Preview mode or that is included in a case
report (appears to not currently be implemented). The X-Tension can
provide a human-readable representation of a binary file for example as
plain text or HTML, or by converting special graphics file formats to
BMP/JPEG/PNG etc., or using any other file format understood by the viewer
component. This representation should be written into a buffer that is
allocated by the X-Tension itself using a function of its own choice (e.g.
VirtualAlloc, GlobalAlloc, HeapAlloc, malloc, GetMem, ...). X-Ways will
call XT_ReleaseMem() at a later point of time to allow it to free up the
allocated memory when no longer needed.

@a handle may be used to query the size of the file (via
xways::api::get_size()) and to read from it (using xways::api::read()),
for example to check the signature of the file, to determine if the
X-Tension supports that particular file type). @a id can be used for the
volume snapshot item property functions, e.g. to get more file system
level metadata about the file such as the filename or to get more data
from the volume snapshot such as the detected type of the file. The @a
volume handle may be used to get more information about the volum that the
file resides in, and the @a evidence handle can be used to ge more
information about the evidence object that the volume represents (if any).

In v18.2 and later, a button labelled "XT" is shown in the user interface
when Viewer X-Tensions are available (loaded), next to the "Raw" button.
That button allows the user to conveniently change the preview to the
representation provided by the first viewer X-Tension that feels
responsible for the type of the selected file. Or back to the regular
preview if not helpful, in both directions with a single mouse click. The
user may also combine Raw and XT submodes of Preview mode, for example for
debugging purposes.

@note If the user loads the X-Tension in X-Ways Forensics for viewing
purposes, XT_Prepare(), XT_Finalize(), XT_ProcessItem(),
XT_ProcessItemEx(), XT_PrepareSearch(), and XT_ProcessSearchHit() will not
be called even if exported. Also, XT_View() will not be called even if
loaded and selected by the user if a Viewer X-Tension higher in the list
signals that it does provide a view of that particular file already.

@param[in] handle  an open handle to the item

@param[in] id  the ID of the item

@param[in] volume  a handle to the volume containing the item

@param[in] evidence  a handle to the evidence item containing the item

@param[in] reserved  currently always NULL

@param[out] size  set to -1 if the X-Tension should not be responsible for
providing a representation of this file (in most instances because
X-Tensions support very specific file types only), -2 to indicate that an
error occurred that needs to be reported, 0 to signal that the file should
be represented as no data, or a positive value to signal success and
indicate the size of the data in the buffer

@return the address of the buffer, or NULL to signal failure
*/
XT_API void* XT_View(
	void* handle,
	int32_t id,
	void* volume,
	void* evidence,
	void* reserved,
	int64_t* size)
{
	return view.create(id, handle, *size);
}

/*!
@brief Entry point to allow an X-Tension to deallocate a buffer provided
to X-Ways by XT_View()

This function, which is mandatory for viewer type X-Tensions to export, is
called by X-Ways to free memory allocated by a previous call to XT_View().

@param[in] buffer  pointer to the buffer to be deallocated

@return currently ignored, but should signal success or failure of the
release of the memory buffer

@version 17.6
*/
XT_API bool XT_ReleaseMem(void* buffer)
{
	return xtension::view<>::release(buffer);
}

/*!
@brief Entry point to prepare an X-Tension for an impending operation

This function, which is optional to export, will be called immediately
for a volume before a volume snapshot refinement or some other action
starts or before items or search hits in that volume are processed
individually.

@param[in] volume  a handle to the file/volume that the X-Tension is
applied to or 0 if run from the main menu with no data window open; to
check if a non-zero handle is a file or volume, call
xways::api::get_volume_information() and check that @a bytes_per_sector
is/is not 0 respectively

@param[in] evidence  a handle to an evidence object if @a volume
represents an evidence object (version 17.5 SR-2), otherwise 0

@param[in] operation  the operation that X-Ways is performing

@param[in] reserved  currently always 0

@return one of xways::prepare_t
*/
XT_API xways::prepare_t XT_Prepare(
	void* volume,
	void* evidence,
	xways::action_t operation,
	void* reserved)
{
	view.prepare(operation);

	return xways::prepare_t::CALLPI | xways::prepare_t::EXPECTMOREITEMS;
}

/*!
@brief Entry point to allow an X-Tension to finalize an operation

This function, which is optional to export, will be called when volume
snapshot refinement or another operation has completed.

@param[in] volume  a handle to the file/volume that the X-Tension is
applied to or 0 if run from the main menu with no data window open; to
check if a non-zero handle is a file or volume, call
xways::api::get_volume_information() and check that @a bytes_per_sector
is/is not 0 respectively

@param[in] evidence  a handle to an evidence object if  @a volume
represents an evidence object (version 17.6), otherwise 0

@param[in] operation  the operation that X-Ways is performing

@param[in] reserved  currently always 0

@return 1 if the current directory listing in the directory browser of the
active data window has to be refreshed after xways::action_t::DBC (usually
not necessary, perhaps when adding new files to the directory, has an
effect in v17.6 and later only), or otherwise 0
*/
XT_API int32_t XT_Finalize(
	void* volume,
	void* evidence,
	uint32_t operation,
	void* reserved)
{
	return 0;
}

/*!
@brief Entry point to allow an X-Tension to process an item that has been
opened

This function, which is optional to export, will be called for each item
(file or directory) in the volume snapshot that is targeted for refinement
or selected and targeted with the directory browser context menu. The item
will be opened for reading prior to the function call. Export this
function if there is a need to read the item's data, which can be done
using the hItem parameter.

@param[in] id  the ID of the item

@param[in] handle  an open handle to the item

@param[in] reserved  currently always 0

@return -1 if X-Ways should stop the current operation (e.g. volume
snapshot refinement), otherwise 0
*/
XT_API int32_t XT_ProcessItemEx(int32_t id, void* handle, void* reserved)
{
	return view.attach(id, handle);
}

/*!
@brief DLL main entry point

This function is the main entry point to the DLL *for the operating
system* and has nothing to do with X-Ways.  It will be called by the
operating system when the DLL is loaded or unloaded.

@param[in] module  a handle to the DLL module

@param[in] reason  the reason code that indicates why the DLL entry-point
function is being called

@param[in] reserved  if @a reason is @a DLL_PROCESS_ATTACH, @a reserved is
NULL for dynamic loads and non-NULL for static loads; if @a reason is @a
DLL_PROCESS_DETACH, @a reserved is NULL if FreeLibrary() has been called
or the DLL load failed and non-NULL if the process is terminating

@return currently always TRUE to indicate success (only applicable when 
@a reason is @a DLL_PROCESS_ATTACH.
*/
bool APIENTRY DllMain(HMODULE module, uint32_t reason, void* reserved)
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
