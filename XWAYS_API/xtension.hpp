#pragma once

// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file xtension.hpp Viewer/regular extension base classes implementation

#include "xwaysapi.hpp"
#include "rapidxml.hpp"
#include <chrono>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <cfenv>

/*!
@brief Advanced tools for creating X-Ways X-Tensions
*/
namespace xtension {
	/*!
	@brief Base class for handling options files for generic X-Tensions

	This class is intended to be used by classes that derived from
	@ref view either directly or by deriving a class from it in
	order to implement custom options.  The options supported by
	this class are generic options suited to all X-Tensions, such
	such as flags to indicate if the X-Tension should run for the
	various operations in xways::action_t.

	Derived classes should use the constructor for this class and should
	override the load() method to implement the custom behaviour.  It is
	mandatory to call the load() method of this class from the derived
	class before doing anything in order to load the XML file.

	To use this, or a derived class, simply add it as a template
	parameter when implementing the relevant @ref view derived class.

	An example of using this class directly is:
	\includelineno "options.hpp"

	An example of deriving from this class is:
	\includelineno "options_custom.hpp"
	*/
	class options {
	public:
		/*!
		@brief Create a options object of the specified type

		Takes the @a name parameter and forms a filename string by
		concatenating with the prefix prefix "XT_" and suffix ".XML" and
		adding the full path of the calling process (i.e. the X-Ways EXE).
		It also saves the name string for subsequent use in messages.

		@param[in] xways  xways::api object to be used to make API calls

		@param[in] name  string that will be used in the options filename
		and messages from the X-Tension
		*/
		options(xways::api &xways, const std::wstring name) :
			xways_(xways), classname(name)
		{
			filename.resize(MAX_PATH);
			filename.resize(GetModuleFileNameW(NULL, &filename[0], MAX_PATH));
			filename = filename.substr(0, filename.find_last_of(L"/\\"));
			filename += L"\\XT_" + classname + L".XML";
		}

		/*!
		@brief (Re)load the options file
		*/
		virtual void load()
		{
			success = false;
			rvs = view = target = verbose = true;
			debug = info = false;
			file_size_max = 10000000;  // Default 10 MB

			try {
				std::ifstream file(filename);
				if (!file.is_open()) throw 0;
				buffer.assign((std::istreambuf_iterator<char>(file)),
					(std::istreambuf_iterator<char>()));
				buffer.push_back(0);
				doc.parse<0>(&buffer[0]);

				root = doc.first_node();
				if (!root) return;
				rapidxml::xml_node<> * node;
				rapidxml::xml_attribute<> * attrib;

				node = root->first_node("operations", 0, false);
				if (node) {
					attrib = node->first_attribute("rvs", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "false"))
							rvs = false;
					}
					attrib = node->first_attribute("view", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "false"))
							view = false;
					}

					attrib = node->first_attribute("target", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "false"))
							target = false;
					}
				}

				node = root->first_node("log", 0, false);
				if (node) {
					attrib = node->first_attribute("verbose", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "false"))
							verbose = false;
					}

					attrib = node->first_attribute("debug", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "true"))
							debug = true;
					}

					attrib = node->first_attribute("info", 0, false);
					if (attrib) {
						if (!_stricmp(attrib->value(), "true"))
							info = true;
					}
				}

				node = root->first_node("file_size", 0, false);
				if (node) {
					attrib = node->first_attribute("max", 0, false);
					if (attrib) {
						try {
							file_size_max = std::stoi(attrib->value());
						}
						catch (...) {}
					}
				}

				success = true;
			}
			catch (...) {}
		}

		/*!
		@brief Output a message to indicate the options file in use or
		failure to load it
		*/
		virtual void message()
		{
			if (!info) return;
			std::wstring str(XT_PREFIX);
			if (success) 
				str += L": Using " + classname + L" options from ";
			else 
				str += L": Could not find " + classname + L" options file ";
			str += filename;
			xways_.output_message(str.c_str(), xways::msg_flag_t::UTF16);
		}


		/*!
		@brief Short name of the X-Tension to be used in messages and the
		options filename
		*/
		const std::wstring classname;

		/*!
		@brief Flag to indicate if the X-Tension should run for rvs
		operations
		*/
		bool rvs;

		/*!
		@brief Flag to indicate if the X-Tension should run if targetted
		against an item
		*/
		bool target;

		/*!
		@brief Flag to indicate if the X-Tension should run as a viewer
		X-Tension
		*/
		bool view;

		/*!
		@brief Flag to indicate if the X-Tension should output verbose
		messages

		This is intended indicated that the X-Tension should output
		messages for any failure to process a specific file such as if an
		invalid/corrupt file is encountered or if unexpected data was
		found (not handled by the base @a View class so if this
		functionality is desired it must be implemented in the derived
		class)
		*/
		bool verbose;

		/*!
		@brief Flag to indicate if the X-Tension should output debug
		messages

		This indicates that the X-Tension should output the id of the file
		before and after processing and the time taken to process
		(automatically handled by the base @a View class)
		*/
		bool debug;

		/*!
		@brief Flag to indicate if the X-Tension should output
		informational messages

		This indicates that the X-Tension should output the name of the
		options file and if the file could be successfully opened
		(implemented in the base Options class)
		*/
		bool info;

		/*!
		@brief Maximum file size to process

		This is intended indicated that the X-Tension should insert a line
		break code for long lines at this interval.  This is not currently
		used as an alternative scheme has been implemented.
		*/
		int file_size_max;

	protected:
		/*!
		@brief X-Ways object
		*/
		xways::api xways_;

		/*!
		@brief rapidXML document object

		rapidXML object representing the parsed XML options file.  Can be
		accessed by derived classes to extract additional data from the
		options file although derived classes will probably only need the
		root node (probably should be private).
		*/
		rapidxml::xml_document<> doc;

		/*!
		@brief rapidXML root node

		rapidXML object representing the root node of the parsed XML
		options file. Can be accessed by derived classes to extract
		additional data from the options file.
		*/
		rapidxml::xml_node<> * root;

		/*!
		@brief Buffer to store the raw options file data

		Raw buffer containing the loaded XML file.  Should never be needed
		by any derived class (probably should be private).
		*/
		std::vector<char> buffer;

		/*!
		@brief Options file name

		Full path to the options file.  Calculated by the constructor by
		getting the path to the calling process (the X-Ways EXE) and
		appending "\XT_" followed by the class name followed by ".XML".
		*/
		std::wstring filename;

		/*!
		@brief Flag to indicate the options file was successfully parsed
		*/
		bool success = false;
	};

	/*!
	@brief Base class for handling options files for html style
	viewer/regular X-Tensions

	This class is intended to be used by classes that derived from
	@ref html_view either directly or by deriving a class from it in 
	order to implement custom options.  The options supported by
	this class are options suited to html style X-Tensions
	such as \<style\> and \<script\> tags.

	Derived classes should use the constructor for this class and should
	override the load() method to implement the custom behaviour.  It is
	mandatory to call the load() method of this class from the derived 
	class before doing anything in order to load the XML file.

	To use this, or a derived class, simply add it as a template
	parameter when implementing the relevant @ref html_view derived class.

	An example of using this class directly is:
	\includelineno "html_options.hpp" 

	An example of deriving from this class is:
	\includelineno "html_options_custom.hpp"
	*/
	class html_options : public options {
	public:
		using options::options;

		/*!
		@brief (Re)load the options file
		*/
		virtual void load()
		{
			options::load();

			if (!success) return;

			line_break = 0;

			try {
				rapidxml::xml_node<> * node;
				rapidxml::xml_attribute<> * attrib;

				node = root->first_node("style", 0, false);
				if (node) style = std::string(node->value());

				node = root->first_node("headscript", 0, false);
				if (node) headscript = std::string(node->value());

				node = root->first_node("bodyscript", 0, false);
				if (node) bodyscript = std::string(node->value());

				node = root->first_node("lines", 0, false);
				if (node) {
					attrib = node->first_attribute("max", 0, false);
					if (attrib) {
						try {
							line_break = std::stoi(attrib->value());
						}
						catch (...) {}
					}
				}
			}
			catch (...) {}
		}

		/*!
		@brief String to insert in the \<style\> tag
		*/
		std::string style;

		/*!
		@brief String to insert in the \<script\> tag in the \<head\> tag
		*/
		std::string headscript;

		/*!
		@brief String to insert in the \<script\> tag in the \<body\> tag
		*/
		std::string bodyscript;

		/*!
		@brief Insert forced line breaks in URLs at this interval

		This is intended indicated that the X-Tension should insert a line
		break code for long lines at this interval.  It was intended to by
		used by view::hyphenateURL but is no longer used by any current
		X-Tension.
		*/
		int line_break;
	};

	/*!
	@brief Base class for generic viewer/regular X-Tensions

	This class is not intended to be used directly, instead derive from
	this class to implement the fuctionality of generic viewer/regular
	X-Tensions.  To derive from this class simply impement an override for
	create() that first checks to see if it is appropriate to handle
	the target and then actaully parses the target and returns a filled
	buffer.

	It should not normally be necessary to override the other public
	methods attach() and release().

	The remaining protected methods are general utility functions - see 
	their respective documentation for details.

	To subclass this class add something similar to the following
	declaration to a header file (if not using custom options then the
	template parameter can be omitted i.e. replace `<custom_options>` with
	`<>`):
	\includelineno "view_custom.h"

	Then override create() by adding something similar to the following
	definition to a source file:
	\includelineno "view_custom.cpp"

	Finally, modify dllmain.cpp to use your new view class instead by 
	changing this line:

	```cpp
	xtension::html_view<> view(xways_api);
	```

	to:

	```cpp
	custom_view view(xways_api);
	```
	*/
	template<typename options_t = options>
	class view {
	public:
		/*!
		@brief Constructor for construction from only an @ref
		xways::api object

		This member exists only to provide the @ref view and @ref
		html_view classes with similar behaviour to derived classes
		i.e. not requiring a second string parameter.

		@param[in] xways  xways::api object to use to access the API
		*/
		view(xways::api &xways) :
			xways_(xways), options(xways, L"X-TENSION"), item(xways) {}

		/*!
		@brief Constructor for construction from an @ref xways::api object
		and a **const wchar_t\***

		This member is intended to be called by the constructor of derived
		classes in order to set the @ref options::classname attribute.

		@param[in] xways  xways::api object to use to access the API

		@param[in] name  pointer to a null-termianted const wchar_t string
		to be used to set the @ref options::classname attribute
		*/
		view(xways::api &xways, const wchar_t* name) : 
			xways_(xways), options(xways, name), item(xways) {}

		/*!
		@brief Quick check options to see if the operation should be 
		handled

		Derived classes should call this method and quickly abort a 
		create() or attach() operation if @a false.

		@return true if it should handled false otherwise
		*/
		bool check()
		{
			if (!attaching) {
				if (options.view) return true;
			}
			else {
				if ((operation == xways::api::ACTION::RVS && options.rvs) ||
					((operation == xways::api::ACTION::RUN ||
						operation == xways::api::ACTION::DBC) && 
						options.target))
				{
					return true;
				}
			}
			return false;
		}

		/*!
		@brief Create a representation of a file in memory

		Derived classes must override this method to create the
		representation for both viewer and regular X-Tensions.  The
		X-Tension DLL stubs will call this method with the @a id and @a
		handle of the file to be examined as well as a parameter in
		which to store the size of the representation.

		Derived classes should first do whatever checks are necessary to
		determine if they can handle the file and, if so, they should call
		the base clase version before constructing the view.  The base
		class version saves the parameters, implements debug messages and
		disables floating point exceptions that are otherwise enabled by
		X-Ways (a bug).

		@param[in] id  the X-Ways internal id of the file to be examined -
		may be used in calls to the X-Ways API that requrie an internal ID

		@param[in] handle  the X-Ways handle of the file to be examined -
		may be used in calls to the X-Ways API that require a handle

		@param[out] size  pointer to a 64bit integer that recieves the
		size of the representation

		@return pointer to the buffer containing the html representation
		*/
		virtual void* create(int32_t id, void* handle, int64_t& size)
		{
			options.load();
			item.open(handle, id);
			size = 0;

			if (options.debug) {
				start = std::chrono::system_clock::now();
				std::wstring message(XT_PREFIX": Examining file with ID ");
				message += std::to_wstring(item.id());
				message += L" using " + options.classname + L" parser...";
				xways_.output_message(
					message.c_str(), xways::msg_flag_t::UTF16);
			}

			// Some libraries use instructions that can generate floating
			// point exceptions even during normal operation but
			// unfortunately X-Ways enables them
			disable_fp_interrupts(true);

			return 0;
		}

		/*!
		@brief Initialize the X-Tension

		Loads the options and outputs a message to the X-Ways message log.
		*/
		void init()
		{
			options.load();
			options.message();
		}

		/*!
		@brief Prepare for an operation

		Reloads the options saves the operation type.

		@param[in] operation  the operation type being conducted by X-Ways
		*/
		void prepare(xways::action_t operation)
		{
			options.load();
			operation = operation;
		}


		/*!
		@brief Attach a representation of a file in as a child object in
		X-Ways

		Automatically calls the derived implementation of create() to
		generate the html representation, saves this representation as a
		temporary file in the X-Ways case temporary directory "_temp" and
		then adds this file as a child item in X-Ways.  There should not
		normally be any need for derived classes to override this method.

		@param[in] id  the X-Ways internal id of the file to be examined -
		may be used in calls to the X-Ways API that requrie an internal id

		@param[in] handle  the X-Ways handle of the file to be examined -
		may be used in calls to the X-Ways API that require a handle

		@return currently always returns 0
		*/
		int32_t attach(int32_t id, void* handle)
		{
			attaching = true;
			int64_t size;

			char * buffer = (char*)this->create(id, handle, size);
			if (size <= 0 || !buffer) return 0;

			std::wstring itemname = xways_.get_item_name(id);
			std::wstring path, filename;
			std::wstring extension = L".htm";
			tempfile(path, filename, itemname, extension);
			std::ofstream outputfile(
				path + filename + extension,
				std::ios::binary);
			outputfile << buffer;
			outputfile.flush();
			outputfile.close();

			release(buffer);

			xways_.create_file(
				(wchar_t*)(itemname + extension).c_str(),
				xways::create_flag_t::EXTERNAL,
				id,
				(void*)(path + filename + extension).c_str());

			return 0;
		}

		/*!
		@brief Release the memory used to hold the representation

		Frees the memory dynamically allocated by create() to store	the
		representation.  Derived classes should not	normally need to
		override this method.

		@param[in] buffer  pointer to the buffer holding the html
		representation (as returned by create())

		@return zero if the file cannot be handled, non-zero if it can
		*/
		static bool release(void* buffer)
		{
			if (buffer) delete buffer;

			return 0;
		}


	protected:

		/*!
		@brief UTF encoding
		*/
		enum class encoding_t : uint32_t {
			UTF8,		//!< UTF-8
			UTF16LE,	//!< UTF-16 little endian
			UTF16BE,	//!< UTF-16 big endian
			UTF32LE,	//!< UTF-32 little endian
			UTF32BE,	//!< UTF-32 big endian
			UTFUNK		//!< Unknown encoding
		};

		/*!
		@brief Find a unique filename for a temporary file in the X-Ways
		case temporary directory

		This method can be called by derived classes to find a filename
		that is guaranteed to be able to be created in the X-Ways case
		temporary directory without overwriting an existing file. It will
		first check if the file could be created using the supplied name
		and if it can't (e.g. because a file with that name already
		exists) then it will add a suffix to the filename until it can be.

		@param[out] path  reference to a string object to receive the path
		to the X-Ways case temporary directory

		@param[out] filename  reference to a string object to receive the
		filename

		@param[in] itemname  string object containing the name of item -
		will be used for the filename as is if possible or otherwise
		modified with a suffix

		@param[in] extension  string object containing the extension to be
		applied to the filename

		@return pointer to the buffer containing the html representation
		*/
		void tempfile(
			std::wstring& path,
			std::wstring& filename, 
			std::wstring itemname,
			std::wstring extension)
		{
			wchar_t buffer[MAX_PATH];

			if (!path.length()) {
				xways_.get_case_prop(NULL,
					xways::caseprop_t::DIR,
					&buffer, MAX_PATH);
				path.assign(buffer);
				if (path.length()) path += L"\\_temp\\";
			}
			filename = itemname;

			std::ifstream inputfile(path + filename + extension);
			int index = 0;
			while (!inputfile.fail()) {
				filename = itemname + L"_" + std::to_wstring(index);
				inputfile = std::ifstream(path + filename + extension);
				index++;
			}
			inputfile.close();
		}

		/*!
		@brief Escape characters in a string that would cause problems in
		the html

		This method can be called by derived classes to escape any
		characters in a string that might cause problems in the html file
		e.g. because it uses special characters such as "<" or ">".  It is
		intelligent enough to avoid escaping characters that are already
		escaped html characters.

		@param[in,out] str  reference to the source string object which
		will be modified to insert escape sequences in place

		@return reference to the modified string object
		*/
		static std::string &escape(std::string& str)
		{
			size_t pos, begin, end;
			std::string temp;
			static const std::string codes[] = { "euro","nbsp","quot",
				"amp","lt","gt","iexcl","cent","pound","curren","yen",
				"brvbar","sect","uml","copy","ordf","not","shy","reg",
				"macr","deg","plusmn","sup2","sup3","acute","micro",
				"para","middot","cedil","sup1","ordm","raquo","frac14",
				"frac12","frac34","iquest","Agrave","Aacute","Acirc",
				"Atilde","Auml","Aring","AElig","Ccedil","Egrave",
				"Eacute","Ecirc","Euml","lgrave","lacute","lcirc","luml",
				"ETH","Ntilde","Ograve","Oacute","Ocirc","Otilde","Ouml",
				"times","Oslash","Ugrave","Uacute","Ucirc","Uuml",
				"Yacute","THORN","szlig","agrave","aacute","acirc",
				"atilde","auml","aring","aelig","ccedil","egrave",
				"eacute","ecirc","euml","igrave","iacute","icirc","iuml",
				"eth","ntilde","ograve","oacute","ocirc","otilde","ouml",
				"divide","oslash","ugrave","uacute","ucirc","uuml",
				"vacute","thorn" };
			std::string::iterator it = str.begin();
			while (it != str.end()) {
				switch (*it) {
				case '&':
					begin = distance(str.begin(), it) + 1;
					end = str.find_first_of(';', begin) - begin;
					temp = str.substr(begin, end);
					if (temp.length() >= 1 && temp.length() <= 6) {
						if (temp[0] == '#') {
							try {
								int val = stoi(temp.substr(1));
								if ((val >= 32 && val <= 383) ||
									val == 8482 ||
									val == 8203)
								{
									std::advance(it, temp.length() + 1);
									continue;
								}
							}
							catch (...) {}
						}
						else {
							bool abort = false;
							for (const std::string &code : codes) {
								if (temp == code) {
									std::advance(it, code.length() + 1);
									abort = true;
									break;
								}
							}
							if (abort) continue;
						}
					}
					pos = distance(str.begin(), it) + 1;
					str.insert(pos, "amp;", 0, 4);
					it = std::next(str.begin(), pos + 3);
					break;
				case '<':
					pos = distance(str.begin(), it);
					str.replace(pos, 1, "&lt;", 0, 4);
					it = std::next(str.begin(), pos + 3);
					break;
				case '>':
					pos = distance(str.begin(), it);
					str.replace(pos, 1, "&gt;", 0, 4);
					it = std::next(str.begin(), pos + 3);
					break;
				}
				it++;
			}
			return str;
		}

		/*!
		@brief Insert unicode zero with space escape characters at regular
		intervals in URL strings

		This method can be called by derived classes to detect URLs and
		insert unicode zero with space escape characters at regular
		intervals to provide line break opportunities.  Not currently used
		by any X-Tension as there are better ways to do this (using this
		will interfere with text searching in html representaitons.

		@param[in,out] str  reference to the source string object which
		will be modified to insert escape sequences in place

		@param[in] linebreak  interval in characters (default = 40)

		@return reference to the modified string object
		*/
		static std::string &hyphenateURL(
			std::string &str,
			unsigned linebreak = 40)
		{
			if (linebreak && str.length() > linebreak) {
				size_t pos = 0, lastPos = 0, codeStart = 0, codeEnd = 0;
				bool urlChecked = false;
				while (pos != string::npos) {
					size_t strLen = str.length();
					pos = str.find_first_of(" \n\r\f\t&", ++pos);
					// Check for escape code - if it is whitespace then
					// reset the count otherwise skip it so that a line
					// break won't occur in the middle of a code
					if (pos != string::npos &&
						str[pos] == '&' &&
						pos + 7 < strLen) 
					{
						pos++;

						// Check for unicode whitespace characters
						if (str[pos] == '#' &&
							str[pos + 1] == '8' &&
							str[pos + 5] == ';')
						{
							if ((str[pos + 2] == '1' &&
								str[pos + 3] == '9' &&
								(str[pos + 4] >= '6' &&
									str[pos + 4] <= '9')) ||
								(str[pos + 2] == '2' &&
									str[pos + 3] == '0' &&
									(str[pos + 4] >= '0' &&
										str[pos + 4] <= '2'))) 
							{
								lastPos += 5;
								continue;
							}
						}

						// Check for html whitespace characters
						if (str[pos] == 'e' &&
							(str[pos + 1] == 'n' || str[pos + 1] == 'm') &&
							str[pos + 2] == 's' && 
							str[pos + 3] == 'p' &&
							str[pos + 4] == ';') 
						{
							lastPos += 5;
							continue;
						}
						if (str[pos] == 't' && 
							str[pos + 1] == 'h' &&
							str[pos + 2] == 'i' &&
							str[pos + 3] == 'n' &&
							str[pos + 4] == 's' &&
							str[pos + 5] == 'p' && 
							str[pos + 6] == ';') 
						{
							lastPos += 7;
							continue;
						}

						// Check for other html codes - if found mark the
						// beginning and end of it
						else {
							size_t skip = str.find_first_of(';', pos);
							if (skip != string::npos && skip < pos + 10) {
								codeStart = pos;
								pos = skip;
								codeEnd = pos;
							}
						}
					}
					if ((pos == string::npos &&
						strLen - lastPos > linebreak) ||
						(pos != string::npos && pos - lastPos > linebreak))
					{
						// Break every MAX_LINE characters
						while (lastPos + linebreak < pos &&
							lastPos + linebreak < strLen)
						{
							lastPos += linebreak;

							// Check we aren't in the middle of a code
							if (lastPos >= codeStart &&
								lastPos < codeEnd)
							{
								lastPos = codeStart - 1;
							}
							str.insert(lastPos, "&#8203;");
							if (lastPos <= codeStart) codeStart += 4;
							strLen += 4;
							lastPos += 4;
						}
					}
				}
			}
			return str;
		}

		/*!
		@brief Determine the character encoding of the supplied buffer and
		the start of the data

		This method can be called by derived classes to determine the
		character encoding of a buffer containing text based on the byte
		order mark and also the starting offset of the data (i.e. the
		location of the first character after the byte order mark)

		@param[in] buffer  reference to a [**std::vector<char>**]
		(http://www.cplusplus.com/reference/vector/vector/)
		containing the buffer

		@param[out] offset  reference to a integer to recieve the offset
		of the first character after the byte order mark

		@return encoding in use, one of encoding_t
		*/
		uint32_t encoding(std::vector<char>&buffer, int& offset)
		{
			offset = 0;
			if (buffer.size() < 2) {
				if (isUTF8(buffer)) return encoding_t::UTF8;
				else return encoding_t::UTFUNK;
			}
			else if (*(uint16_t*)&buffer[0] == 0xFEFF) {
				offset = 2;
				return encoding_t::UTF16LE;
			}
			else if (*(uint16_t*)&buffer[0] == 0xFFFE) {
				offset = 2;
				return encoding_t::UTF16BE;
			}
			if (buffer.size() < 4) {
				if (isUTF8(buffer)) return encoding_t::UTF8;
				else return encoding_t::UTFUNK;
			}
			else if ((*(uint32_t*)&buffer[0] & 0xFFFFFF) == 0xBFBBEF) {
				return encoding_t::UTF8;
			}
			else if (*(uint32_t*)&buffer[0] == 0x0000FEFF) {
				offset = 4;
				return encoding_t::UTF32LE;
			}
			else if (*(uint32_t*)&buffer[0] == 0xFFFE0000) {
				offset = 4;
				return encoding_t::UTF32BE;
			}
			else if (isUTF8(buffer)) return encoding_t::UTF8;
			else return encoding_t::UTFUNK;
		}

		/*!
		@brief Determine if the supplied buffer is encoded in UTF8 based
		on its content

		This method can be called by derived classes to determine if the
		character encoding of a buffer containing text is UTF8 based on
		the content.  It is only possible to reliably detect UTF8 in this
		way.

		@param[in] buffer  reference to a [**std::vector<char>**]
		(http://www.cplusplus.com/reference/vector/vector/) containing
		the buffer

		@return true if UTF8 encoding is in use, false otherwise
		*/
		bool isUTF8(std::vector<char>&buffer)
		{
			int code_length;
			uint32_t ch;
			size_t size = buffer.size();

			for (size_t i = 0; i < size;) {
				auto byte = buffer[i];
				if (byte <= 0x7F) {
					/* 1 byte sequence: U+0000..U+007F */
					if (!byte && i < size - 1) {
						return false;  // NUL (UTF-16/32??)
					}
					i++;
					continue;
				}

				if (0xC2 <= byte && byte <= 0xDF)
					/* 0b110xxxxx: 2 bytes sequence */
					code_length = 2;
				else if (0xE0 <= byte && byte <= 0xEF)
					/* 0b1110xxxx: 3 bytes sequence */
					code_length = 3;
				else if (0xF0 <= byte && byte <= 0xF4)
					/* 0b11110xxx: 4 bytes sequence */
					code_length = 4;
				else {
					/* invalid first byte of a multibyte character */
					return false;
				}

				if (i + (code_length - 1) >= buffer.size()) {
					/* truncated string or invalid byte sequence */
					return false;
				}

				/* Check continuation bytes: bit 7 should be set, bit 6
				 * should be unset (b10xxxxxx). */
				for (auto j = 1; j < code_length; j++) {
					if ((buffer[i + j] & 0xC0) != 0x80)
						return false;
				}

				if (code_length == 2) {
					/* 2 bytes sequence: U+0080..U+07FF */
					ch = ((buffer[i] & 0x1f) << 6) +
						(buffer[i + 1] & 0x3f);
				}
				else if (code_length == 3) {
					/* 3 bytes sequence: U+0800..U+FFFF */
					ch = ((buffer[i] & 0x0f) << 12) + 
						((buffer[i + 1] & 0x3f) << 6) +
						(buffer[i + 2] & 0x3f);
					if (ch < 0x0800) return false;

					/* surrogates (U+D800-U+DFFF) are invalid in UTF-8:
					test if (0xD800 <= ch && ch <= 0xDFFF) */
					if ((ch >> 11) == 0x1b) return false;
				}
				else if (code_length == 4) {
					/* 4 bytes sequence: U+10000..U+10FFFF */
					ch = ((buffer[i] & 0x07) << 18) +
						((buffer[i + 1] & 0x3f) << 12) +
						((buffer[i + 2] & 0x3f) << 6) +
						(buffer[i + 3] & 0x3f);
					if ((ch < 0x10000) || (0x10FFFF < ch)) return 0;
				}
				i += code_length;
			}
			return true;
		}

		/*!
		@brief Swap big endian buffer to little endian

		This method can be called by derived classes to swap a buffer from
		big endian to little endian. It will work for any size type but it
		only makes sense to use it for types larger than one byte.

		@param[in,out] buffer  reference to a [**std::vector<char>**]
		(http://www.cplusplus.com/reference/vector/vector/)
		containing the buffer to be modified in place

		@param[in] type  the type of the data contained in the buffer
		(required to instantiate the correct template version) e.g.
		**char16_t()**

		@return reference to the modified buffer
		*/
		template<typename T>
		std::vector<char>& endian_swap(std::vector<char>&buffer, T type)
		{
			uint8_t byte;
			for (size_t i=0; i<buffer.size()-sizeof(T)+1; i+=sizeof(T))
			{
				for (size_t j = 0; j < sizeof(T) / 2; j++) {
					byte = buffer[i + j];
					buffer[i + j] = buffer[i + sizeof(T) - j - 1];
					buffer[i + sizeof(T) - j - 1] = byte;
				}
			}
			return buffer;
		}

		/*!
		@brief disable/enable floating point interrupts

		This method can be called by derived classes to disable floating
		point interrupts (saving the environment first) and to restore the
		previous floating point environment.  It is used by the base class
		methods @a create and @a finalise so it is not necessary for
		derived classes to call it directly if these methods are used.

		Implemented outside main class definition due to \#pragma, which
		must have global or namespace scope.

		@param[in] enable  false to disable interrupts, true to restore
		the previous environment
		*/
		static void disable_fp_interrupts(bool enable);

		/*!
		@brief X-Ways object to use for API calls
		*/
		xways::api &xways_;

		/*!
		@brief Options for this X-Tension
		*/
		options_t options;

		/*!
		@brief File to be analysed
		*/
		xways::item_stream<> item;

		/*!
		@brief Flag to indicate to the @a create method that it was called
		by attach()
		*/
		bool attaching = false;

		/*!
		@brief Time the analysis was started (used the base class
		create()/finalise() to calculate the analysis time)
		*/
		std::chrono::system_clock::time_point start;

		/*!
		@brief Operation being conducted by X-Ways
		*/
		xways::action_t operation;
	};

#pragma fenv_access (off)
	template<typename options_t = options>
	void view<options_t>::disable_fp_interrupts(bool enable)
	{
		static fenv_t fe;
		if (enable) feholdexcept(&fe);
		else fesetenv(&fe);
	}
#pragma fenv_access (off)


	/*!
	@brief Base class for html style viewer/regular X-Tensions

	This class is not intended to be used directly, instead derive from
	this class to implement the fuctionality of html style viewer/regular
	X-Tensions.  To derive from this class simply impement an override for
	create() that first checks to see if it is appropriate ot handle
	the target and then actaully parses the target.

	It should not normally be necessary to override the other public
	methods attach() and release().

	The protected methods are available to assist in the creation of a
	html representation but there use is optional.  The main methods are
	add_table() and finalize().  Use add_table() to create a html section
	(there must be at least one), which is a [**std::stringstream**]
	(http://www.cplusplus.com/reference/stringstream/stringstream/)
	object in the @ref tables collection.  Then add html text to the 
	[**std::stringstream**]
	(http://www.cplusplus.com/reference/stringstream/stringstream/)
	objects representing each section and when done, call finalise()
	to assemble them ito a buffer.  See the documentation on these methods
	for further information.

	The remaining protected methods inherited from the base class @ref
	view are general utility functions - see their respective
	documentation for details.

	To subclass this class add something similar to the following
	declaration to a header file (if not using custom options then the
	template parameter can be omitted i.e. replace `<custom_options>` with
	`<>`):
	\includelineno "html_view_custom.h"

	Then override create() by adding something similar to the following
	definition to a source file:
	\includelineno "html_view_custom.cpp"

	Finally, modify dllmain.cpp to use your new view class instead by
	changing this line:

	```cpp
	xtension::html_view<> view(xways_api);
	```

	to:

	```cpp
	custom_view view(xways_api);
	```

	*/
	template<typename options_t = html_options>
	class html_view : public view<options_t> {
	public:

		using view::view;

	protected:

		/*!
		@brief Create a new [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object to contain a section of the html representation

		This can be used to simplify creating a html representation by
		using [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		objects. The html representation then consists of one or more
		sections that are each maintained in a [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object - with all of the [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		objects being contained in a [**std::vector**]
		(http://www.cplusplus.com/reference/vector/vector/).  The first
		section (contained in the first [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object) also contains the html header. This enables the sections
		to be created independently and then assembled into one html file
		at the end by simply concatenating the [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		objects together.

		This method constructs a new [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object and adds it to the collection contained in the @ref tables
		vector. If it is the first [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object then the html header is also added.

		If using this method the derived class should also call 
		finalize() to assemble the html representation after it has
		finished populating the [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		objects.

		@return zero based offset of the section just added
		*/
		size_t add_table()
		{
			tables.push_back(std::stringstream());

			size_t current = tables.size() - 1;

			if (!current) {
				tables[current] << 
					"<!DOCTYPE html>\n<html>\n"
					"<head>\n<meta http-equiv=\"X-UA-Compatible\" "
					"content=\"IE=edge\">\n"
					"<meta http-equiv=\"Content-Type\" "
					"content=\"text/html; charset=UTF-8\">\n<style>";
				tables[current] << options.style;
				tables[current] << "</style>\n<script>";
				tables[current] << options.headscript;
				tables[current] << "</script>\n</head>\n<body>\n";
			}
			return current;
		}

		/*!
		@brief Close off the html sections in each [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		and assemble them

		Should be called by the derived @a create method to close off the
		html sections in each [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		object after the file has been examined and immediately before the
		buffer is returned to X-Ways.  This method writes whatever is
		contained in the @ref stream_close string object to the end of
		each [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		and for the last [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		only it also writes the html footer.  It then assembles the
		sections into one buffer.

		This method additionally restores the previous floating point
		exception behaviour and outputs debug messages if enabled before
		returning a pointer to the buffer.

		@param[out] size  reference to the size of the buffer

		@return pointer to the buffer containing the html representation
		*/
		void *finalize(int64_t& size)
		{
			size = 0;
			for (size_t i = 0; i < tables.size(); i++) {
				tables[i] << stream_close;
				if (i == tables.size() - 1) {
					tables[i] << "<script>" << options.bodyscript;
					tables[i] << "</script>\n</body>\n</html>\n";
				}
				else {
					tables[i] << "<br>\n";
				}
				size += tables[i].tellp();
			}
			if (!(size)) {
				size = -1;
				return 0;
			}
			char * buffer = new char[((unsigned int)size) + 1];
			size = 0;
			for (size_t i = 0; i < tables.size(); i++) {
				tables[i].read(buffer + size, tables[i].tellp());
				size += tables[i].tellp();
			}
			*(buffer + size) = 0;

			// Restore whatever crazy settings X-Ways uses
			disable_fp_interrupts(false);

			if (options.debug) {
				std::chrono::duration<double> elapsed =
					std::chrono::system_clock::now() - start;
				std::wstring message(
					XT_PREFIX": Examination of file with ID ");
				message += std::to_wstring(item.id());
				message += L" using " + options.classname;
				message += L" parser took ";
				message += std::to_wstring(elapsed.count()) + L" s";
				xways.output_message(message.c_str(), 0);
			}

			return buffer;
		}

		/*!
		@brief Vector of [**std::stringstream**]
		(http://www.cplusplus.com/reference/stringstream/stringstream/)
		objects to hold the html sections (if used)
		*/
		std::vector<std::stringstream> tables;

		/*!
		@brief String to be added to the end of each section (default =
		"</table>")
		*/
		std::string stream_close = "</table>\n";
	};

	/*!
	@brief Base exception class

	This is the base class from which the two main exception classes @ref
	nonfatal_error and @ref fatal_error are derived.  This class is not
	intended to be used directly.

	Constructors for this class take a message string (either **char\***
	or [**std::string**]
	(http://www.cplusplus.com/reference/string/string/)), the ID of the
	file being examined and optionally, the offset to the location causing
	the error.  Two methods are implemented, @ref what(), which returns a
	string suitable for display in the X-Ways message log window, and @ref
	where(), which returns the offset (if applicable).
	*/
	class error : public std::exception {
	protected:
		std::string message_;	//!< Error message to be returned by @ref
								//!  what()
		uint64_t offset_;		//!< Offset to be returned by @ref where()
		uint64_t id_;			//!< Item ID to be included in the message
								//!  returned by @ref what()
	public:
		/*!
		@brief Construct from a **const char\*** error message and an
		offset

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined

		@param[in] offset  the location in the file where the error
		occurred
		*/
		explicit error(const char* message, uint64_t id, uint64_t offset) :
			error(std::string(message), id, offset) {}

		/*!
		@brief Construct from a [**std::string**]
		(http://www.cplusplus.com/reference/string/string/) reference
		error message and an offset

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined

		@param[in] offset  the location in the file where the error
		occurred
		*/
		explicit error(
			const std::string & message,
			uint64_t id,
			uint64_t offset) :
			message_(
				XT_PREFIX_A": " +
				message +
				" in file with ID: " +
				std::to_string(id) +
				", at offset: " +
				std::to_string(offset)),
			id_(id),
			offset_(offset) {}

		/*!
		@brief Construct from a **const char\*** error message (no offset)

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined
		*/
		explicit error(const char* message, uint64_t id) :
			error(std::string(message), id) {}

		/*!
		@brief Construct from a [**std::string**]
		(http://www.cplusplus.com/reference/string/string/)	reference
		error message (no offset)

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined
		*/
		explicit error(
			const std::string & message,
			uint64_t id) :
			message_(
				XT_PREFIX_A": " +
					message +
					" in file with ID: " +
					std::to_string(id)),
			id_(id) {}

		/*!
		@brief Destructor (rethrows)
		*/
		virtual ~error() throw () {}

		/*!
		@brief Get the error message

		@return **const char\*** to the error message
		*/
		virtual const char* what() const throw ()
		{
			return message_.c_str();
		}

		/*!
		@brief Get the offset to the error location

		@return offset to the error location
		*/
		virtual uint64_t where() const throw () { return offset_; }
	};

	/*!
	@brief Derived exception class for fatal error handling

	This is the derived exception class intended to be thrown in the case
	of fatal errors.

	Constructors (inherited from the base class) take a message string
	(either **char\*** or [**std::string**]
	(http://www.cplusplus.com/reference/stringstream/stringstream/)),
	the ID of the file being examined and, optionally, the offset to the 
	location causing the error.  Two methods are implemented, @ref what(),
	which returns a string suitable for display in the X-Ways message log
	window, and @ref where(), which returns the offset (if applicable).
	*/
	class fatal_error : public error { using error::error; };

	/*!
	@brief Derived exception class for non-fatal error handling

	This is the derived exception class intended to be thrown in the case
	of non-fatal errors.

	Constructors (inherited from the base class) take a message string
	(either **char\*** or [**std::string**]
	(http://www.cplusplus.com/reference/string/string/)), the ID of the
	file being examined and, optionally, the offset to the location 
	causing the error.  Two methods are implemented, @ref what(), which
	returns a string suitable for display in the X-Ways message log
	window, and @ref where(), which returns the offset (if applicable).
	*/
	class nonfatal_error : public error { using error::error; };
}