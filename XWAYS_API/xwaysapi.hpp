#pragma once

// Copyright (C) 2016 Trevor Welsby, derived from the official
// X-Ways API documentation at:
// https://www.x-ways.net/forensics/x-tensions/api.html
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file xwaysapi.hpp Base X-Ways API and stream wrapper implementation

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <istream>
#include <streambuf>
#include <sstream>
#include <vector>
#include <algorithm>
#include <codecvt>
#include <memory>
#include "VER.H"

//! X-Tension DLL exports macro
#define XT_API extern "C" __declspec(dllexport)

//! Default X-Tension version string (if not defined in VER.H)
#if !defined(XT_VERSION)
#define XT_VERSION L"0.0.0.0"
#endif

//! Platform string (used in the X=Ways messages window and about box)
#ifdef _WIN64
#define XT_PLATFORM L" (64 bit)"
#else
#define XT_PLATFORM L" (32 bit)"
#endif

/*!
@brief Basic X-Ways API functionality
*/
namespace xways {

	////////////////////
	// Enumumerations //
	////////////////////

	/*!
	@brief Flags passed to the @a flags parameter of XT_Init()
	*/
	enum class init_flag_t : uint32_t {
		XWF = 0x01,				//!< X-Ways Forensics
		WHX = 0x02,				//!< WinHex
		XWI = 0x04,				//!< X-Ways Investigator
		BETA = 0x08,			//!< Beta version
		QUICKCHECK = 0x20,		//!< Called to check whether the API
								//!  accepts the calling application
								//!  @version 16.5
		ABOUTONLY = 0x40		//!< Called to prepare for @ref XT_About()
								//!  @version 16.5
	};

	/*!
	@brief Licence types passed to @ref XT_Init() as part of the
	xways::licence_info_t struct
	*/
	enum class licence_t : uint32_t {
		LICENCED = 0x01,		//!< Unlocked / licensed version
								//!  (if this flag is not set, the
								//!  other flags are not
								//!  guaranteed to be valid)
		EXP_DATE = 0x02,		//!< Expiration date field is
								//!  populated
		NON_PERPETUAL = 0x04,	//!< Non-perpetual license (not
								//!  only the update maintenance,
								//!  but the license itself will
								//!  expire on the expiration
								//!  date)
		HARDWARE = 0x10,		//!< The software is unlocked by a
								//!  hardware device (dongle or
								//!  BYOD)
		DONGLE = 0x20,			//!< The hardware device is a
								//!  dongle
		NET_DONGLE = 0x40		//!< The dongle is a network
								//!  dongle
	};

	/*!
	@brief Operation types passed to @ref XT_Prepare() via the @a
	operation parameter
	*/
	enum class action_t : uint32_t {
		RUN = 0,				//!< Simply run directly from the main
								//!  menu, not for any particular volume,
								//!  @version 16.6
		RVS = 1,				//!< Volume snapshot refinement starting
		LSS = 2,				//!< Logical simultaneous search starting
		PSS = 3,				//!< Physical simultaneous search starting
		DBC = 4,				//!< Directory browser context menu
								//!  command invoked
		SHC = 5					//!< Search hit context menu command
								//!  invoked
	};

	/*!
	@brief Possible return values from @ref XT_Prepare()

	@note Skip and abort operations are only supported for @ref
	action_t::RVS operations
	*/
	enum class prepare_t : int32_t {
		ABORT_OPERATION = -4,	//!< Stop the whole operation (e.g.
								//!  volume snapshot refinement)
								//! altogether
		SKIP_OPERATION = -3,	//!< Prevent further use of the
								//!  X-Tension for the remainder of
								//!  the whole operation
		ABORT_VOLUME = -2,		//!< Exclude the current volume
								//!  from the operation
		SKIP_VOLUME = -1,		//!< Prevent other functions of this
								//!  X-Tension being called for this
								//!  volume, not even XT_Finalize()
		FINALISE = 0x00,		//!< Just call XT_Finalize()
		CALLPI = 0x01,			//!< Call for each item in the volume
								//!  snapshot
		CALLPILATE = 0x02,		//!< Call for each item in the volume
								//!  snapshot after other refinement
								//!  operations
		EXPECTMOREITEMS = 0x04, //!< Signal that more items may be
								//!  added to the volume snapshot
		DONTOMIT = 0x08,		//!< Don't omit files that the user
								//!  wants to omit
		TARGETDIRS = 0x10		//!< Target directories not just files
	};

	/*!
	@brief Evidence object types
	*/
	enum class evobj_type_t : uint32_t {
		FILE = 0,				//!< File
		IMAGE = 1,				//!< Image
		MEM_DUMP = 2,			//!< Memory dump
		DIR_PATH = 3,			//!< Directory/path
		DISK = 4				//!< Disk
	};

	/*!
	@brief Open evidence object flags

	@version 18.1
	*/
	enum class open_evobj_flag_t : uint32_t {
		IGNORE_DISK = 0x01,		//!< Open the evidence object without
								//!  opening or even touching/looking for
								//!  the underlying disk or image
		READ_ONLY = 0x02		//!< Open the volume snapshot read-only
	};

	/*!
	@brief Flags that may be returned by
	xways::api::get_report_table_info()

	@version 18.1
	*/
	enum class report_table_flag_t : uint32_t {
		INTERNAL = 0x01,		//!< Created internally by application
		USER = 0x02,			//!< Created by the user
		FILTERING = 0x04,		//!< Select for filtering
		MANUAL = 0x08,			//!< Select for future manual report
								//!  table associations
		INCLUDE = 0x10,			//!< Selected for inclusion in the case
								//!  report
		FILTER = 0x20,			//!< Selected for the report table filter
		SEARCH = 0x80			//!< Represents a search term
	};

	/*!
	@brief Item information types passed to
	xways::api::get_item_information() via the @a info_type parameter
	*/
	enum class item_info_t : int32_t {
		ORIG_ID = 1,				//!< Original ID
		ATTR = 2,					//!< Attributes
		FLAGS = 3,					//!< Flags
		DELETION = 4,				//!< Deleted
		CLASSIFICATION = 5,			//!< e.g. extracted e-mail message,
									//!  alternate data stream, etc.
		LINKCOUNT = 6,				//!< Hard-link count
		COLORANALYSIS = 7,			//!< Skin color percentage or...
									//!   0: n/a, -2: error,-3:
									//!  b/w/grayscale, -4: irrelevant,
									//!  @version 17.2
		PIXELCOUNT = 8,				//!< Indicator of the pixel count
									//!  of a raster image. It is the
									//!  square root of width * height
									//!  in pixels, divided by 20, or:
									//!   0: not yet computed or not a
									//!  picture. 1: <= 0,02 KP. 254 =
									//!  16.5 MP. 255 (maximum) = even
									//!  larger, @version 18.9
		FILECOUNT = 11,				//!< How many child objects exist
									//!  recursively that are files
		EMBEDDEDOFFSET = 16,		//!< For a file linearly embedded
									//!  within another file, offset
									//!  in that file, @version 17.7
		CREATIONTIME = 32,			//!< Creation time
		MODIFICATIONTIME = 33,		//!< Modification time
		LASTACCESSTIME = 34,		//!< Last access time
		ENTRYMODIFICATIONTIME = 35,	//!< Entry modification time
		DELETIONTIME = 36,			//!< Deletion time
		INTERNALCREATIONTIME = 37,	//!< Internal creation time
		FLAGS_SET = 64,				//!< Indicates only flags that
									//!  should be set, others remain
									//!  unchanged
		FLAGS_REMOVE = 65			//!< Indicates flags that should
									//!  be removed, others remain
									//!  unchanged
	};

	/*!
	@brief Flags returned by xways::api::get_item_information() when the
	xways::item_info_t::FLAGS information type is selected
	*/
	enum class item_info_flag_t : int64_t {
		DIRECTORY = 0x00000001,			//!< Is a directory
		HAS_CHILDREN = 0x00000002,		//!< Has child objects (for
										//!  files)
		HAS_SUBDIRS = 0x00000004,		//!< Has subdirectories (for
										//!  directories)
		VIRTUAL = 0x00000008,			//!< Is a virtual item
		HIDDEN = 0x00000010,			//!< Hidden by examiner
		TAGGED = 0x00000020,			//!< Tagged
		PART_TAGGED = 0x00000040,		//!< Tagged partially
		VIEWED = 0x00000080,			//!< Viewed by examiner
		FS_N_UTC = 0x00000100,			//!< File system timestamps
										//!  not in UTC
		CREATE_N_UTC = 0x00000200,		//!< Internal creation
										//!  timestamp not in UTC
		FAT_TIMESTAMPS = 0x00000400,	//!< FAT* timestamps
		NTFS_ORIGIN = 0x00000800,		//!< Originates from NTFS
		UNIX_ATTRIB = 0x00001000,		//!< UNIX world attributes
		HAS_COMMENT = 0x00002000,		//!< Has examiner comment
		HAS_METADATA = 0x00004000,		//!< Has extracted metadata
		CONT_UNK = 0x00008000,			//!< File contents totally
										//!  unknown
		CONT_PART_UNK = 0x00010000,		//!< File contents partially
										//!  unknown
		RESERVED = 0x00020000,			//!< Reserved
		HASH1_DONE = 0x00040000,		//!< Hash 1 already computed
		HAS_DUPLICATES = 0x00080000,	//!< Has duplicates
		HASH2_DONE = 0x00100000,		//!< Hash 2 already computed
										//!  @version 18.0
		KNOWN_GOOD_HASH = 0x00200000,	//!< Known good hash category
		KNOWN_BAD_HASH = 0x00400000,	//!< Known bad hash category
		FOUND_IN_VSC = 0x00800000,		//!< Found in volume shadow
										//!  copy
		DELETED_ORG_CONT = 0x01000000,	//!< Deleted files with known
										//!  original contents
		FORMAT_CONSIST = 0x02000000,	//!< File format consistency
										//!  OK
		FORMAT_N_CONSIST = 0x04000000,	//!< File format consistency
										//!  not OK
		ARCH_EXPLORED = 0x10000000,		//!< File archive already
										//!  explored @version 17.6
		EAV_PROCESSED = 0x20000000,		//!< Email archive or video
										//!  already processed
										//!  @version 17.6
		EMBEDDED_DONE = 0x40000000,		//!< Embedded data already
										//!  uncovered @version 17.6
		METADATA_DONE = 0x80000000,		//!< Metadata extraction
										//!  already applied
										//!  @version 17.6
		FILE_EMBEDDED = 0x100000000,	//!< File embedded in other
										//!  file linearly
										//!  @version 17.7
		STORED_EXT = 0x200000000,		//!< File whose contents is
										//!  stored externally
										//!  @version 17.7
	};

	/*!
	@brief Delete status flags returned by
	xways::api::get_item_information() when the
	xways::item_info_t::DELETION information type is selected
	*/
	enum class delete_status_t : int64_t {
		EXISTING = 0,			//!< Existing
		PREV_EXIST_REC = 1,		//!< Previously existing, possibly
								//!  recoverable
		PREV_EXIST_N_REC = 2,	//!< Previously existing, first
								//!  cluster overwritten or unknown
		REN_MOV_REC = 3,		//!< Renamed/moved, possibly
								//!  recoverable
		REN_MOV_N_REC = 4,		//!< Renamed/moved, first cluster
								//!  overwritten or unknown
	};

	/*!
	@brief Classification flags returned by
	xways::api::get_item_information() when the
	xways::item_info_t::CLASSIFICATION information type is selected
	*/
	enum class class_flag_t : int64_t {
		NORMAL_FILE = 0x00,		//!< Normal file
		HFS_RES_FORK = 0x04,	//!< HFS resource fork
		NTFS_ALT_DATA = 0x08,	//!< NTFS alternate data stream
		NTFS_N_DIR_INDEX = 0x0A,//!< NTFS non-directory index
		NTFS_BITMAP = 0x0B,		//!< NTFS bitmap attribute
		NTFS_GEN_LOG = 0x10,	//!< NTFS general logged utility
								//!  stream
		NTFS_EFS_LOG = 0x11,	//!< NTFS EFS logged utility stream
		EMAIL_RELATED = 0xF5,	//!< Email related
		EXCERPT = 0xF6,			//!< Excerpt
		ATTACHED = 0xF7,		//!< Manually attached
		VIDEO_STILL = 0xF8,		//!< Video still
		EMAIL_ATTACH = 0xF9,	//!< Email attachment
		EMAIL_MESSAGE = 0xFA,	//!< Email message
		INDX_REC_REMNANT = 0xFD,//!< INDX record remnant
		SESSION_ROOT = 0xFE,	//!< Session root directory in
								//!  CDFS/UDF
	};

	/*!
	@brief Status of the file type returned by xways::api::get_item_type()
	*/
	enum class type_status_t : int32_t {
		STATUS_ERROR = -1,		//!< Error
		NOT_VERIFIED = 0,		//!< Not verified
		TOO_SMALL = 1,			//!< Too small
		UNKNOWN = 2,			//!< Totally unknown
		CONFIRMED = 3,			//!< Confirmed
		NOT_CONFIRMED = 4,		//!< Not confirmed
		NEWLY_IDENTIFIED = 5,	//!< Newly identified (or greater)
	};

	/*!
	@brief Search options that may be set via xways::search_info_t::flags
	*/
	enum class search_option_t {
		LOGICAL = 0x00000001,		//!< Logical search instead of
									//!  physical search (only logical
									//!  search currently available)
		TAGGEDOBJ = 0x00000004,		//!< Tagged objects in volume
									//!  snapshot only
		MATCHCASE = 0x00000010,		//!< Match case
		WHOLEWORDS = 0x00000020,	//!< Whole words only
		GREP = 0x00000040,			//!< GREP syntax
		OVERLAPPED = 0x00000080,	//!< Allow overlapping hits
		COVERSLACK = 0x00000100,	//!< Cover slack space
		COVERSLACKEX = 0x00000200,	//!< Cover slack/free space
									//!  transition
		DECODETEXT = 0x00000400,	//!< Decode text in standard file
									//!  types
		DECODETEXTEX = 0x00000800,	//!< Decode text in specified file
									//!  types (not yet supported)
		ONEHITPERFILE = 0x00001000,	//!< 1 hit per file needed only
		OMITIRRELEVANT = 0x00010000,//!< Omit files classified as
									//!  irrelevant
		OMITHIDDEN = 0x00020000,	//!< Omit hidden files
		OMITFILTERED = 0x00040000,	//!< Omit files that are filtered
									//!  out
		DATAREDUCTION = 0x00080000, //!< Recommendable data reduction
		OMITDIRS = 0x00100000,		//!< Omit directories
		CALLPSH = 0x01000000,		//!< Call XT_ProcessSearchHit, if
									//!  exported, for each hit
									//!  @version 16.8 SR-5
		DISPLAYHITS = 0x04000000	//!< Display search hit list when
									//!  the search completes
	};

	/*!
	@brief Search flags sent via xways::prepare_search_info_t::flags to
	XT_PrepareSearch()
	*/
	enum class search_flag_t {
		MATCHCASE = 0x00000010,		//!< Match case
		WHOLEWORDS = 0x00000020,	//!< Whole words only
		GREP = 0x00000040,			//!< GREP syntax
		WHOLEWORDS2 = 0x00004000,	//!< Whole words only for search
									//!  terms that are specially marked
		SEARCH_GREP2 = 0x00008000	//!< GREP syntax only search terms
									//!  that start with "grep:"
	};

	/*!
	@brief Flags to be supplied to xways::api::create_container()
	*/
	enum class ctr_create_flag_t : uint32_t {
		OPEN = 0x0001,				//!< Opens an existing container,
									//!  all other flags ignored
		XWFS2 = 0x0002,				//!< Use new XWFS2 file system
		SECURE = 0x0004,			//!< Mark this container to be
									//!  filled indirectly/secure
		TOPLEVEL = 0x0008,			//!< Include evidence object names
									//!  as top directory level
		INCLDIRDATA = 0x0010,		//!< Include directory data
		FILEPARENTS = 0x0020,		//!< Allow files as parents of
									//!  files
		USERREPORTTABLES = 0x0100,	//!< Export associations with
									//!  user-created report table
		SYSTEMREPORTTABLES = 0x0200,//!< Export associations with
									//!  system-created report tables
									//!  (currently requires @a
									//!  USERREPORTTABLES)
		ALLCOMMENTS = 0x0800,		//!< Pass on comments
		OPTIMIZE1 = 0x1000,			//!< Optimize for >1,000 items
		OPTIMIZE2 = 0x2000,			//!< Optimize for >50,000 items
		OPTIMIZE3 = 0x4000,			//!< Optimize for >250,000 items
		OPTIMIZE4 = 0x8000			//!< Optimize for >1 million items
	};

	/*!
	@brief Flags to be supplied to xways::api::copy_to_container()
	*/
	enum class ctr_copy_flag_t : uint32_t {
		RECREATE_PATH = 0x01,		//!< Recreate full original path
		INCLUDE_PARENT_DATA = 0x02,	//!< Include parent item data
									//!  (requires @a RECREATE_PATH)
		STORE_HASH = 0x04			//!< Store hash value in container
	};

	/*!
	@brief Mode flags to be supplied to xways::api::copy_to_container()
	*/
	enum class ctr_mode_flag_t : uint32_t {
		LOGICAL = 0,			//!< Copy logical file contents only
		PHYSICAL = 1,			//!< Copy physical file contents (not
								//!  supported)
		LOGICAL_SEP_SLACK = 2,	//!< Logical contents and file slack
								//!  separately
		SLACK_ONLY = 3,			//!< Copy slack only
		RANGE_ONLY = 4,			//!< Copy range only (the last 2
								//!  parameters, which are otherwise
								//!  ignored and should be -1)
		METADATA_ONLY = 5,		//!< Copy metadata only
	};

	/*!
	@brief Case properties able to be requested by
	xways::api::get_case_prop()
	*/
	enum class caseprop_t : int32_t {
		TITLE = 0x01,		//!< Title of the case
		EXAMINER = 0x03,	//!< Name of the examiner
		FILE = 0x05,		//!< Case filename
		DIR = 0x06			//!< Case directory
	};

	/*!
	@brief Evidence object properties able to be retrieved by
	xways::api::get_ev_obj_prop()
	*/
	enum class objprop_t : uint32_t {
		NUMBER = 0,			//!< Evidence object number - **uint16_t**
							//!  returned, @a buffer can be NULL
		ID = 1,				//!< Evidence object ID - **uint32_t**
							//!  returned, @a buffer can be NULL
		PARENT_ID = 2,		//!< Parent evidence object ID (for 
							//!  partitions, 0 if no parent) -
							//!  **uint32_t** returned, @a buffer can NULL
		TITLE = 6,			//!< Evidence object title (e.g.
							//!  "Partition 2") - NULL terminated
							//!  **wchar_t\*** returned @a buffer can be
							//!  NULL
		EXT_TITLE = 7,		//!< Extended evidence object title (e.g.
							//!  "HD123, Partition 2) - returns the
							//!  actual string length and @a buffer
							//!  must point to a buffer @a MAX_PATH
							//!  **wchar_t** characters in length
		ABR_EXT_TITLE = 8,	//!< Abbr. extended evidence object title
							//!  (e.g. "HD123, P2) - returns the 
							//!  actual string length and @a buffer
							//!  must point to a buffer @a MAX_PATH
							//!  **wchar_t** characters in length
		INTERNAL_NAME = 9,	//!< Internal name - NULL terminated
							//!  **wchar_t\*** returned @a buffer can be
							//!  NULL
		DESC = 10,			//!< Description - NULL terminated
							//!  **wchar_t\*** returned @a buffer can be
							//!  NULL
		COMMENTS = 11,		//!< Examiner comments - NULL terminated
							//!  **wchar_t\*** returned @a buffer can be
							//!  NULL
		INTERNAL_DIR = 12,	//!< Internally used directory - returns
							//!  the actual string length and 
							//!  @a buffer must point to a buffer @a
							//!  MAX_PATH **wchar_t** characters in length
		OUTPUT_DIR = 13,	//!< Output directory - returns the
							//!  actual string length and @a buffer
							//!  must point to a buffer @a MAX_PATH
							//!  **wchar_t** characters in length
		SIZE = 16,			//!< Size in bytes - **int16_t** returned,
							//!  @a buffer can be NULL
		FILE_COUNT = 17,	//!< Volume snapshot file count - **uint32_t**
							//!  returned, @a buffer can be NULL
		FLAGS = 18,			//!< Flags - returns #objprop_flag_t,
							//!  @a buffer can be NULL
		FS_ID = 19,			//!< File system identifier - returns
							//!  #fs_type_t, @a buffer can be NULL
		HASH_TYPE = 20,		//!< Hash type - **uint32_t** 
							//!  returned, @a buffer can be NULL
		HASH = 21,			//!< Hash value - returns the size of the
							//!  hash value, @a buffer should point to
							//!  a buffer sized appropriately to the
							//!  hash type
		CREATED = 32,		//!< Creation time (when the evidence
							//!  object was added to the case) -
							//!  returns a **FILETIME** object, @a buffer
							//!  may be NULL
		MODIFIED = 33,		//!< Modification time - returns a
							//!  **FILETIME** object, @a buffer may be
							//!  NULL
		HASH_TYPE2 = 40,	//!< Hash type #2 - **uint32_t** returned,
							//!  @a buffer can be NULL
		HASH2 = 41			//!< Hash value #2 - returns the size of
							//!  the hash value, @a buffer should
							//!  point to a buffer sized appropriately
							//!  to the hash type
	};

	/*!
	@brief Volume snapshot property types that can be requested via
	xways::api::get_vs_prop()
	*/
	enum class vsprop_t : int32_t {
		SPECIALITEMID = 10,	//!< Returns the ID of a @ref 
							//!  xways::special_item_t in the volume
							//!  snapshot, or -1 if the requested special
							//!  item is not present in the volume
							//!  snapshot
		HASHTYPE = 11,		//!< @deprecated details unknown
		HASHTYPE1 = 20,		//!< Retrieves the @ref xways::hash_type_t
							//!  primary hash values that the files in
							//!  the volume snapshot have; @version
							//!  17.8 SR-17, 17.9 SR-10, 18.0 SR-4
							//!  or later; @a buffer is not used
		HASHTYPE2 = 21		//!< Retrieves the @ref xways::hash_type_t of
							//!  the secondary hash values that the files
							//!  in the volume snapshot have,
							//!  @version 18.0 SR-4
	};

	/*!
	@brief Special item IDs
	*/
	enum class special_item_t : uint8_t {
		ROOT_DIR = 1,		//!< Root directory
		PATH_UNKNOWN = 2,	//!< Path Unknown directory
		CARVED_FILES = 4,	//!< Carved Files directory
		FREE_SPACE = 5,		//!< Free Space file
		SYS_VOL_INFO = 11,	//!< System Volume Information directory
		WINDOWS_EDB = 12,	//!< Windows.edb file
	};

	/*!
	@brief Hash types
	*/
	enum class hash_type_t {
		UNDEFINED = 0,		//!< Undefined
		CS8 = 1,			//!< CS8
		CS16 = 2,			//!< CS16
		CS32 = 3,			//!< CS32
		CS64 = 4,			//!< CS64
		CRC16 = 5,			//!< CRC16
		CRC32 = 6,			//!< CRC32
		MD5 = 7,			//!< MD5
		SHA1 = 8,			//!< SHA-1
		SHA256 = 9,			//!< SHA-256
		RIPEMD128 = 10,		//!< RIPEMD-128
		RIPEMD160 = 11,		//!< RIPEMD-160
		MD4 = 12,			//!< Md4
		ED2K = 13,			//!< eDonkey2000
		ADLER32 = 14,		//!< Adler-32
		TIGER = 15,			//!< Tiger Tree Hash @version 18.1
		TIGER128 = 16,		//!< Tiger128 @version 18.1
		TIGER160 = 17,		//!< Tiger160 @version 18.1
		TIGER192 = 18		//!< Tiger192 @version 18.1
	};

	/*!
	@brief Evidence object properties flags
	*/
	enum class objprop_flag_t {
		DATA_WINDOW_OPEN = 0x02,//!< Data window open
		FLAGGED = 0x04,			//!< Flagged
		SELECTED = 0x08			//!< Selected for operations
	};

	/*!
	@brief
	Flags to indicate if a comment added by xways::api::add_comment()
	should replace any existing comment
	*/
	enum class replace_flag_t : uint32_t {
		REPLACE = 0x00,			//!< Replace existing comment
		APPEND = 0x01,			//!< Append to any existing comment,
								//!  do not replace it
		APPEND_NEWLINE = 0x02	//!< Append to any existing comment,
								//!  do not replace it, and insert a
								//!  line break as a delimiter
	};

	/*!
	@brief File systems returned by xways::api::get_volume_information()
	*/
	enum class fs_type_t {
		PART_DISKS = -16,		//!< Partitioned disks
		BITLOCKER = -15,		//!< Bitlocker encrypted NTFS 
								//!  partition
		HFSPLUS = -13,			//!< HFS Plus partition
		HFS = -12,				//!< HFS partition
		UFS = -11,				//!< UFS partition
		XFS = -10,				//!< XFS partition
		JFS = -9,				//!< JFS partition
		EXT4 = -7,				//!< Ext4 partition
		REISER4 = -6,			//!< ReiserFS 4 partition
		REISERFS = -5,			//!< ReiserFS partition
		EXT3 = -4,				//!< Ext3 partition
		EXT2 = -3,				//!< Ext2 partition
		HPFS = -2,				//!< HPFS partition
		NTFS = -1,				//!< NTFS partition
		UNKNOWN = 0,			//!< Unknown partition type
		FAT12 = 1,				//!< FAT12 partition
		FAT16 = 2,				//!< FAT16 partition
		FAT32 = 3,				//!< FAT32 partition
		exFAT = 4,				//!< exFAT partition
		UDF = 5,				//!< UDF partition
		XWFS = 6,				//!< XWFS partition
		OS = 7,					//!< via OS
		CDFS = 8,				//!< CDFS partition
		MEMORY = 9				//!< main memory
	};

	/*!
	@brief Item/file creation flags

	The flags @a EXCERPT and @a EXTERNAL are mutually exclusive, and @a
	KEEP_FILE can only be combined with @a EXTERNAL. If neither of the
	flags @a EXCERPT, @a EXTERNAL, or @a KEEP_FILE is specified, @a source
	is ignored and xways::api::create_file() works exactly like
	xways::api::create_item().
	*/
	enum class create_flag_t : uint32_t {
		EXPECT_MORE = 0x01,		//!< For performance reasons, set if
								//!  many more items are expected to
								//!  be created
		EXCERPT = 0x02,			//!< Create a file that is defined as
								//!  an excerpt from its parent (i.e.
								//!  a file carved within another
								//!  file), where @a source points to
								//!  a 64-bit start offset within that
								//!  parent file
		EXTERNAL = 0x04,		//!< Attach an external file, and @a
								//!  source is an **wchar_t\*** to the
								//!  path of that file. Can only be used
								//!  with the volume snapshot of a an
								//!  evidence object.
		KEEP_FILE = 0x08,		//!< Keep that external file that you
								//!  designate if you still need it
								//!  yourself after calling this
								//!  function.
	};

	/*!
	@brief Flags for sector I/O operations
	*/
	enum class sector_flag_t : uint32_t {
		WRITE = 0x01,			//!< Write (not yet implemented, if
								//!  not set, this function will read)
		STOP_ON_ERROR = 0x02,	//!< Stop upon I/O error and return
								//!  the number of successfully read
								//!  sectors (if not set, X-Ways
								//!  Forensics will try to continue
								//!  and fill unreadable sectors with
								//!  an ASCII pattern and return the
								//!  total number of sectors tried)
		ERROR_MESSAGES = 0x04,	//!< Output error messages in the GUI
								//!  in case of I / O errors
		NO_IMAGE_ACQ = 0x08,	//!< Do not trigger any pending
								//!  skeleton image acquisition
								//!  through a read operation
		CHECK_SPARSE = 0x10,	//!< Check whether the entire range of
								//!  sectors is defined as sparse in a
								//!  lower abstraction layer, for
								//!  performance benefits
		SPARSE = 0x20,			//!< (returned) : the entire range of
								//!  sectors targeted is sparse, so
								//!  you may ignore it, and the buffer
								//!  was not filled
	};

	/*!
	@brief Open item flags for xways::api::open_item()
	*/
	enum class open_item_flag_t : uint32_t {
		SLACK_SPACE = 0x01,		//!< Open the item including its file
								//!  slack space, if it has any
		SUPPRESS_ERROR = 0x02,	//!< Suppress error messages in the
								//!  program in case of failure
		PREF_ALT_DATA = 0x08,	//!< Pefer alternative file data if
								//!  available, e.g.a thumbnail
								//!  created by X - Ways Forensics for
								//!  a picture
		ALT_DATA_ONLY = 0x10	//!< Open alternative file data if
								//!  available, and fail if not
								//!  @version 17.7 SR-3
	};

	/*!
	@brief Flags for xways::raster_image_info_t to request a particular
	image format
	*/
	enum class raster_flag_t : uint32_t {
		BMP_HEADER = 0x01,		//!< Get a memory buffer that starts
								//!  with an appropriate Windows
								//!  Bitmap header
		ALIGN_32 = 0x02,		//!< Align line offsets at 4-byte
								//!  boundaries
		FLIP_VER_PHY = 0x04,	//!< Vertically flip image, physically
								//!  (reverse the order of pixel lines
								//!  in thememory buffer)
		BMP = 0x07,				//!< Create a standard Windows BMP
								//!  image (suitable combination of
								//!  flags @a BMP_HEADER, @a ALIGN_32
								//!  and @a FLIP_VERT)
		FLIP_VER_LOG = 0x08,	//!< Vertically flip image, logically
								//!  (only in conjunction with
								//!  @a BMP_HEADER, using a negative
								//!  height in the BMP header)
		FLIP_HOR_PHY = 0x10,	//!< Horizontally flip image,
								//!  physically (reverse the order of
								//!  the pixels in each line in the
								//!  memory buffer)
	};

	/*!
	@brief Flags used by xways::api::add_search_term()
	*/
	enum class search_term_flag_t : uint32_t {
		ALLOW_REUSE = 0x01,		//!< Allow re-use of existing search
								//!  term of the same name
		USER_HITS = 0x02,		//!< Mark search term as a search term
								//!  for user search hits
	};

	/*!
	@brief Search hit flags used by @ref xways::search_hit_t.
	*/
	enum class search_hit_flags_t : uint16_t {
		EXTRACTED = 0x0001,		//!< Resides in the text that was extracted
								//!  from the file, @ref
								//!  xways::search_hit_t::relative_offset is
								//!  not an offset in the file.
		NOTABLE = 0x0002,		//!< Notable.
		DELETED = 0x0008,		//!< Set to discard the search hit.
		INDEX = 0x0040,			//!< Index search hit.
		SLACK = 0x0080			//!< In slack space etc.
	};

	/*!
	@brief Flags used by xways::api::output_message()
	*/
	enum class msg_flag_t : uint32_t {
		UTF16 = 0x00,			//!< Default flags (UTF16)
		NO_LINE_BREAK = 0x01,	//!< Append without line break
		NO_LOG = 0x02,			//!< Don't log this error message
		ANSI = 0x04				//!< ANSI string @version 16.5
	};

	/*!
	@brief Flags used by xways::api::get_user_input()
	*/
	enum class user_input_flag_t : uint32_t {
		POSITIVE_INT = 0x01,	//!< Requires the user to enter a
								//!  positive integer, which is
								//!  returned if the user clicks ok,
								//!  otherwise -1 is returned
		ALLOW_NO_INPUT = 0x02	//!< Empty user input allowed
	};

	/*!
	@brief Flags used by xways::api::get_user_input()
	*/
	enum class progress_flag_t : uint32_t {
		NO_PROGRESS = 0x01,		//!< Show just the window, no actual
								//!  progress bar
		NO_INTERRUPT = 0x02,	//!< Do not allow the user to
								//!  interrupt the operation
		SHOW_NOW = 0x04,		//!< Show window immediately
		CONFIRM_ABORT = 0x08,	//!< Confirm abort
		NO_LOG = 0x10			//<! Prevent logging
	};

	//////////////////////
	// Enum Flag Helper //
	//////////////////////

	/*!
	@brief Support automatic conversion of enum types to bool

	This struct has conversion operators for the template type @a T
	and also bool and will choose between them as appropriate.
	*/
	template<typename T =
		typename std::enable_if<std::is_enum<T>::value, T>::type>
	struct auto_bool_t
	{
		T val_;		//!< Stored value of type @a T

					/*!
					@brief Construct from @a T

					@param val value of type @a T
					*/
		constexpr auto_bool_t(T val) : val_(val) {}

		/*!
		@brief Return value as type @a T

		@return value returned as type @a T
		*/
		constexpr operator T() const { return val_; }

		/*!
		@brief Return value as type @a bool

		@return value returned as type @a bool
		*/
		constexpr explicit operator bool() const
		{
			return  static_cast<
				typename std::underlying_type_t<T >> (val_) != 0;
		}
	};

	/*!
	@brief Bitwise AND operator

	Performs the bitwise operation and returns an @ref auto_bool_t struct
	to automatically convert to @a bool if necessary.

	@param lhs  left operand

	@param rhs  right operand

	@return @a lhs & @a rhs as a bool or type @a T as required
	*/
	template <typename T =
		typename std::enable_if<std::is_enum<T>::value, T>::type>
		constexpr auto_bool_t<T> operator&(T lhs, T rhs)
	{
		return static_cast<T>(
			static_cast<typename std::underlying_type<T>::type>(lhs)
			& static_cast<typename std::underlying_type<T>::type>(rhs));
	}

	/*!
	@brief Bitwise OR operator

	@param lhs  left operand

	@param rhs  right operand

	@return @a lhs | @a rhs as type @a T
	*/
	template <typename T =
		typename std::enable_if<std::is_enum<T>::value, T>::type>
		constexpr T operator|(T lhs, T rhs)
	{
		return static_cast<T>(
			static_cast<typename std::underlying_type<T>::type>(lhs) |
			static_cast<typename std::underlying_type<T>::type>(rhs));
	}

	/*!
	@brief Bitwise XOR operator

	@param lhs  left operand

	@param rhs  right operand

	@return @a lhs ^ @a rhs as type @a T
	*/
	template <typename T =
		typename std::enable_if<std::is_enum<T>::value, T>::type>
		constexpr T operator^(T lhs, T rhs)
	{
		return static_cast<T>(
			static_cast<typename std::underlying_type<T>::type>(lhs) ^
			static_cast<typename std::underlying_type<T>::type>(rhs));
	}

	/*!
	@brief Bitwise NOT operator

	@param val  operand

	@return @a ~val as type @a T
	*/
	template <typename T =
		typename std::enable_if<std::is_enum<T>::value, T>::type>
		constexpr T operator~(T val)
	{
		return static_cast<T>(
			~static_cast<typename std::underlying_type<T>::type>(val));
	}

	////////////////
	// Structures //
	////////////////

#pragma pack(push,1)

	/*!
	@brief Caller information provided to the @a version parameter of @ref
	XT_Init()
	*/
	struct caller_info_t {
		uint8_t lang;			//!< Language
		uint8_t service_rel;	//!< Service release number
		uint16_t version;		//!< Version number
	};

	/*!
	@brief Licence information provided to @ref XT_Init() via the @a
	licence parameter
	*/
	struct licence_info_t {
		uint32_t size;			//!< Size of the struct
		licence_t flags;		//!< Flags as per @ref licence_t

								/*!
								@brief Number of simultaneous users permitted

								Specifies how many different users may use the same
								dongle/license file simultaneously. For BYOD this is currently
								always 1. 0 means unknown.
								*/
		uint32_t num_users;

		FILETIME exp_date;		//!< Expriration date (if
								//!  #licence_t::EXP_DATE set)

								/*!
								@brief Licence ID

								Uniquely identifies the license. Based on the dongle ID or
								something similar. You could license your X-Tension based on
								that ID and only allow users to run your X-Tension if the ID
								matches your expectations. The actual dongle ID cannot be
								derived from @a id.
								*/
		uint8_t id[16];
	};

	/*!
	@brief Information provided to XT_PrepareSearch() via the
	@a search_info parameter
	*/
	struct prepare_search_info_t {
		int32_t size;			//!< Size of the struct
		wchar_t* search_terms;	//!< Search terms (line breaks
								//!  separate terms)
		uint32_t buffer_len;	//!< Size of the buffer that
								//!  lpSearchTerms points to in
								//!  Unicode characters
		uint32_t flags;			//!< Combination of @ref search_flag_t
	};

	/*!
	@brief Search information provided to the @ref api::search() method
	*/
	struct search_info_t {
		int32_t size;			//!< Size of the struct
		void* volume;			//!< Volume (currently must be 0 - the
								//!  current volume)
		wchar_t* search_terms;	//!< Search terms (line breaks
								//!  separate terms)
		search_option_t flags;	//!< Combination of flags contained
								//!  in xways::search_option_t
		uint32_t search_window; //!< Length of the search window (0
								//!  for standard length)
	};

	/*!
	@brief List of code pages to be used by the @ref api::search() method
	*/
	struct code_pages_t {
		int32_t size;			//!< Size of the struct
		uint16_t code_page1;	//!< Code page 1
		uint16_t code_page2;	//!< Code page 2
		uint16_t code_page3;	//!< Code page 3
		uint16_t code_page4;	//!< Code page 4
		uint16_t code_page5;	//!< Code page 5
	};

	/*!
	@brief Information about a search hit passed to
	@ref xways::x_tension::process_search_hit().
	*/
	struct search_hit_t {
		uint32_t size;				//!< Size of the structure
		int32_t item;				//!< ID of the item
		int64_t relative_offset;	//!< Relative offset of the search hit,
									//!  if any, otherwise -1
		int64_t absolute_offset;	//!< Absolute offset of the search hit,
									//!  in the volume, if available, otherwise
									//!  -1. This may be changed to imporve the
									//!  quality of the search hit
		void* hit_pointer;			//!< Pointer to the search hit in memory.
									//!  Provided only if @ref
									//!  xways::x_tension::process_search_hit()
									//!  is called during a search, not when
									//!  later applied to an existing search
									//!  hit, and only for search hits of the
									//!  simultaneous search, not for index
									//!  searches. Do not make any assumption
									//!  about how many bytes before or after
									//!  the search hit can be read in memory.
		int16_t search_term;		//!< ID of the search term. You may assign
									//!  a search hit to a different search
									//!  term by changing this ID
		int16_t length;				//!< Size of the search hit in bytes. You
									//!  may change that size, if that helps to
									//!  improve the quality of the search hit.
		int16_t code_page;			//!< The code page applicable to the hit
		search_hit_flags_t flags;	//!< Flags, one of @ref
									//!  xways::search_hit_flags_t
		void* handle;				//!< Specifies the item (for a logical
									//!  search) or volume (for a physical
									//!  search) in which a search hit was
									//!  found. Useful if you wish to read more
									//!  data from this item. Provided if @ref
									//!  xways::x_tension::process_search_hit
									//!  was called during a search, only for
									//!  search hits of simultaneous search.
									//!  0 for index searches, 0 when later
									//! applied to an existing search hit.
									//!  @version 16.5.
	};

	/*!
	@brief Event information relating to an evidence object passed to @ref
	api::add_event() or received from @ref api::get_event()
	*/
	struct event_info_t {
		int32_t size;			//!< Size of the struct
		void* evidence;			//!< Evidence object handle

								/*!
								@brief Event type

								Should be in the range:
								- Unknown: 0
								- File system events: 100+
								- File metadata events: 1000+
								- Internet events: 8000+
								- Messaging events: 10000+
								- Operating system events: 14000+
								- Windows event log events: 15000+
								- Windows registry events: 20000+
								*/
		uint32_t event_type;

		/*!
		@brief Timestamp flags

		Can be any of the following:
		- 0x04: low timestamp precision, only seconds
		- 0x08: low timestamp precision, only even numbers of seconds
		- 0x10: very low timestamp precision, only date
		- 0x20: timestamp in local time, not UTC
		- 0x40: designates an outdated timestamp
		*/
		uint32_t flags;

		FILETIME timestamp;	//!< Timestamp

							/*!
							@brief Item ID to which this event relates

							If the event is related to a particular object in the volume
							snapshot (file or directory), that object's ID, otherwise -1.
							*/
		int32_t item;

		/*!
		@brief Offset to where the timestamp was found in the volume
		or object

		If this event is related to a particular object (@a item =
		-1) then the offset is relative to the start of the object or
		otherwise the offset is relative to the start of the volume or
		-1 if unknown.
		*/
		int64_t offset;

		/*!
		@brief Textural description of the event

		Optional null-terminated textual description of the event,
		preferably in 7-bit ASCII or else in UTF-8. Will be truncated
		internally after 255 bytes. NULL if not provided.
		*/
		char* desc;
	};

	/*!
	@brief Raster image information returned by @ref
	api::get_raster_image()

	If @a width and/or @a height for are non-zero their values will
	be interpreted as the desired maximum width or height for the
	resulting image, and the image will be shrunk accordingly
	(resized, not cropped), if not already smaller, while preserving
	the aspect ratio of the original picture.
	*/
	struct raster_image_info_t {
		uint32_t size;			//!< Size of the struct
		int32_t item;			//!< Item ID to get a raster image from
		void* handle;			//!< Handle to the item
		uint32_t flags;			//!< Flags as per @ref raster_flag_t
		uint32_t width;			//!< Width of the returned image
		uint32_t height;		//!< Height of the returned image
		uint32_t data_size;		//!< Size of the returned image data
	};

	/*!
	@brief Drive information passed to @a XT_SectorIOInit().

	If @a width and/or @a height for are non-zero their values will
	be interpreted as the desired maximum width or height for the
	resulting image, and the image will be shrunk accordingly
	(resized, not cropped), if not already smaller, while preserving
	the aspect ratio of the original picture.
	*/
	struct drive_info_t {
		uint32_t size;				//!< Size of the struct

									/*!
									@brief Drive ID.

									Negative (physical, partitioned disk or image or hardware RAID)
									or a positive number (partition or volume on a disk or in an image
									or RAID). This parameter identifies the data source and must be
									used for any calls to XWF_DriveIO.
									*/
		int32_t drive;

		int32_t parent;				//!< Parent to the drive
		uint32_t sector_size;		//!< Sector size (bytes)

									/*!
									@brief Number of sectors.

									The number of sectors, which may be changed if appropriate - for
									example if the X-Tension is interpreting a compressed or encrypted
									drive that has a larger or smaller number of sectors than the
									containing drive/partition.
									*/
		int64_t sectors;			//!< Number of sectors

		int64_t parent_sectors;		//!< Number of sectors in the parent
		int64_t start_sector;		//!< Start sector on parent

									/*!
									@brief Private data area.

									Pointer to buffer 255 wchar_t characters in length into which an
									error message can be copied if returning 0, otherwise the pointer
									may be changed to a private data area.  The pointer will be passed
									to future calls to @a XT_SectorIO and @a XT_SectorIODone.
									*/
		void* private_area;
	};

	/*!
	@brief Report table associate list item returned by @ref
	api::get_ev_obj_report_table_assocs().
	*/
	struct report_assoc_item_t {
		uint16_t table;		//! Table ID
		int32_t item;		//! Item ID
	};

	/*!
	@brief Disk partition information in @ref api::disk_desc_t and
	returned by @ref api::parse_disk_desc().
	*/
	struct disk_part_info_t {
		uint64_t first_sector;			//!< First sector of partition
		uint64_t table_sector;			//!< Sector of partition table
		uint64_t size;					//!< Size of partition in bytes
		std::wstring name;				//!< Name of partition
		std::wstring type;				//!< Type of partition
	};

	/*!
	@brief Disk information returned by @ref
	api::parse_disk_desc().
	*/
	struct disk_desc_t {
		std::wstring case_name;			//!< Case name from EO1
		std::wstring evidence_no;		//!< Evidence number from EO1
		std::wstring eo1_desc;			//!< Description from EO1
		std::wstring examiner;			//!< Examiner from EO1
		std::wstring notes;				//!< Notes from EO1
		std::wstring app_ver;			//!< Version of imager from EO1
		std::wstring os_ver;			//!< OS version of imager from EO1
		std::wstring acquired;			//!< Date/time acquired from EO1
		uint64_t chunks_expected;		//!< Chunks reported in EO1
		uint64_t chunks_actual;			//!< Chunks found
		uint64_t chunk_size;			//!< Chunk size in EO1
		uint64_t capacity;				//!< Size of disk in bytes
		uint64_t sector_size;			//!< Sector size in bytes
		uint64_t sectors;				//!< Number of sectors
		std::wstring partition_style;	//!< Partition style (MBR/GPT)
		std::wstring disk_sig;			//!< Disk signature
		uint64_t unpartitionable;		//!< Unpartitionable space
		std::vector<disk_part_info_t> partitions;	//!< Partition info
	};

	/*!
	@brief Partition information returned by @ref
	api::parse_part_desc().
	*/
	struct part_desc_t {
		std::wstring fs;			//!< File system
		std::wstring label;			//!< Volume label
		std::wstring computer;		//!< Computer name (Windows)
		std::wstring domain;		//!< Domain (Windows)
		std::wstring install_date;	//!< Windows installation date
		uint64_t accounts;			//!< Number of accounts (Windows)
		std::wstring owner;			//!< Owner (Windows)
		std::wstring timezone;		//!< Timezone (Windows)
		std::wstring os_ver;		//!< Version (Windows)
		std::wstring vol_guid;		//!< Volume GUID
		std::wstring windows;		//!< Windows installation directory
		uint64_t capacity;			//!< Partition size in bytes
		uint64_t sectors;			//!< Number of sectors
		uint64_t sector_size;		//!< Sector size in bytes
		uint64_t cluster_size;		//!< Cluster size in sectors
		uint64_t clusters_free;		//!< Free clusters
		uint64_t clusters;			//!< Total clusters
		std::wstring ntfs_ver;		//!< NTFS version
		uint64_t flags;				//!< Volume flags
		/*!
		@ brief List of serial numbers (first string) and types (second
		string).
		*/
		std::vector<std::pair<std::wstring, std::wstring>> serials;
	};

#pragma pack(pop)

	////////////////
	// Constants  //
	////////////////

	/*!
	@brief Hash type strings
	*/
	static const wchar_t* hash_type_strings[] = {
		L"Undefined",
		L"CS8",
		L"CS16",
		L"CS32",
		L"CS64",
		L"CRC16",
		L"CRC32",
		L"MD5",
		L"SHA1",
		L"SHA256",
		L"RIPEMD128",
		L"RIPEMD160",
		L"MD4",
		L"ED2K",
		L"ADLER32",
		L"TIGERTREE",
		L"TIGER128",
		L"TIGER160",
		L"TIGER192"
	};

	/*!
	@brief Hash sizes
	*/
	static const int64_t hash_sizes[] = {
		0,			//!< Undefined
		1,			//!< CS8
		2,			//!< CS16
		4,			//!< CS32
		8,			//!< CS64
		2,			//!< CRC16
		4,			//!< CRC32
		16,			//!< MD5
		20,			//!< SHA1
		32,			//!< SHA256
		16,			//!< RIPEMD128
		20,			//!< RIPEMD160
		16,			//!< MD4
		16,			//!< ED2K
		4,			//!< ADLER32
		24,			//!< TIGERTREE
		16,			//!< TIGER128
		20,			//!< TIGER160
		24			//!< TIGER192
	};

	/*!
	@brief File type verification from @ref api::type_status_t and returned via
	@ref api::to_wstring(api::type_status_t).
	*/
	static const wchar_t* verification_strings[] = {
		L"Error",
		L"Not verified",
		L"Too small",
		L"Unknown",
		L"Confirmed",
		L"Not confirmed",
		L"Newly identified"
	};

	/*!
	@brief File system type from @ref api::fs_type_t and returned via @ref
	api::to_wstring(api::fs_type_t).
	*/
	static const wchar_t* fs_type_strings[] = {
		L"Partitioned Disks",
		L"NTFS Bitblocker",
		L"",
		L"HFSPlus",
		L"HFS",
		L"UFS",
		L"XFS",
		L"JFS",
		L"",
		L"Ext4",
		L"Reiser4",
		L"ReiserFS",
		L"Ext3",
		L"Ext2",
		L"HPFS",
		L"NTFS",
		L"Unknown",
		L"FAT12",
		L"FAT16",
		L"FAT32",
		L"exFAT",
		L"UDF",
		L"XWFS",
		L"(via OS)",
		L"CDFS",
		L"main memory"
	};

	/*!
	@brief Deletion status strings from @ref api::delete_status_t and returned
	via @ref api::to_wstring(api::delete_status_t).
	*/
	static const wchar_t* delete_status_strings[] = {
		L"Existing file",
		L"Previously existing, possibly recoverable",
		L"Previously existing, first cluster overwritten or unknown",
		L"Renamed/moved, possibly recoverable",
		L"Renamed/moved, first cluster overwritten or unknown"
	};

	///////////////////////
	// Global Functions  //
	///////////////////////

	/*!
	@brief Get the hash type as a std::wstring.

	@param[in] hash_type  a api::type_status_t

	@return a std::wstring containing the string representation
	*/
	inline std::wstring to_wstring(hash_type_t hash_type)
	{
		return hash_type_strings[(int)hash_type];
	}

	/*!
	@brief Get the file type verification status as a std::wstring.

	@param[in] type_status  a api::type_status_t

	@return a std::wstring containing the string representation
	*/
	inline std::wstring to_wstring(type_status_t type_status)
	{
		return verification_strings[(int)type_status -
			(int)type_status_t::STATUS_ERROR];
	}

	/*!
	@brief Get the file system type as a std::wstring.

	@param[in] fs_type  a api::fs_type_t

	@return a std::wstring containing the string representation
	*/
	inline std::wstring to_wstring(fs_type_t fs_type)
	{
		return fs_type_strings[(int)fs_type - (int)fs_type_t::PART_DISKS];
	}

	/*!
	@brief Get the deletion status as a std::wstring.

	@param[in] delete_status  a api::delete_status_t

	@return a std::wstring containing the string representation
	*/
	inline std::wstring to_wstring(delete_status_t delete_status)
	{
		return delete_status_strings[(int)delete_status];
	}

	/*!
	@brief Convert a std::wstring to a UTF8 std::string

	@param[in] in  std::wstring to convert

	@returns  std::string containing the UTF8 representation
	*/
	inline std::string to_utf8(std::wstring in)
	{
		return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>()
			.to_bytes(in);
	}

	/*!
	@brief Convert a file/disk size to a std::wstring with automatic scaling
	to the nearest multiplier prefix.

	@param[in] size  size to convert

	@param[in] binary  use binary prefixes (i.e. kiB, MiB, GiB, TiB and PiB)

	@returns  std::wstring containing the representation
	*/
	inline std::wstring disk_size(uint64_t size, bool binary = true)
	{
		const wchar_t* units[2][5] = {{
			L"kB",
			L"MB",
			L"GB",
			L"TB",
			L"PB"
		},{
			L"kiB",
			L"MiB",
			L"GiB",
			L"TiB",
			L"PiB"
		}};

		auto multiplier = binary ? 1024 : 1000;

		if (size < multiplier) return std::to_wstring(size) + L" bytes";

		int i = 0;
		while ((size >>= 10) >= multiplier) { i++; }

		return std::to_wstring(size) + L" " + units[binary][i];
	}

	/*!
	@brief Process windows messages to prevent hanging
	*/
	inline void process_messages()
	{
		MSG message;
		while (PeekMessageW(&message, NULL, 0, 0, PM_REMOVE) > 0)
		{
			TranslateMessage(&message);
			DispatchMessageW(&message);
		}
	}

	/*!
	@brief Convert a Windows FILETIME to its std::wstring representation.

	@param[in] file_time  FILETIME to convert

	@param[in] time_flags  time flags to pass to WIN32 API

	@param[in] date_flags  date flags to pass to WIN32 API

	@returns  std::wstring containing the representation
	*/
	inline std::wstring get_time_str(
		int64_t file_time,
		uint32_t time_flags = 0,
		uint32_t date_flags = DATE_SHORTDATE)
	{
		// Handle a bad FILETIME
		if (!file_time) return L"Unknown";

		// Result string
		std::wstring str;

		// Allocate enough space
		str.resize(32);

		// Convert the FILETIME to a local FILETIME
		FileTimeToLocalFileTime((FILETIME*)&file_time, (FILETIME*)&file_time);

		// Convert the local FILETIME to a SYSTEMTIME
		SYSTEMTIME sys_time;
		FileTimeToSystemTime((FILETIME*)&file_time, &sys_time);

		// Get the date part
		auto num = GetDateFormatW(LOCALE_USER_DEFAULT, date_flags,
			&sys_time, NULL, &str[0], (int)str.length());

		// Add a space between the date and time
		str[num - 1] = ' ';

		// Get the time part
		num += GetTimeFormatW(LOCALE_USER_DEFAULT, time_flags, &sys_time, NULL,
			&str[num], (int)str.length() - num);

		// Adjust the size of the string
		str.resize(num - 1);
		return str;
	}

	/*!
	@brief Base class for the X-Tension

	All X-Tensions must derive from this class in order to define the exports
	that will be called by X-Ways.  Simply override the desired methods to
	implement the custom functionality.  For all optional methods (i.e. except
	for api::x-tension::init()) it is also necessary to #define a macro
	equivalent to the method prior to including xwayapi.hpp in the class
	implementation file an example would be, to include process_item it is
	necessary to insert #define XT_PROCESS_ITEM before #include "xwaysapi.hpp".

	An example of this is the following code fragment:
	\includelineno "xtension.cpp"

	For further details see the documentation for the relevant method.
	*/
	class x_tension {
	protected:
		//! Introductory text to display in the X-Ways messages log and about
		//! @note XT_PREFIX should be defined via the project itself
		const wchar_t* about_text =
			XT_PREFIX L": Version " XT_VERSION XT_PLATFORM;

	public:
		/*!
		@brief Constructor.

		The base constructor saves a pointer to itself in @ref
		xways:api::x_tension_. This pointer is used by the DLL exports to
		access the derived overrided methods.  If this method is overrided in
		the derived class the base class version must be called, or
		alternatively a pointer to the derived object saved by the derived
		constructor.
		*/
		x_tension();

		/*!
		@brief Entry point to initialize the X-Tension

		This function, which is mandatory to export, will be called by X-Ways
		before anything else happens, to inform the DLL of the version of
		X-Ways Forensics that is loading the DLL and permit any initialization
		that may be required.

		@param[in] version  an @ref xways::caller_info_t struct containing
		information about the X-Ways version

		@param[in] flags  one of @ref xways::init_flag_t

		@param[in] main_window  WIN32 handle to the main X-Ways window

		@param[in] licence  pointer to a @ref xways::licence_info_t struct with
		information about the licence

		@return -1 to prevent further use of the DLL by X-Ways (e.g. if the
		version is not supported), otherwise 1
		*/
		virtual int32_t init(
			xways::caller_info_t version,
			xways::init_flag_t flags,
			HWND main_window,
			xways::licence_info_t* licence);

		/*!
		@brief Entry point to clean up the X-Tension before being unloaded

		This function, which is optional to export, will be called just before
		the DLL is unloaded to give you a chance to dispose any allocated
		memory, save certain data permanently etc.

		@param[in] reserved  currently always NULL

		@return always return 0
		*/
		virtual int32_t done(void* reserved);

		/*!
		@brief Entry point to show information about the X-Tension to the user

		This function, which is optional to export, will be called when the
		user requests to see information about the DLL. Things that an
		X-Tension might choose to display include:
		- copyright notices
		- a version number
		- a brief description of the exported functionality
		- extensive help on how to use it
		- from where in X-Ways Forensics to call it
		- with what settings

		An X-Tension might also display a dialog window where the user can
		change settings for this X-Tension, which it then stores in the
		Windows registry or in a permanent file.

		@param[in] parent_window  WIN32 handle to the parent window

		@param[in] reserved  currently always NULL

		@return always return 0
		*/
		virtual int32_t about(void* parent_window, void* reserved);

		/*!
		@brief Entry point to prepare an X-Tension for an impending operation

		This function, which is optional to export, will be called immediately
		for a volume before a volume snapshot refinement or some other action
		starts or before items or search hits in that volume are processed
		individually.

		@param[in] volume  a handle to the file/volume that the X-Tension is
		applied to or 0 if run from the main menu with no data window open; to
		check if a non-zero handle is a file or volume, call
		xways::api::get_volume_information() and check that @a bytes_per_sector
		is/is not 0 respectively

		@param[in] evidence  a handle to an evidence object if @a volume
		represents an evidence object (version 17.5 SR-2), otherwise 0

		@param[in] operation  the operation that X-Ways is performing

		@param[in] reserved  currently always 0

		@return one of xways::prepare_t
		*/
		virtual xways::prepare_t prepare(
			void* volume,
			void* evidence,
			xways::action_t operation,
			void* reserved);

		/*!
		@brief Entry point to allow an X-Tension to finalize an operation

		This function, which is optional to export, will be called when volume
		snapshot refinement or another operation has completed.

		@param[in] volume  a handle to the file/volume that the X-Tension is
		applied to or 0 if run from the main menu with no data window open; to
		check if a non-zero handle is a file or volume, call
		xways::api::get_volume_information() and check that @a bytes_per_sector
		is/is not 0 respectively

		@param[in] evidence  a handle to an evidence object if  @a volume
		represents an evidence object (version 17.6), otherwise 0

		@param[in] operation  the operation that X-Ways is performing

		@param[in] reserved  currently always 0

		@return 1 if the current directory listing in the directory browser of
		the active data window has to be refreshed after xways::action_t::DBC
		(usually not necessary, perhaps when adding new files to the directory,
		has an effect in v17.6 and later only), or otherwise 0
		*/
		virtual int32_t finalize(
			void* volume,
			void* evidence,
			uint32_t operation,
			void* reserved);

		/*!
		@brief Entry point to allow an X-Tension to process an item that has
		not been opened.

		This function, which is optional to export, will be called for each
		item (file or directory) in the volume snapshot that is targeted for
		refinement or selected and targeted with the directory browser context
		menu. Export this function if you merely need to retrieve information
		about the file and don't need to read its data. There is a slight
		performance benefit if the user does not select other refinement
		operations that do need to open the item and read its data, i.e. you
		save a little bit time if a file does not need to opened for reading.
		You can still open the file to read its data if needed using @ref
		xways::open_item().

		@param[in] id  the ID of the item

		@param[in] reserved  currently always 0

		@return -1 if X-Ways should stop the current operation (e.g. volume
		snapshot refinement), otherwise 0
		*/
		virtual int32_t process_item(int32_t id, void* reserved);

		/*!
		@brief Entry point to allow an X-Tension to process an item that has
		been opened.

		This function, which is optional to export, will be called for each
		item (file or directory) in the volume snapshot that is targeted for
		refinement or selected and targeted with the directory browser context
		menu. The item will be opened for reading prior to the function call.
		Export this function if there is a need to read the item's data, which
		can be done using the hItem parameter.

		@param[in] id  the ID of the item

		@param[in] handle  an open handle to the item

		@param[in] reserved  currently always 0

		@return -1 if X-Ways should stop the current operation (e.g. volume
		snapshot refinement), otherwise 0
		*/
		virtual int32_t process_item_ex(
			int32_t id, void* handle, void* reserved);

		/*!
		@brief Entry point to allow an X-Tension to perform tasks before a
		simultaneous search operation.

		This function, which is optional to export, will be called if an
		X-Tension is loaded for use with a simultaneous search, so that the
		X-Tension can enter predefined search terms into the dialog window for
		use with the search. The X-Tension can also learn about the current
		search settings (the active code pages and some other settings through
		the flags field) and could inform the user of necessary adjustments for
		the search to work as intended by the X-Tension. Adjustments to the
		flags or the code pages are ignored.

		@param[in] id  the ID of the item

		@param[in] handle  an open handle to the item

		@param[in] reserved  currently always 0

		@return 1 if adjustments have been made to the search terms, or 0 if
		not, or -1 if the X-Tension should be unselected.

		@version 16.9
		*/
		virtual int32_t prepare_search(
			prepare_search_info_t* info, code_pages_t* code_pages);

		/*!
		@brief Entry point to allow an X-Tension to process a simultaneous
		search hit.

		This function, which is optional to export, will be called  for each
		search hit, either when it is found or, in a future version of X-Ways
		Forensics, later if selected by the user in a search hit list. This
		function is not called if the search was initiated by the X-Tension
		itself (via @ref xways::search()).

		@param[in] id  the ID of the item

		@param[in] handle  an open handle to the item

		@param[in] reserved  currently always 0

		@return 0, or -1 to have X-Ways Forensics to abort the search, or -2
		to have X-Ways Forensics to stop calling this X-Tension.
		*/
		virtual int32_t process_search_hit(search_hit_t* info);

		/*!
		@brief Entry point to implement a "Viewer X-Tension"

		This function, which is optional to export, then the X-Tension is a
		"Viewer X-Tension".  It will be called for each file that is to be
		viewed in a separate window or in Preview mode or that is included in a
		case report (appears to not currently be implemented). The X-Tension
		can provide a human-readable representation of a binary file for
		example as plain text or HTML, or by converting special graphics file
		formats to BMP/JPEG/PNG etc., or using any other file format understood
		by the viewer component. This representation should be written into a
		buffer that is allocated by the X-Tension itself using a function of
		its own choice (e.g. VirtualAlloc, GlobalAlloc, HeapAlloc, malloc,
		GetMem, ...). X-Ways will call XT_ReleaseMem() at a later point of
		time to allow it to free up the allocated memory when no longer needed.

		@a handle may be used to query the size of the file (via
		xways::api::get_size()) and to read from it (using xways::api::read()),
		for example to check the signature of the file, to determine if the
		X-Tension supports that particular file type). @a id can be used for
		the volume snapshot item property functions, e.g. to get more file
		system level metadata about the file such as the filename or to get
		more data from the volume snapshot such as the detected type of the
		file. The @a volume handle may be used to get more information about
		the volum that the file resides in, and the @a evidence handle can be
		used to ge more information about the evidence object that the volume
		represents (if any).

		In v18.2 and later, a button labelled "XT" is shown in the user
		interface when Viewer X-Tensions are available (loaded), next to the
		"Raw" button. That button allows the user to conveniently change the
		preview to the representation provided by the first viewer X-Tension
		that feels responsible for the type of the selected file. Or back to
		the regular preview if not helpful, in both directions with a single
		mouse click. The user may also combine Raw and XT submodes of Preview
		mode, for example for debugging purposes.

		@note If the user loads the X-Tension in X-Ways Forensics for viewing
		purposes, @ref xways::x_tension::prepare(), @ref
		xways::x_tension::finalize(), @ref xways::x_tension::process_item(),
		@ref xways::x_tension::process_item_ex(), @ref
		xways::x_tension::prepare_search(), and @ref
		xways::x_tension::process_search_hit() will not be called even if
		exported. view() will not be called even if loaded and selected by the
		user if a Viewer X-Tension higher in the list signals that it does
		provide a view of that particular file already.

		@param[in] handle  an open handle to the item

		@param[in] id  the ID of the item

		@param[in] volume  a handle to the volume containing the item

		@param[in] evidence  a handle to the evidence item containing the item

		@param[in] reserved  currently always NULL

		@param[out] size  set to -1 if the X-Tension should not be responsible
		for providing a representation of this file (in most instances because
		X-Tensions support very specific file types only), -2 to indicate that
		an error occurred that needs to be reported, 0 to signal that the file
		should be represented as no data, or a positive value to signal success
		and indicate the size of the data in the buffer

		@return the address of the buffer, or NULL to signal failure
		*/
		virtual void* view(
			void* handle,
			int32_t id,
			void* volume,
			void* evidence,
			void* reserved,
			int64_t* size);

		/*!
		@brief Entry point to allow an X-Tension to deallocate a buffer
		provided to X-Ways by @ref xways::x_tension::view().

		This function, which is mandatory for viewer type X-Tensions to export,
		is called by X-Ways to free memory allocated by a previous call to
		@ref xways::x_tension::view()).

		@param[in] buffer  pointer to the buffer to be deallocated

		@return currently ignored, but should signal success or failure of the
		release of the memory buffer

		@version 17.6
		*/
		virtual bool release_mem(void* buffer);

		/*!
		@brief Entry point to show information about the X-Tension to the user

		If an X-Tension exports this function (optional), it will be called by
		X-Ways Forensics when opening a disk or image or partition or volume or
		reconstructed RAID to see whether the X-Tension wishes to act as an
		additional abstraction layer for disk I/O. In particular that allows
		the X-Tension to decrypt data on encrypted disks/partitions on the fly
		when needed by X-Ways Forensics, so that X-Ways Forensics will only get
		to see the decrypted data. Such an X-Tension will likely check first
		whether the disk/partition contains any supported kind of encryption
		and may have to prompt the user for a decryption key or a key file
		(using a dialog window of its own) and/or read some sectors by calling
		the @ref xways::disk_io() function to make the final decision.

		@param[in] drive_info  @ref xways::drive_info_t structure containing
		information about the current drive/partition

		@return 0 to not be responsible for access to this drive, 0x01 to be
		responsible for read access, 0x02 to be responsible for write access
		(not supported) and 0x04 to enforce read-only mode if direct writing
		(not through the X-Tension).

		@versin 18.4
		*/
		virtual uint32_t sector_io_init(drive_info_t* info);

		/*!
		@brief Entry point to show information about the X-Tension to the user.

		If an X-Tension exports this function (mandatory if @ref
		xways::x_tension::sector_io_init() is exported), it will be called by
		X-Ways Forensics only if an X-Tension returned the flag 0x01 for a
		previous call for @ref xways::x_tension::sector_io_init(), for any
		internally triggered I/O operations.

		It is your resonsibility to fill the buffer and return the number of
		transferred sectors. You retrieve the original sector contents by
		calling @ref xways::sector_io(), alter the data as necessary (e.g.
		decrypt), and then return it. You may read from the same sector number
		or translate the external sector number to a different internal sector
		number and read from that sector instead, for example to skip a header
		with encryption metadata.

		@param[in] private_area  pointer to the private area, as provided by
		@ref xways::x_tension::sector_io_init()

		@param[in] drive  drive/partition ID

		@param[in] sector  the starting sector to read from

		@param[in] count  the number of sectors to read

		@param[out]  the buffer to fill

		@param[in] flags  @ref sector_flag_t as required by @ref
		xways::sector_io

		@return the number of transferred sectors

		@version 18.4
		*/
		virtual uint32_t sector_io(void* private_data, int32_t drive,
			int64_t sector, uint32_t count, void* buffer, uint32_t flags);

		/*!
		@brief Entry point to show information about the X-Tension to the user

		If an X-Tension exports this function (mandatory if @ref
		xways::x_tension::sector_io_init() is exported), it will be called by
		X-Ways Forensics 18.4 and later, only if an X-Tension returned 1 for a
		previous call for @ref xways::x_tension::sector_io_init(). Gives the
		X-Tension the opportunity to free any resources when the disk/partition
		is about to be closed, in particular memory on the heap that you may
		have allocated before and this is pointed to by @a private_area.

		@param[in] private_area  pointer to the private area, as provided by
		@ref xways::x_tension::sector_io_init()

		@param[in reserved

		@return 0
		*/
		virtual uint32_t sector_io_done(void* private_data, void* reserved);
	};

	/*!
	@brief Wrapper class for the X-Ways API

	Provides direct access to all of the functions export by the API. To
	use this class (or any other functionality provided by either
	xwaysapi.hpp or xtension.hpp) you must first create an xways::api
	object.  You may then access the API by calling any of its public
	methods.

	An example of this is the following code fragment:
	\includelineno "api.cpp"

	For further details see the documentation for the relevant method.
	*/
	class api {
	public:

		/////////////////
		// Public Data //
		/////////////////

		x_tension *x_tension_;


		////////////////////
		// Public Methods //
		////////////////////

		/*!
		@brief construct an @ref api object and initialise method
		pointers
		*/
		static api& instance()
		{
			static api inst;
			return inst;
		}
		protected:
		api()
		{
			HMODULE Hdl = GetModuleHandle(NULL);

			// Disk and General I/O Functions
			get_size_ = (get_size_t)GetProcAddress(Hdl, "XWF_GetSize");
			get_volume_name_ = (get_volume_name_t)GetProcAddress(
				Hdl,
				"XWF_GetVolumeName");
			get_volume_information_ =
				(get_volume_information_t)GetProcAddress(
					Hdl,
					"XWF_GetVolumeInformation");
			get_block_ = (get_block_t)GetProcAddress(Hdl, "XWF_GetBlock");
			set_block_ = (set_block_t)GetProcAddress(Hdl, "XWF_SetBlock");
			get_sector_contents_ = (get_sector_contents_t)GetProcAddress(
				Hdl,
				"XWF_GetSectorContents");
			open_item_ = (open_item_t)GetProcAddress(Hdl, "XWF_OpenItem");
			close_ = (close_t)GetProcAddress(Hdl, "XWF_Close");
			read_ = (read_t)GetProcAddress(Hdl, "XWF_Read");
			sector_io_ = (sector_io_t)GetProcAddress(Hdl, "XWF_SectorIO");

			// Evidence Object/Case Management Functions
			get_case_prop_ =
				(get_case_prop_t)GetProcAddress(Hdl, "XWF_GetCaseProp");
			get_first_ev_obj_ = (get_first_ev_obj_t)GetProcAddress(
				Hdl,
				"XWF_GetFirstEvObj");
			get_next_ev_obj_ =
				(get_next_ev_obj_t)GetProcAddress(
					Hdl,
					"XWF_GetNextEvObj");
			create_ev_obj_ =
				(create_ev_obj_t)GetProcAddress(Hdl, "XWF_CreateEvObj");
			open_ev_obj_ =
				(open_ev_obj_t)GetProcAddress(Hdl, "XWF_OpenEvObj");
			close_ev_obj_ =
				(close_ev_obj_t)GetProcAddress(Hdl, "XWF_CloseEvObj");
			get_ev_obj_prop_ =
				(get_ev_obj_prop_t)GetProcAddress(
					Hdl,
					"XWF_GetEvObjProp");
			get_ev_obj_ =
				(get_ev_obj_t)GetProcAddress(Hdl, "XWF_GetEvObj");
			get_report_table_info_ =
				(get_report_table_info_t)GetProcAddress(
					Hdl,
					"XWF_GetReportTableInfo");
			get_ev_obj_report_table_assocs_ =
				(get_ev_obj_report_table_assocs_t)GetProcAddress(
					Hdl,
					"XWF_GetEvObjReportTableAssocs");

			// Volume Snapshot Management Functions
			select_volume_snapshot_ =
				(select_volume_snapshot_t)GetProcAddress(
					Hdl,
					"XWF_SelectVolumeSnapshot");
			get_vs_prop_ =
				(get_vs_prop_t)GetProcAddress(Hdl, "XWF_GetVSProp");
			get_item_count_ =
				(get_item_count_t)GetProcAddress(Hdl, "XWF_GetItemCount");
			get_file_count_ =
				(get_file_count_t)GetProcAddress(Hdl, "XWF_GetFileCount");
			create_item_ =
				(create_item_t)GetProcAddress(Hdl, "XWF_CreateItem");
			create_file_ =
				(create_file_t)GetProcAddress(Hdl, "XWF_CreateFile");

			// Volume Snapshot Item Property Functions
			get_item_name_ =
				(get_item_name_t)GetProcAddress(Hdl, "XWF_GetItemName");
			get_item_size_ =
				(get_item_size_t)GetProcAddress(Hdl, "XWF_GetItemSize");
			set_item_size_ =
				(set_item_size_t)GetProcAddress(Hdl, "XWF_SetItemSize");
			get_item_ofs_ =
				(get_item_ofs_t)GetProcAddress(Hdl, "XWF_GetItemOfs");
			set_item_ofs_ =
				(set_item_ofs_t)GetProcAddress(Hdl, "XWF_SetItemOfs");
			get_item_information_ = (get_item_information_t)GetProcAddress(
				Hdl,
				"XWF_GetItemInformation");
			set_item_information_ = (set_item_information_t)GetProcAddress(
				Hdl,
				"XWF_SetItemInformation");
			get_item_type_ =
				(get_item_type_t)GetProcAddress(Hdl, "XWF_GetItemType");
			set_item_type_ =
				(set_item_type_t)GetProcAddress(Hdl, "XWF_SetItemType");
			get_item_parent_ = (get_item_parent_t)GetProcAddress(
				Hdl,
				"XWF_GetItemParent");
			set_item_parent_ = (set_item_parent_t)GetProcAddress(
				Hdl,
				"XWF_SetItemParent");
			get_report_table_assocs_ =
				(get_report_table_assocs_t)GetProcAddress(
					Hdl,
					"XWF_GetReportTableAssocs");
			add_to_report_table_ = (add_to_report_table_t)GetProcAddress(
				Hdl,
				"XWF_AddToReportTable");
			get_comment_ =
				(get_comment_t)GetProcAddress(Hdl, "XWF_GetComment");
			add_comment_ =
				(add_comment_t)GetProcAddress(Hdl, "XWF_AddComment");
			get_extracted_metadata_ =
				(get_extracted_metadata_t)GetProcAddress(
					Hdl,
					"XWF_GetExtractedMetadata");
			add_extracted_metadata_ =
				(add_extracted_metadata_t)GetProcAddress(
					Hdl,
					"XWF_AddExtractedMetadata");
			get_hash_value_ =
				(get_hash_value_t)GetProcAddress(Hdl, "XWF_GetHashValue");

			// File Contents Functions
			get_metadata_ =
				(get_metadata_t)GetProcAddress(Hdl, "XWF_GetMetadata");
			get_metadata_ex_ =
				(get_metadata_ex_t)GetProcAddress(Hdl, "XWF_GetMetadataEx");
			get_raster_image_ = (get_raster_image_t)GetProcAddress(
				Hdl,
				"XWF_GetRasterImage");

			// Search-Related Functions
			search_ = (search_t)GetProcAddress(Hdl, "XWF_Search");
			add_search_term_ = (add_search_term_t)GetProcAddress(
				Hdl,
				"XWF_AddSearchTerm");
			get_search_term_ = (get_search_term_t)GetProcAddress(
				Hdl,
				"XWF_GetSearchTerm");

			// Event-Related Functions
			add_event_ = (add_event_t)GetProcAddress(Hdl, "XWF_AddEvent");
			get_event_ = (get_event_t)GetProcAddress(Hdl, "XWF_GetEvent");

			// Evidence File Container Functions
			create_container_ = (create_container_t)GetProcAddress(
				Hdl,
				"XWF_CreateContainer");
			copy_to_container_ = (copy_to_container_t)GetProcAddress(
				Hdl,
				"XWF_CopyToContainer");
			close_container_ = (close_container_t)GetProcAddress(
				Hdl,
				"XWF_CloseContainer");

			// Miscellaneous Functions
			output_message_ = (output_message_t)GetProcAddress(
				Hdl,
				"XWF_OutputMessage");
			get_user_input_ =
				(get_user_input_t)GetProcAddress(Hdl, "XWF_GetUserInput");
			show_progress_ =
				(show_progress_t)GetProcAddress(Hdl, "XWF_ShowProgress");
			set_progress_percentage_ =
				(set_progress_percentage_t)GetProcAddress(
					Hdl,
					"XWF_SetProgressPercentage");
			set_progress_description_ =
				(set_progress_description_t)GetProcAddress(
					Hdl,
					"XWF_SetProgressDescription");
			should_stop_ =
				(should_stop_t)GetProcAddress(Hdl, "XWF_ShouldStop");
			hide_progress_ =
				(hide_progress_t)GetProcAddress(Hdl, "XWF_HideProgress");
		}

		public:

		///@name Disk and General I/O Functions

		/*!
		@brief Returns the size of the volume or physical size of the file

		@a optional is supported from v16.7 SR - 8. This parameter must be
		NULL in earlier versions.

		(LPVOID)1 will make this function retrieve the logical file size
		(which may be different	from the size that was known of the file
		in the volume snapshot before it was opened),

		(LPVOID)2 the valid data length (a.k.a. initialized size of the
		data stream, which may be available from NTFS, exFAT, XWFS,
		XWFS2).

		@param[in] handle  handle of the volume or item

		@param[in] type  the type of size to return: 0 for physical, 1 for
		logical, 2 for valide data length (must be 0 prior to 16.7 SR-8)

		@return the size of the volume or file
		*/
		int64_t get_size(void* handle, uint8_t* type = 0)
		{
			return get_size_(handle, type);
		}

		/*!
		@brief Retrieves the name of the volume

		Volume name is returned in UTF-16, 255 characters at most. 3 types
		of names are available (1, 2 or 3). For example, 3 can be more
		generic than 2 ("Hard disk 1" instead of "WD12345678").

		@param[in] volume  handle of the volume

		@param[out] buffer  buffer to receive the volume name (255 UTF-16
		characters)

		@param[in] type  the type of name to retrieve (1, 2 or 3)
		*/
		void get_volume_name(void* volume, wchar_t* buffer, uint32_t type)
		{
			get_volume_name_(volume, buffer, type);
		}

		/*!
		@brief Retrieves the name of the volume

		Volume name is one of 3 types (1, 2 or 3). For example, 3 can be
		more generic than 2 ("Hard disk 1" instead of "WD12345678").

		@param[in] volume  handle of the volume

		@param[in] type  the type of name to retrieve (1, 2 or 3)

		@returns std::wstring containing the volume name
		*/
		std::wstring get_volume_name(void* volume, uint32_t type)
		{
			std::wstring str;
			str.resize(255);
			get_volume_name_(volume, &str[0], type);
			auto last = str.find_first_of(L'\0');
			if (last < str.length()) str.resize(last);
			return str;
		}

		/*!
		@brief Retrieves various information about the volume

		All parameters are optional.

		@param[in] volume  handle of the volume

		@param[out] filesystem  the file system type as per @ref fs_type_t

		@param[out] bytes_per_sector  the number of bytes per sector

		@param[out] sectors_per_cluster  the number of sectors per cluster

		@param[out] cluster_count  the number of clusters

		@param[out] first_cluster_sector  the sector number of the first
		cluster
		*/
		void get_volume_information(void* volume, int32_t* filesystem,
			uint32_t* bytes_per_sector, uint32_t* sectors_per_cluster,
			int64_t* cluster_count, int64_t* first_cluster_sector)
		{
			get_volume_information_(volume, filesystem, bytes_per_sector,
				sectors_per_cluster, cluster_count, first_cluster_sector);
		}

		/*!
		@brief Retrieves the boundaries of the currently selected block,
		if any

		@param[in] volume  handle of the volume

		@param[out] start_off  the starting offset

		@param[out] end_off  the ending offset

		@return true if a block is selected otherwise false

		@version 17.7
		*/
		bool get_block(void* volume, int64_t* start_off, int64_t* end_off)
		{
			return get_block_(volume, start_off, end_off);
		}

		/*!
		@brief Sets the boundaries of the currently selected block, if any

		Set @a end_off to -1 to clear any currently defined block.

		@param[in] volume  handle of the volume

		@param[in] start_off  the starting offset

		@param[in] end_off  the ending offset

		@return false if the boundaries indicated exceed the size of the
		volume

		@version 17.7
		*/
		bool set_block(void* volume, int64_t start_off, int64_t end_off)
		{
			return set_block_(volume, start_off, end_off);
		}

		/*!
		@brief Retrieves information about a certain sector on a volume

		Retrieves a textual description of what this sector is used for.
		Can be the name and path of a file or something like "FAT 1". May
		be language specific. Use a buffer that has space for 511
		characters and a terminating null.

		@param[in] volume  handle of the volume

		@param[in] sector  the sector number

		@param[out] desc  receives a textual description of what this
		sector is used for (511 characters plus terminating null)

		@param[out] item_id  receives the ID of the item in the volume
		snapshot that the sector is allocated to, if any, otherwise -1
		(optional)

		@return false if the sector belongs to an unused/free cluster,
		otherwise true
		*/
		bool get_sector_contents(
			void* volume,
			int64_t sector,
			wchar_t* desc,
			int32_t* item_id)
		{
			return get_sector_contents_(volume, sector, desc, item_id);
		}

		/*!
		@brief Opens a file or directory for reading

		Opens the file or directory that is contained in the specified
		volume and that has the specified ID for reading and returns a
		handle to it, or NULL if unsuccessful.

		@param[in] volume  handle of the volume

		@param[out] item_id  the ID of the item

		@param[out] flags  flags as per @ref open_item_flag_t (by
		v17.5 SR-11, v17.6 SR-7, v17.7 and later)

		@return handle of the item or NULL if unsuccessful

		@version 16.5
		*/
		void* open_item(
			void* volume,
			int32_t item_id,
			open_item_flag_t flags = (open_item_flag_t)0)
		{
			return open_item_(volume, item_id, flags);
		}

		/*!
		@brief Closes a volume opened with @a open_volume (not currently
		implemented) or an item opened with @ref open_item

		@param[in] handle  handle of the volume or item

		@version 16.5
		*/
		void close(void* handle) { close_(handle); }

		/*!
		@brief Read bytes from a volume or item

		Reads the specified number of bytes from the specified position in
		the specified volume or item into the specified buffer. Returns
		the number of bytes read.

		@param[in] handle  handle of the volume or item

		@param[in] offset  the offset to read from

		@param[out] buffer  the buffer to write to

		@param[in] bytes_to_read  the number of bytes to read

		@return the number of bytes read
		*/
		uint32_t read(void* handle, int64_t offset, uint8_t*buffer,
			uint32_t bytes_to_read)
		{
			xways::process_messages();
			return read_(handle, offset, buffer, bytes_to_read);
		}

		/*!
		@brief Read or write sectors

		Reads or writes the specified sectors number from or to the
		specified disk or image or partition or volume or RAID. May be
		called when processing XT_SectorIO. @a flags may be NULL if you do
		not wish to specify or receive any flags.

		All other flags are reserved and must not be set.

		@param[in] drive  device/disk/partition/volume/image/RAID
		identifier

		@param[in] sector  the sector number

		@param[in] count  the number of sectors to read/write

		@param[in,out] buffer  the buffer to read from or write to

		@param[in] flags  flags as per @ref sector_flag_t

		@return the number of sectors read/written/covered

		@version 18.4
		*/
		uint32_t sector_io(
			int32_t drive,
			int64_t sector,
			uint32_t count,
			void* buffer,
			sector_flag_t* flags)
		{
			return sector_io_(drive, sector, count, buffer, flags);
		}

		///@}
		///@name Evidence Object/Case Management Functions

		/*!
		@brief Get information about the current case

		@param[in] reserved  must be NULL

		@param[in] prop_type  the case information to get, one of @ref
		caseprop_t

		@param[in,out] buffer  pointer to buffer to receive the requested
		property

		@param[in] buf_size  the size of the buffer (in **wchar_t**
		characters)

		@return the length of the string (in wchar_t characters), or a
		negative number if an error occurred.
		*/
		int64_t get_case_prop(void* reserved, caseprop_t prop_type,
			void* buffer, int32_t buf_size)
		{
			return get_case_prop_(reserved, prop_type, buffer, buf_size);
		}

		/*!
		@brief Get information about the current case

		@param[in] prop_type  the case information to get, one of @ref
		caseprop_t

		@return std::wstring containing the requested information string.
		*/
		std::wstring get_case_prop(xways::caseprop_t prop)
		{
			int64_t len;
			std::wstring str;
			str.resize(256);
			len = get_case_prop_(NULL, prop, &str[0], (int32_t)str.length());
			if (len < 0) len = 0;
			else if ((uint64_t)len > str.length())
			{
				str.resize(len);
				get_case_prop_(NULL, prop, &str[0], (int32_t)str.length());
			}
			else str.resize(len);

			return str;
		}

		/*!
		@brief Get a handle to the first evidence object in the case

		Retrieves a handle to the first evidence object in the case. In
		conjunction with @ref get_next_ev_obj this function allows the
		enumeration of all evidence objects of the case.

		@param[in] reserved  must be NULL

		@return handle to the first evidence object

		@version 17.6
		*/
		void* get_first_ev_obj(void* reserved = NULL)
		{
			return get_first_ev_obj_(reserved);
		}

		/*!
		@brief Get a handle to the next evidence object in the case

		Retrieves the next evidence object in the chain. In conjunction
		with @ref get_first_ev_obj this function allows the enumeration
		of all evidence objects of the case.

		@param[in] prev_evidence  handle to the first evidence object

		@param[in] reserved  must be NULL

		@return handle to the next evidence object

		@version 17.6
		*/
		void* get_next_ev_obj(void* prev_evidence, void* reserved = NULL)
		{
			return get_next_ev_obj_(prev_evidence, reserved);
		}

		/*!
		@brief Creates one or more evidence objects

		Creates one or more evidence objects from one source (which can be
		a medium, disk/volume image, memory dump, or a directory/path). A
		case must already be loaded. If more than 1 evidence object is
		created (for example for a physical disk that contains
		partitions, which count as evidence objects themselves), use
		get_next_ev_obj to find them.

		@param[in] type  evidence object type, one of @ref
		evobj_type_t

		@param[in] disk_id  0 for an image, memory dump or file; 3+ for a
		drive letter (3 for C: drive) or (-3)- for a physical disk (-3 for
		physical disk 0)

		@param[in] path  Path in case of a file or directory, otherwise
		NULL.

		@param[in] reserved  must be NULL

		@return handle to the first evidence object, or NULL on failure

		@version 16.5
		*/
		void* create_ev_obj(
			evobj_type_t type,
			int32_t disk_id,
			wchar_t* path,
			void* reserved = NULL)
		{
			return create_ev_obj_(type, disk_id, path, reserved);
		}

		/*!
		@brief Open an evidence object

		If not currently open, opens the specified evidence object in a
		data window (and at the operating system level opens the
		corresponding disk or image file), interprets the image file
		(if the evidence object is an image), loads or takes the volume
		snapshot and returns a handle to the volume that the evidence
		object represents. Use this function if you wish to read data
		from the volume or process the volume snapshot. Potentially
		time-consuming. nFlags must be 0 in v18.0 and older.

		@param[in] evidence  handle of the evidence object to open

		@param[in] flag  one of @ref open_evobj_flag_t (from v18.1,
		otherwise must be 0)

		@return handle to the evidence object or NULL if unsuccessful

		@version 17.6
		*/
		void* open_ev_obj(void* evidence, open_evobj_flag_t flag)
		{
			return open_ev_obj_(evidence, flag);
		}

		/*!
		@brief Close an open evidence object

		Closes the specified evidence object if it is open currently and
		unloads the volume snapshot, otherwise does nothing.

		@param[in] evidence  handle of the evidence object to open

		@version 17.6
		*/
		void close_ev_obj(void* evidence)
		{
			close_ev_obj_(evidence);
		}

		/*!
		@brief Get evidence object properties

		Retrieves information about the specified evidence object. Does
		not require that the evidence object is open. General error return
		code is -1. Available from v17.6.

		@param[in] evidence  handle of the evidence object

		@param[in] prop_type  one of @ref objprop_t

		@param[in] buffer  buffer to recieve string properties

		@return the property (for numeric properties) or the length
		of the string (for string properties) or -1 for failure

		@version 17.6
		*/
		int64_t get_ev_obj_prop(
			void* evidence, objprop_t prop_type, void* buffer = NULL)
		{
			return get_ev_obj_prop_(evidence, prop_type, buffer);
		}

		/*!
		@brief Get evidence object properties

		Retrieves information about the specified evidence object. Does
		not require that the evidence object is open. General error return
		code is -1. Available from v17.6.

		@param[in] evidence  handle of the evidence object

		@param[in] prop_type  one of @ref objprop_t

		@return the requested string

		@version 17.6
		*/
		std::wstring get_ev_obj_prop_s(
			void* evidence, objprop_t prop_type)
		{
			int64_t len;
			std::wstring str;
			if (prop_type < objprop_t::TITLE ||
				prop_type > objprop_t::OUTPUT_DIR)
				return std::wstring(L"");

			if (prop_type > objprop_t::COMMENTS ||
				prop_type == objprop_t::EXT_TITLE ||
				prop_type == objprop_t::ABR_EXT_TITLE)
			{
				str.resize(MAX_PATH);
				len = get_ev_obj_prop_(evidence, prop_type, &str[0]);
				if (len < 0) len = 0;
				else str.resize(len);
			}
			else
			{
				auto ret = (wchar_t*)get_ev_obj_prop_(evidence, prop_type, NULL);
				if (ret > 0) str = ret;
			}

			return str;
		}

		/*!
		@brief Get evidence object hashes

		Retrieves information about a hash from the specified evidence
		object. Does not require that the evidence object is open. General
		error return code is -1. Available from v17.6.

		@param[in] evidence  handle of the evidence object

		@param[in] prop_type  one of the HASH numbers in @ref objprop_t

		@return the requested string

		@version 17.6
		*/
		std::wstring get_ev_obj_prop_hash(
			void* evidence, objprop_t prop_type = objprop_t::HASH)
		{
			auto type = (hash_type_t)get_ev_obj_prop_(
				evidence, (objprop_t)((int)prop_type - 1), NULL);
			uint8_t buffer[32];
			if(get_ev_obj_prop_(evidence, prop_type, &buffer[0]) != -1)
				return get_hash_string(&buffer[0], type);
			else return std::wstring(L"");
		}

		/*!
		@brief Get a handle to an evidence object

		Retrieves a handle to the evidence object with the specified
		unique ID. Returns NULL if not found. The unique ID of an
		evidence object remains the same after closing and re-opening a
		case, whereas the handle will likely change. The evidence object
		number may also change. That happens if the user re-orders the
		evidence objects in the case. The unique ID, however, is
		guaranteed to never change and also guaranteed to be unique
		within the case (actually likely unique even across all the
		cases that the user will even deal with) and can be used to
		reliably recognize a known evidence object.

		@param[in] id  the unique ID of the evidence object

		@return a handle to the evidence object

		@version 18.7
		*/
		void* get_ev_obj(uint32_t id)
		{
			return get_ev_obj_(id);
		}

		/*!
		@brief Get report table information

		Get the name of the nominated report table or the maximum
		number of report tables supported by this version.

		@param[in] reserved  must be NULL

		@param[in] report_table  the report table ID, or -1 to get the
		maximum number of report tables supported

		@param[in,out] optional  must point to 0 when called; if
		@a report_table is set to -1 this will be set to the maximum
		number of report tables or otherwise for v18.1 and later may
		receive a flag from @ref report_table_flag_t

		@return a pointer to a null terminated string containing the name
		of the report table

		@version 17.7
		*/
		void* get_report_table_info(void* reserved, int32_t report_table,
			xways::report_table_flag_t* optional)
		{
			get_report_table_info_(reserved, report_table, optional);
		}

		/*!
		@brief Get report table information

		Get the name of the nominated report table or the maximum
		number of report tables supported by this version.

		@param[in] report_table  the report table ID, or -1 to get the
		maximum number of report tables supported

		@param[in,out] optional  must point to 0 when called; if
		@a report_table is set to -1 this will be set to the maximum
		number of report tables or otherwise for v18.1 and later may
		receive a flag from @ref report_table_flag_t

		@return a std::wstring containing the name of the report table

		@version 17.7
		*/
		std::wstring get_report_table_info(int32_t report_table,
			xways::report_table_flag_t* optional = NULL)
		{
			xways::report_table_flag_t flag =
				(xways::report_table_flag_t)0;
			auto str =
				(wchar_t*)get_report_table_info_(NULL, report_table, &flag);
			if (optional) *optional = flag;
			if (!str) str = L"";
			return str;
		}

		/*!
		@brief Get report table associations for an evidence object

		Returns a pointer to an internal list that describes all report
		table associations of the specified evidence object. Scanning this
		list is a much quicker way to find out which items are associated
		with a given report table than calling @ref
		get_report_table_assocs for all items in a volume snapshot,
		especially if the snapshot is huge. The list consists of 16-bit
		report table ID and 32-bit item ID pairs repeatedly, stored back
		to back. The integer pointed to by value is set to the number
		of pairs in the data structure.

		@param[in] evidence  handle to the evidnece object

		@param[in] flags  set to 0x01 to request a list sorted by item IDs

		@param[in] value  receives the number of pairs in the returned
		data structure

		@return pointer to a data structure containing @a value report
		table ID/item ID pairs

		@version 17.7
		*/
		report_assoc_item_t* get_ev_obj_report_table_assocs(
			void* evidence, int32_t flags, int32_t* value)
		{
			return get_ev_obj_report_table_assocs_(evidence, flags, value);
		}

		///@}
		///@name Volume Snapshot Management Functions

		/*!
		@brief Select the volume snapshot for future operations

		@param[in] volume  handle to the volume
		*/
		void select_volume_snapshot(void* volume)
		{
			return select_volume_snapshot_(volume);
		}

		/*!
		@brief Retrieve information about the current volume snapshot

		@param[in] prop_type  property type, one of @ref vsprop_t

		@param[in] buffer  points to a @ref special_item_t

		@return the requested property

		@version 17.4
		*/
		int64_t get_vs_prop(vsprop_t prop_type, special_item_t* buffer = NULL)
		{
			return get_vs_prop_(prop_type, buffer);
		}

		/*!
		@brief Get the number of items in the current volume snapshot

		Retrieves the number of items in the current volume snapshot
		(files and directories). Item IDs are consecutive 0-based. That
		means the ID of the first item has the ID 0 and the last item in
		a volume snapshot has the ID (@ref get_item_count - 1). You
		address each and every item in that range, be it a file or
		directory, by specifying its ID.

		@param[in] volume  handle to the volume

		@return the number of items
		*/
		uint32_t get_item_count(void* volume)
		{
			return get_item_count_(volume);
		}

		/*!
		@brief Get the number of files in a directory

		Retrieves the accumulated number of files in the directory with
		the specified ID and all its subdirectories. Also works for files
		that have child objects. Not currently supported for the root
		directory. You may specify -1 as the ID instead to get the total
		file count of the entire volume snapshot.

		@param[in] directory  id of the directory

		@return the number of files

		@version 17.7
		*/
		uint32_t get_file_count(int32_t directory)
		{
			return get_file_count_(directory);
		}

		/*!
		@brief Create a new item (file or directory) in the volume
		snapshot

		May be called when refining the volume snapshot. Should be
		followed by calls to @ref set_item_parent, @ref set_item_size,
		@ref set_item_information, and/or @ref set_item_ofs.

		For example, if you are creating a file carved from the sectors of
		the evidence object, you can specify the file size using
		@ref set_item_size and the start offset via the @a offset
		parameter (must be negative) using set_item_ofs.

		@param[in] name  the name that this file will have in the volume
		snapshot

		@param[in] flags  @ref create_flag_t::EXPECT_MORE if more items are
		expected to be created (non of the other flags are supported)

		@return ID of the newly created item, or -1 if an error occurred
		(e.g. out of memory)
		*/
		long int create_item(
			wchar_t* name, create_flag_t flags = (create_flag_t)0)
		{
			return create_item_(name, flags);
		}

		/*!
		@brief Create a new item (file or directory) in the volume
		snapshot

		May be called when refining the volume snapshot. Should be
		followed by calls to @ref set_item_parent, @ref set_item_size,
		@ref set_item_information, and/or @ref set_item_ofs.

		For example, if you are creating a file carved from the sectors of
		the evidence object, you can specify the file size using
		@ref set_item_size and the start offset via the @a offset
		parameter (must be negative) using set_item_ofs.

		@param[in] name  the name that this file will have in the volume
		snapshot

		@param[in] flags  @ref create_flag_t::EXPECT_MORE if more items are
		expected to be created (non of the other flags are supported)

		@return ID of the newly created item, or -1 if an error occurred
		(e.g. out of memory)
		*/
		long int create_item(
			std::wstring name, create_flag_t flags = (create_flag_t)0)
		{
			return create_item_((wchar_t*)name.c_str(), flags);
		}

		/*!
		@brief Create a new file in the volume snapshot

		Similar to @ref create_item, but also allows to attach an external
		file to the volume snapshot or to define a file that is an excerpt
		of another file (its parent). Should be followed by a call to @ref
		set_item_size (not necessary if attaching an external file) or
		set_item_information (not necessary when carving a file in a
		file).

		@param[in] name  name that this file will have in the volume
		snapshot, which may be different from its source file name if you
		are attaching an external file

		@param[in] flags  one or more of @ref create_flag_t

		@param[in] parent  id of the parent item

		@param[in] source  source of the file's data - exact meaning
		depends on the flags.

		@return ID of the newly created item, or -1 if an error occurred
		(e.g. out of memory)
		*/
		long int create_file(
			wchar_t* name,
			create_flag_t flags,
			int32_t parent,
			void* source)
		{
			return create_file_(name, flags, parent, source);
		}

		/*!
		@brief Create a new file in the volume snapshot

		Similar to @ref create_item, but also allows to attach an external
		file to the volume snapshot or to define a file that is an excerpt
		of another file (its parent). Should be followed by a call to @ref
		set_item_size (not necessary if attaching an external file) or
		set_item_information (not necessary when carving a file in a
		file).

		@param[in] name  name that this file will have in the volume
		snapshot, which may be different from its source file name if you
		are attaching an external file

		@param[in] flags  one or more of @ref create_flag_t

		@param[in] parent  id of the parent item

		@param[in] source  source of the file's data - exact meaning
		depends on the flags.

		@return ID of the newly created item, or -1 if an error occurred
		(e.g. out of memory)
		*/
		long int create_file(
			std::wstring name,
			create_flag_t flags,
			int32_t parent,
			std::wstring source)
		{
			return create_file_(
				(wchar_t*)name.c_str(),
				flags,
				parent,
				(void*)source.c_str());
		}

		///@}
		///@name Volume Snapshot Item Property Functions

		/*!
		@brief Get the name of an item

		Retrieves a pointer to a null-terminated wchar_t string containing
		the name of the specified item (file or directory). You may call
		get_item_name and get_item_parent repeatedly until you reach the
		root directory and concatenate the results to get the full path of
		an item.

		@param[in] item  the id of the item

		@return null-terminated wchar_t* string containing the name of the
		item
		*/
		const wchar_t* get_item_name(int32_t item)
		{
			return get_item_name_(item);
		}

		/*!
		@brief Get the name of an item

		Retrieves a pointer to a null-terminated wchar_t string containing
		the name of the specified item (file or directory). You may call
		get_item_name and get_item_parent repeatedly until you reach the
		root directory and concatenate the results to get the full path of
		an item.

		@param[in] item  the id of the item

		@return null-terminated wchar_t* string containing the name of the
		item
		*/
		std::wstring get_item_name_s(int32_t item)
		{
			return std::wstring(get_item_name_(item));
		}

		/*!
		@brief Get the size of an item

		@param[in] item  the id of the item

		@return size of the item (file or directory) in bytes; -1 means
		unknown size
		*/
		int64_t get_item_size(int32_t item)
		{
			return get_item_size_(item);
		}

		/*!
		@brief Sets the size of an item

		@param[in] item  the id of the item

		@param[in] size  the size of the item
		*/
		void set_item_size(int32_t item, int64_t size)
		{
			return set_item_size_(item, size);
		}

		/*!
		@brief Get the offset of an item

		Retrieves the offset of the file system data structure (e.g. NTFS
		FILE record) where the item is defined. If negative, the absolute
		value is the offset where a carved file starts on the volume.
		0 if an error occurred. 0xFFFFFFFF if not available/not
		applicable.

		Also retrieves the number of the sector from the point of the
		volume in which the data of the item starts.

		@param[in] item  the id of the item

		@param[in] offset  the offset of the item

		@param[in] sector  the starting sector of the item
		*/
		void get_item_ofs(
			int32_t item,
			int64_t* offset,
			int64_t* sector)
		{
			return get_item_ofs_(item, offset, sector);
		}

		/*!
		@brief Set the offset of an item

		Sets the offset of the file system data structure (e.g. NTFS
		FILE record) where the item is defined. If negative, the absolute
		value is the offset where a carved file starts on the volume.
		0 if an error occurred. 0xFFFFFFFF if not available/not
		applicable.

		Also sets the number of the sector from the point of the
		volume in which the data of the item starts.

		@param[in] item  the id of the item

		@param[in] offset  the offset of the item

		@param[in] sector  the starting sector of the item
		*/
		void set_item_ofs(
			int32_t item,
			int64_t offset,
			int64_t sector)
		{
			return set_item_ofs_(item, offset, sector);
		}

		/*!
		@brief Get information about an item stored in a volume snapshot

		Returns information about an item (file or directory) as stored in
		the volume snapshot, such as the original ID or attributes that
		the item had in its defining file system. What information is
		actually returned depends on nInfoType. All timestamps are
		transferred in Windows FILETIME format.

		@param[in] item  the ID of the item to return information about

		@param[in] info_type  the type of information to return, one of
		@ref item_info_t

		@param[out] success  true for success, false otherwise (may be
		NULL if not required)

		@return the requested item information
		*/
		int64_t get_item_information(
			int32_t item, xways::item_info_t info_type, bool* success = NULL)
		{
			return get_item_information_(item, info_type, success);
		}

		/*!
		@brief Set information about an item stored in a volume snapshot

		Sets information about an item (file or directory) in the volume
		snapshot.

		@param[in] item  the ID of the item to return information about

		@param[in] info_type  the type of information to return, one of
		@ref item_info_t

		@param[in] info_value  the value to set

		@return true for success, false otherwise
		*/
		bool set_item_information(
			int32_t item,
			item_info_t info_type,
			int64_t info_value)
		{
			return set_item_information_(item, info_type, info_value);
		}

		/*!
		@brief Get the type of the specified item

		Retrieves a description of the type of the specified file and
		returns information about the status of the type detection of the
		file. The type description parameter can be NULL if not required.

		@param[in] item  the ID of the item to return the type of

		@param[out] type  pointer to buffer to receive the type string

		@param[in] type_len  the length of the buffer in wchar_t
		characters

		@return type status, one of @ref type_status_t (note that newly
		identified files can have a numerical value higher than
		@ref type_status_t::NEWLY_IDENTIFIED)
		*/
		xways::type_status_t get_item_type(
			int32_t item, wchar_t* type = NULL, int32_t type_len = NULL)
		{
			return get_item_type_(item, type, type_len);
		}

		/*!
		@brief Get the type of the specified item

		Retrieves a description of the type of the specified file and
		returns information about the status of the type detection of the
		file.

		@param[in] item  the ID of the item to return the type of

		@param[out] type status  type status, one of @ref type_status_t
		(note that newly identified files can have a numerical value
		higher than @ref type_status_t::NEWLY_IDENTIFIED)

		@return std::wstring containing the type
		*/
		std::wstring get_item_type_s(
			int32_t item, xways::type_status_t* type_status = NULL)
		{
			std::wstring str;
			str.resize(32);
			auto status =
				get_item_type_(item, &str[0],(int32_t)str.length());
			if (type_status) *type_status = status;
			auto pos = str.find_first_of(L'\0');
			if (pos < str.length()) str.resize(pos);
			return str;
		}

		/*!
		@brief Set the type of the specified item

		Sets a description of the type of the specified file (or specify
		NULL if not required) and information about the status of the type
		detection of the file.

		@param[in] item  the ID of the item to return the type of

		@param[in] type  pointer to NULL-terminated wchar_t* string
		containing the type string

		@param[in] length  the length of the buffer in wchar_t
		characters
		*/
		void set_item_type(
			int32_t item,
			wchar_t*type,
			int32_t length)
		{
			return set_item_type_(item, type, length);
		}

		/*!
		@brief Get the parent of the specified item

		@param[in] item  the ID of the item to return the parent of

		@return the ID of the parent of the item, or -1 if the item is the
		root directory or if for some strange reason no parent object is
		assigned
		*/
		int32_t get_item_parent(int32_t item)
		{
			return get_item_parent_(item);
		}


		/*!
		@brief Set the parent of the specified item

		If the parent is a file that does not have child objects yet, you
		should use @ref set_item_information to mark it has having child
		objects.

		@param[in] item  the ID of the item to set the parent of

		@param[in] parent  the ID of the parent, or -1 for the virtual
		"Path unknown" directory or -2 for the "Carved files" directory
		*/
		void set_item_parent(int32_t item, int32_t parent)
		{
			return set_item_parent_(item, parent);
		}

		/*!
		@brief Get the report tables associated with the item

		Retrieves the names of the report tables that the specified item
		is associated with. The names are delimited with comma and space.
		If the buffer was filled completely, that likely means that the
		specified buffer length was insufficient. In v17.6 SR-7 and later,
		returns the total number of associations of that file, and
		@a buffer may be NULL.

		@param[in] item  the ID of the item to get the report table
		associations of

		@param[out] buffer  pointer to a buffer to receive the list of
		report table associations (in v17.6 SR-7 and later may be NULL)

		@param[in] buffer_len  length of @a buffer in wchar_t characters

		@return  total number of associations of that file (v17.6 SR-7)
		*/
		int32_t get_report_table_assocs(
			int32_t item,
			wchar_t* buffer,
			int32_t buffer_len)
		{
			return get_report_table_assocs_(item, buffer, buffer_len);
		}

		/*!
		@brief Associate an item with a report table

		Associates the specified file with the specified report table. If
		the report table does not exist yet in the currently active case,
		it will be created.

		@param[in] item  the ID of the item to get the report table
		associations of

		@param[out] report_table  pointer to a null-terminated @a wchar_t
		string containing the name of the report table

		@param[in] flags  one or more of #report_table_flag_t

		@return 1 if the file was successfully and newly associated with
		the report table, 2 if that association existed before, or 0 in
		case of failure
		*/
		int32_t add_to_report_table(
			int32_t item,
			wchar_t* report_table,
			report_table_flag_t flags)
		{
			return add_to_report_table_(item, report_table, flags);
		}

		/*!
		@brief Get the examiner comment for an item

		@param[in] item  the ID of the item to get the report table
		associations of

		@return pointer to the comment of an item, if any, otherwise
		NULL; this pointer is guaranteed to be valid only at the time
		when you retrieve it
		*/
		wchar_t* get_comment(int32_t item)
		{
			return get_comment_(item);
		}

		/*!
		@brief Get the examiner comment for an item

		@param[in] item  the ID of the item to get the report table
		associations of

		@return std::wstring containing the comment of an item, if any
		*/
		std::wstring get_comment_s(int32_t item)
		{
			wchar_t* comment = get_comment_(item);
			if (!comment) comment = L"";
			return std::wstring(comment);
		}

		/*!
		@brief Add an examiner comment for an item

		@param[in] item  the ID of the item to get the report table
		associations of

		@param[in] comment  pointer to a null-terminated @a wchar_t
		string containing the comment

		@param[in] flag  either of #replace_flag_t to indicate if any
		existing comment should be replaced

		@return success or failure
		*/
		bool add_comment(
			int32_t item,
			wchar_t* comment,
			replace_flag_t flag = replace_flag_t::APPEND_NEWLINE)
		{
			return add_comment(item, comment, flag);
		}

		/*!
		@brief Get the extracted metadata for an item

		Retrieves a pointer to the internal metadata of a file as stored
		in the volume snapshot (as extracted by volume snapshot
		refinement and as seen in the Metadata column), or returns NULL
		if no such metadata exists. The pointer is guaranteed to be valid
		only at the time when you retrieve it. If you wish to do something
		with the text that it points to after your X-Tension returns
		control to X-Ways Forensics, you need to copy it to your own
		buffer.

		@param[in] item  the ID of the item to get the extracted metadata
		for

		@return pointer to a null-terminated @a wchar_t string containing
		the metadata

		@sa get_metadata.

		@version 17.7
		*/
		wchar_t* get_extracted_metadata(int32_t item)
		{
			return get_extracted_metadata_(item);
		}

		/*!
		@brief Get the extracted metadata for an item

		Retrieves a pointer to the internal metadata of a file as stored
		in the volume snapshot (as extracted by volume snapshot
		refinement and as seen in the Metadata column), or returns NULL
		if no such metadata exists. The pointer is guaranteed to be valid
		only at the time when you retrieve it. If you wish to do something
		with the text that it points to after your X-Tension returns
		control to X-Ways Forensics, you need to copy it to your own
		buffer.

		@param[in] item  the ID of the item to get the extracted metadata
		for

		@return std::wstring containing the metadata

		@sa get_metadata.

		@version 17.7
		*/
		std::wstring get_extracted_metadata_s(int32_t item)
		{
			auto str = get_extracted_metadata_(item);
			if (!str) str = L"";
			return str;
		}

		/*!
		@brief Add extracted metadata to an item

		Retrieves a pointer to the internal metadata of a file as stored
		in the volume snapshot (as extracted by volume snapshot
		refinement and as seen in the Metadata column), or returns NULL
		if no such metadata exists. The pointer is guaranteed to be valid
		only at the time when you retrieve it. If you wish to do something
		with the text that it points to after your X-Tension returns
		control to X-Ways Forensics, you need to copy it to your own
		buffer.

		@param[in] item  the ID of the item to add the extracted metadata
		to

		@param[in] metadata  pointer to a null-terminated @a wchar_t
		stirng containing the metadata

		@param[in] flag  either of #replace_flag_t to indicate if any
		existing comment should be replaced

		@return pointer to the metadata

		@sa get_metadata.
		@sa get_extracted_metadata.

		@version 17.7
		*/
		bool add_extracted_metadata(
			int32_t item,
			wchar_t* metadata,
			replace_flag_t flag)
		{
			return add_extracted_metadata_(item, metadata, flag);
		}

		/*!
		@brief Get the hash value for an item

		Retrieves the hash value of a a file if one has been computed,
		which can be checked using #get_item_information().

		To find out the type of the hash values, use #vsprop_t::HASHTYPE.
		The buffer should be large enough to receive a hash of the
		selected type and should be prepared and filled with a @a uint32_t
		value of 0x00000001. In v18.0 SR-12, v18.1 SR-7, v18.2 SR-5, v18.3
		SR-4 and later you may fill the buffer with a @a uint32_t value of
		0x00000002 to request the secondary hash value of a file instead of
		the primary hash value.

		Deprecated: In v18.0 through v18.5 you may negate @a item to
		retrieve the secondary hash value of @a item, if any is available.

		Subject to change at any time: In v18.8 and later, this function
		may also be used to retrieve pre-computed PhotoDNA hash values
		from the volume snapshot. For that purpose the buffer must be
		filled with a @a uint32_t value of 0x00000100 for the first
		PhotoDNA hash value, 0x00000101 for the 2nd, and 0x00000102 for
		the 3rd. Note that for most items, if at all, X-Ways Forensics
		only computes 1 such hash value. Note that it depends on the user
		whether or not PhotoDNA hash values are permanently stored in the
		database, which is a precondition for this function to work. The
		buffer must have space for 144 bytes. The function returns @a true
		if a PhotoDNA hash value was available and actually stored in
		@a buffer.

		@param[in] item  the ID of the item to get the hash value of

		@param[in] buffer  pointer to a buffer of a length appropriate
		to the hash type

		@return  true if no I/O error occurred when copying the hash
		value into the buffer, @version 17.4

		@version 16.8
		*/
		bool get_hash_value(int32_t item, void* buffer)
		{
			return get_hash_value_(item, buffer);
		}

		/*!
		@brief Get the hash value for an item

		Retrieves the hash value of a a file if one has been computed,
		which can be checked using #get_item_information().

		To find out the type of the hash values, use #vsprop_t::HASHTYPE.
		The buffer should be large enough to receive a hash of the
		selected type and should be prepared and filled with a @a uint32_t
		value of 0x00000001. In v18.0 SR-12, v18.1 SR-7, v18.2 SR-5, v18.3
		SR-4 and later you may fill the buffer with a @a uint32_t value of
		0x00000002 to request the secondary hash value of a file instead of
		the primary hash value.

		Deprecated: In v18.0 through v18.5 you may negate @a item to
		retrieve the secondary hash value of @a item, if any is available.

		Subject to change at any time: In v18.8 and later, this function
		may also be used to retrieve pre-computed PhotoDNA hash values
		from the volume snapshot. For that purpose the buffer must be
		filled with a @a uint32_t value of 0x00000100 for the first
		PhotoDNA hash value, 0x00000101 for the 2nd, and 0x00000102 for
		the 3rd. Note that for most items, if at all, X-Ways Forensics
		only computes 1 such hash value. Note that it depends on the user
		whether or not PhotoDNA hash values are permanently stored in the
		database, which is a precondition for this function to work. The
		buffer must have space for 144 bytes. The function returns @a true
		if a PhotoDNA hash value was available and actually stored in
		@a buffer.

		@param[in] item  the ID of the item to get the hash value of

		@param[in] type  type of hash, one of xways::hash_type_t

		@return  std::wstring containing hash, @version 17.4

		@version 16.8
		*/
		std::wstring get_hash_value(int32_t item, hash_type_t type)
		{
			uint8_t buffer[32];
			if (get_hash_value_(item, &buffer[0]))
				return get_hash_string(&buffer[0], type);
			else return std::wstring(L"");
		}

		///@}
		///@name File Contents Functions

		/*!
		@brief Get the metadata for an item

		Extracts internal metadata of a file to memory and returns a
		pointer to it if successful, or NULL otherwise. The pointer is
		guaranteed to be valid only at the time when you retrieve it. If
		you wish to do something with the text that it points to after
		your X-Tension returns control to X-Ways Forensics, you need to
		copy it to your own buffer. Unlike @ref get_extracted_metadata,
		the file must have been opened with open_item because this
		function reads from the file contents, not from data stored in the
		volume snapshot. The metadata is taken from the very file that
		contains it, for example in the case of zip-style Office documents
		from the XML files.

		@param[in] item  the ID of the item to get the metadata of

		@param[in] handle  handle to the item (must be open)

		@return pointer to a null-terminated @a wchar_t string containing
		the metadata or NULL if unsuccessful

		@version 17.7
		*/
		wchar_t* get_metadata(int32_t item, void* handle)
		{
			return get_metadata_(item, handle);
		}

		/*!
		@brief Get the metadata for an item

		Can extract extensive internal metadata of files of various types,
		exactly as seen in Details mode in X-Ways Forensics, typically much
		more than @ref xways::get_metadata. Fills a memory buffer with
		either null-terminated UTF-16 plain text or null-terminated ASCII
		HTML code, and returns a pointer to it. You may parse the buffer to
		retrieve specific metadata that you need. The format may
		theoretically change from one version to the other. You must
		release the allocated memory by passing that pointer to
		@ref xways::release_mem when you do not need it any more.

		Unlike @ref xways::get_metadata, this function is thread-safe.
		Unlike @ref xways::get_extracted_metadata, the file must have been
		opened with @ref xways::open_item because this function reads from
		the file contents, not from data stored in the volume snapshot. The
		metadata is taken from the very file that contains it, for example
		in the case of zip-style Office documents from the XML files.

		@param[in] item  the ID of the item to get the metadata from

		@param[in] flags  points to flags for input and output. The only
		currently defined input flag is 0x01. It tells X-Ways Forensics to
		extract only a subset of the available metadata, as shown in the
		software in the Metadata column. The resulting buffer currently
		contains always UTF-16 plain text. If the flag is not set, the
		complete metadata as seen in Details mode is extracted. The
		resulting buffer can be of either above-mentioned formats. The only
		currently defined output flag is 0x01 (indicating, if set, that the
		resulting buffer is HTML-formatted ASCII instead of UTF-16 plain
		text). If any of the 8 highest bits are set, you should not touch the
		resulting buffer, just release it with XWF_ReleaseMem. The output
		flags are valid only if the function was successful, i.e. if it has
		returned a pointer.

		@return pointer to a null-terminated @a wchar_t string containing
		the metadata or NULL if unsuccessful.

		@version 18.9
		*/
		void* get_metadata_ex(void* item, uint32_t* flags)
		{
			return get_metadata_ex(item, flags);
		}

		/*!
		@brief Get a raster image of an item

		Provides a standardized true-color RGB raster image representation
		for any picture file type that is supported internally in X-Ways
		Forensics (e.g. JPEG, GIF, PNG, ...), with 24 bits per pixel. The
		result is a pointer to a memory buffer, or NULL if not successful
		(e.g. if not a supported file type variant or the file is too
		corrupt). The caller is responsible for releasing the allocated
		memory buffer when no longer needed, by calling the Windows API
		function @a VirtualFree, with parameters @a dwSize = 0 and @a
		dwFreeType = @a MEM_RELEASE.

		@param[in,out] image_info  pointer to a @ref raster_image_info_t
		struct that contains an ID and handle of the item and receives
		information about the returned image data

		@return pointer to a buffer containing the image data of NULL if
		unsuccessful

		@version 18.0
		*/
		void* get_raster_image(struct raster_image_info_t* image_info)
		{
			return get_raster_image_(image_info);
		}

		///@}
		///@name Search-Related Functions

		/*!
		@brief Start a simultaneous search on a volume

		Must only be called from XT_Prepare or XT_Finalize. Runs a
		simultaneous search for multiple search terms in the specified
		volume. The volume must be associated with an evidence object.
		Note that if this function is called as part of volume snapshot
		refinement, it can be called automatically for all selected
		evidence objects if the user applies the X-Tension to all
		selected evidence objects. Returns a negative value in case of an
		error.

		@param[in] search_info  pointer to a @ref search_info_t struct

		@param[in] code_pages  pointer to a @ref code_pages_t struct

		@return negative for error

		@version 16.5
		*/
		int32_t search(search_info_t* search_info, code_pages_t* code_pages)
		{
			return search_(search_info, code_pages);
		}

		/*!
		@brief Add a new search term

		Creates a new search term and returns its ID or, if @ref
		search_term_flag_t::ALLOW_REUSE is specified, alternatively returns
		the ID of an existing search term with the same name, if any.

		The maximum number of search terms in a case is currently 8,191
		(in v18.5). Use this function if you wish to automatically
		categorize search hits (assign them to different search terms)
		while responding to calls of @a XT_ProcessSearchHit or using
		@a set_search_hit (not currently implemented).

		@param[in] search_term  pointer to a null-terminated @a wchar_t
		string containing the search term

		@param[in] flags  one or more of @ref search_term_flag_t

		@return ID of the search term or -1 in case of an error

		@version 18.5
		*/
		int32_t add_search_term(wchar_t* search_term, search_term_flag_t flags)
		{
			return add_search_term_(search_term, flags);
		}

		/*!
		@brief Get a search term from a search term ID or the total number
		of search terms

		@param[in] search_term  ID of the search term to get, or -1 to get
		the total number of search terms

		@param[in] reserved  must be NULL

		@return pointer to a null-terminated @a wchar_t string containing
		the search term, NULL if no such term exists or the total number
		of search terms if @a search_term was set to -1, (requires cast to
		@a int)

		@version 17.7
		*/
		wchar_t* get_search_term(int32_t search_term, void* reserved = NULL)
		{
			return get_search_term_(search_term, reserved);
		}

		///@}
		///@name Event-Related Functions

		/*!
		@brief Add an event to the event hit list of an evidence object

		Allows to add events to the internal event hit list of an evidence
		object. The internal event is loaded and accessible only if the
		evidence object is open. Returns 1 if the event was successfully
		added, 2 if deliberatedly ignored, or 0 in case of failure to
		signal that the caller should stop adding more events.

		@param[in] event_info  pointer to an @ref event_info_t struct

		@return 1 if successful, 2 if delibarately ignored and 0 for
		failure in which case the caller should stop adding events

		@version 17.6
		*/
		int32_t add_event(event_info_t* event_info)
		{
			return add_event_(event_info);
		}

		/*!
		@brief Get an event from the event hit list of an evidence object

		Retrieves information about an event from the internal event hit
		list of an evidence object. The internal event is loaded and
		accessible only if the evidence object is open. The structure will
		be populated with values as described above, except where noted.

		@param[in] event  the event number

		@param[in] event_info  pointer to an @ref event_info_t struct

		@return MAXDWORD on failure or otherwise the length of the
		description copied into the lpDescr buffer, in bytes, or 0
		if there is no description

		@version 18.1
		*/
		int32_t get_event(uint32_t event, event_info_t* event_info)
		{
			return get_event_(event, event_info);
		}

		///@}
		///@name Evidence File Container Functions

		/*!
		@brief Create or open an evidence file container

		Currently only 1 container can be open at a time for filling. If a
		container is open already when this function is called, it will be
		closed automatically.

		@param[in] filename  null-terminated @a wchar_t string containing
		the filename of the container

		@param[in] flags  one or more flags from @ref ctr_create_flag_t

		@return a handle to the evidence file container

		@version 16.5
		*/
		void* create_container(
			wchar_t* filename,
			ctr_create_flag_t flags = (ctr_create_flag_t)0,
			void* reserved = 0)
		{
			return create_container_(filename, flags, reserved);
		}

		/*!
		@brief Copy a file to an evidence file container

		@param[in] container  handle to the evidence file container

		@param[in] handle  handle to the file to be added

		@param[in] flags  one or more flags from @ref ctr_copy_flag_t

		@param[in] mode  one of @ref ctr_mode_flag_t

		@param[in] start_offset  starting offset of the range (should be
		-1 unless @a mode is ctr_mode_flag_t::RANGE_ONLY

		@param[in] end_offset  ending offset of the range (should be -1
		unless @a mode is ctr_mode_flag_t::RANGE_ONLY

		@param[in] reserved

		@return 0 if successful, otherwise an error code which, if
		negative indicates no further attempt should be made to fill the
		container

		@version 16.5
		*/
		int32_t copy_to_container(
			void* container,
			void* handle,
			ctr_copy_flag_t flags,
			ctr_mode_flag_t mode,
			int64_t start_offset,
			int64_t end_offset,
			void* reserved = 0)
		{
			return copy_to_container_(container, handle, flags,
				mode, start_offset, end_offset, reserved);
		}

		/*!
		@brief Close an evidence file container

		@param[in] container  handle to the evidence file container

		@param[in] reserved

		@return 1 if successful

		@version 16.5
		*/
		int32_t close_container(void* container, void* reserved = 0)
		{
			return close_container_(container, reserved);
		}

		///@}
		///@name Miscellaneous Functions

		/*!
		@brief Print a message to the Messages window

		Outputs the specified message in the Messages window. You may use
		this function for example to alert the user of errors or to
		output debug information.

		@param[in] message  pointer to a null-terminated @a wchar_t string
		containing the message

		@param[in] flags  one or more flags contained in @ref msg_flag_t
		*/
		void output_message(const wchar_t* message,
			msg_flag_t flags = msg_flag_t::UTF16)
		{
			output_message_(message, flags);
		}

		/*!
		@brief Print a message to the Messages window

		Outputs the specified message in the Messages window. You may use
		this function for example to alert the user of errors or to
		output debug information.

		@param[in] message  a @a std::wstring containing the message

		@param[in] flags  one or more flags contained in @ref msg_flag_t
		*/
		void output_message(std::wstring message,
			msg_flag_t flags = msg_flag_t::UTF16)
		{
			output_message_(message.c_str(), flags);
		}

		/*!
		@brief Print a message to the Messages window

		Outputs the specified message in the Messages window. You may use
		this function for example to alert the user of errors or to
		output debug information.

		@param[in] message  pointer to a null-terminated @a char string
		containing the message

		@param[in] flags  one or more flags contained in @ref msg_flag_t
		*/
		void output_message(const char* message,
			msg_flag_t flags = msg_flag_t::ANSI)
		{
			output_message_((wchar_t*)message, flags);
		}

		/*!
		@brief Print a message to the Messages window

		Outputs the specified message in the Messages window. You may use
		this function for example to alert the user of errors or to
		output debug information.

		@param[in] message  a @a std::string containing the message

		@param[in] flags  one or more flags contained in @ref msg_flag_t
		*/
		void output_message(std::string message,
			msg_flag_t flags = msg_flag_t::ANSI)
		{
			output_message_((wchar_t*)message.c_str(), flags);
		}

		/*!
		@brief Display a dialog to get user input

		Requests user input through a dialog window provided by X-Ways
		Forensics. This input can be text (e.g. a password) or can be
		directly interpreted as an integer number. The text in @a message
		is presented to the user (truncated if too long) as an explanation
		for what is required, and the text entered by the user is filled
		into @a buffer (not when requesting a positive integer number).

		Suggested user input can be supplied by filling @a buffer prior
		to calling this function, otherwise @a buffer must start with a
		NULL character.

		@param[in] message  pointer to a null-terminated @a wchar_t string
		containing a prompt message

		@param[in,out] buffer  pointer to a buffer to receive the @a
		wchar_t string containing user entered text; pre-load with a null-
		terminated string to be provided to the user, or a null string;
		must be zero if @a flags is @ref user_input_flag_t::POSITIVE_INT

		@param[in] buffer_len  size @a buffer, must be zero if @a flags is
		@ref user_input_flag_t::POSITIVE_INT

		@param[in] flags  one or more flags contained in @ref
		user_input_flag_t

		@return  number of characters filled into the buffer (excluding
		the terminating null character), or -1 if the user clicked Cancel,
		or the number entered if @a flags was set to @ref
		user_input_flag_t::POSITIVE_INT

		@version 18.5
		*/
		int64_t get_user_input(
			wchar_t* message,
			wchar_t* buffer,
			uint32_t buffer_len,
			user_input_flag_t flags = (user_input_flag_t)0)
		{
			return get_user_input_(message, buffer, buffer_len, flags);
		}

		/*!
		@brief Display a progress indicator

		Creates a progress indicator window with the specified caption.
		You should call @a PeekMessage, @a TranslateMessage,
		@a DispatchMessage occasionally to allow for the main window to
		remain responsive. You must not use any of the progress indicator
		functions when implementing @a XT_ProcessItem or @a
		XT_ProcessItemEx or when calling API functions that create a
		progress bar themselves.

		@param[in] caption  pointer to a null-terminated @a wchar_t string
		containing a caption

		@param[in] flags  one or more flags contained in @ref
		progress_flag_t
		*/
		void show_progress(
			wchar_t* caption, progress_flag_t flags = (progress_flag_t)0)
		{
			return show_progress_(caption, flags);
		}

		/*!
		@brief Set the progress indicator percentage

		@param[in] percentage  the percentage to set
		*/
		void set_progress_percentage(uint32_t percent)
		{
			return set_progress_percentage_(percent);
		}

		/*!
		@brief Set the progress description

		@param[in] description  pointer to a null-terminated @a
		wchar_t string containing the text to display
		*/
		void set_progress_description(wchar_t* description)
		{
			return set_progress_description_(description);
		}

		/*!
		@brief Check if the user indicated the operation should stop

		When a progress indicator window is on the screen and you call
		@a PeekMessage etc. regularly, you can use this function to check
		whether the user wants to abort the operation.

		@return true if the operation should stop, false otherwise
		*/
		bool should_stop(void) { return should_stop_(); }

		/*!
		@brief Close the progress indicator window
		*/
		void hide_progress(void) { hide_progress_(); }

		disk_desc_t parse_disk_desc(std::wstring text)
		{
			// List of strings to parse for
			const std::vector<std::wstring>disk_desc_str = {
				L"Acquired on: ",
				L"AppVer: ",
				L"Bytes per sector: ",
				L"Case: ",
				L"Chunk size: ",
				L"Disk signature: ",
				L"EvNo: ",
				L"Examiner: ",
				L"Internal .e01 description: ",
				L"Notes: ",
				L"OSVer: ",
				L"Partition ",
				L"Partitioning style: ",
				L"Sector count: ",
				L"Total capacity: ",
				L"Unpartitionable space: ",
				L"Unused inter-partition space:"
			};

			// The descrition to return
			disk_desc_t desc;

			// Current line
			std::wstring line;

			// List of partition end sectors
			std::vector<uint64_t> ends;

			// Stream containing the input text
			std::wstringstream ss(text);

			// Continue until the end of the input text
			while (!ss.eof())
			{
				// Length of the target string
				size_t len;

				// Get the next line
				line = get_next_line(ss);

				// Search for the line in the list of strings - the upper
				// bound will be the first item that is larger than the
				// match string so if there is a matching string it will
				// be the one immediately before
				auto it = std::upper_bound(disk_desc_str.begin(),
					disk_desc_str.end(), line);

				// First string offset means it is probably starting with a
				// number, which means chunks expected or chunks referenced
				if (it == disk_desc_str.begin())
				{
					// Trim any carriage return
					if (line.back() == '\r') line.pop_back();

					// Get the length to do range checking
					len = line.length();

					// Check for either chunks expected or referenced
					if (len >= 15 && !line.compare(
						line.length() - 15, 15, L"chunks expected"))
					{
						// Chunks expected - parse it and save it
						desc.chunks_expected = parse_int(line, 0);
					}
					else if (len >= 26 && !line.compare(
						line.length() - 26, 26, L"chunks actually referenced"))
					{
						// Chunks referenced - parse it and save it
						desc.chunks_actual = parse_int(line, 0);
					}
					continue;  // Neither
				}

				// Get the item before the current item and look
				// for an exact match
				it--;
				if (!line.compare(0, len = it->length(), *it))
				{
					switch (it - disk_desc_str.begin())
					{
					case 0: // Acquired on
						parse_string(line, len);
						desc.acquired = line;
						break;
					case 1: // App ver
						parse_string(line, len);
						desc.app_ver = line;
						break;
					case 2: // Bytes per sector
						desc.sector_size = parse_int(line, len);
						break;
					case 3: // Case
						parse_string(line, len);
						desc.case_name = line;
						break;
					case 4: // Chunk size
						desc.chunk_size = parse_int(line, len);
						break;
					case 5: // Disk sig
						parse_string(line, len);
						desc.disk_sig = line;
						break;
					case 6: // Evidence number
						parse_string(line, len);
						desc.evidence_no = line;
						break;
					case 7: // Examiner
						parse_string(line, len);
						desc.examiner = line;
						break;
					case 8: // EO1 description
						parse_string(line, len);
						desc.eo1_desc = line;
						break;
					case 9: // Notes
						parse_string(line, len);
						desc.notes = line;
						break;
					case 10: // OS version
						parse_string(line, len);
						desc.os_ver = line;
						break;
					case 11: // Partitions
					{
						// Check to see if there is a number
						if (line.length() < 11 ||
							!std::isdigit(line[10], std::locale())) continue;

						// Trim any carriage return
						if (line.back() == '\r') line.pop_back();

						// The new partition
						disk_part_info_t new_part;

						// Save the title
						new_part.name = line;

						// Get the sectors
						uint64_t val;
						if (!get_named_int(
							ss, L"Sectors ", new_part.first_sector, val))
							continue;

						// Save the end sector
						ends.push_back(val);

						// Get the partition table sector
						if (!get_named_int(
							ss, L"Partition table: Sector ",
							new_part.table_sector))
							continue;

						// Parse and save the type
						new_part.type = get_next_line(ss);

						// Save the new partition
						desc.partitions.push_back(new_part);
						break;
					}
					case 12: // Partitioning style
						parse_string(line, len);
						desc.partition_style = line;
						break;
					case 13: // Sector count
						desc.sectors = parse_int(line, len);
						break;
					case 14: // Capacity
						desc.capacity = parse_int(line, len);
						break;
					case 15: // Unpartitionable
						desc.unpartitionable = parse_int(line, len);
						break;
					case 16: // Unused areas
					{
						// Create a new partition with null values
						disk_part_info_t new_part;
						new_part.name = L"Unused";
						new_part.table_sector = 0;
						new_part.type = L"n/a";

						// Add all sectors ranges in the list
						uint64_t val;
						while (get_named_int(
							ss, L"Sectors ", new_part.first_sector, val))
						{
							desc.partitions.push_back(new_part);
							ends.push_back(val);
						}
						break;
					}
					}
				}
			}

			if (!desc.partitions.size() || !ends.size()) return desc;

			std::sort(desc.partitions.begin(), desc.partitions.end(), [](disk_part_info_t &a, disk_part_info_t &b) { return a.first_sector < b.first_sector; });
			std::sort(ends.begin(), ends.end());
			desc.partitions.push_back({ ends.back(), 0, 0, L"", L"" });
			for (auto it = desc.partitions.begin(); it < desc.partitions.end() - 1; it++)
			{
				it->size = ((it + 1)->first_sector - it->first_sector)*desc.sector_size;
			}
			return desc;
		}

		part_desc_t parse_part_desc(std::wstring text)
		{
			// List of strings to parse for
			const std::vector<std::wstring>part_desc_str = {
				L"Bytes per cluster: ",
				L"Bytes per sector: ",
				L"Computer name: ",
				L"Domain: ",
				L"File system: ",
				L"Free clusters: ",
				L"Installation date: ",
				L"NTFS version: ",
				L"Name: ",
				L"Number of accounts: ",
				L"Owner: ",
				L"Sector count: ",
				L"Serial No.: ",
				L"Time zone: ",
				L"Total capacity: ",
				L"Total clusters: ",
				L"Version: ",
				L"Volume GUID: ",
				L"Volume flags: ",
				L"Windows Installation"
			};

			// The descrition to return
			part_desc_t desc;

			// The current line
			std::wstring line;

			// List of partition end sectors
			std::vector<uint64_t> ends;

			// Stream containing the input text
			std::wstringstream ss(text);

			// Continue until the end of the input text
			while (!ss.eof())
			{
				// Length of the target string
				size_t len;

				// Get the next line
				line = get_next_line(ss);

				// Search for the line in the list of strings - the upper
				// bound will be the first item that is larger than the
				// match string so if there is a matching string it will
				// be the one immediately before
				auto it = std::upper_bound(part_desc_str.begin(),
					part_desc_str.end(), line);

				// The first string means none of the strings were found
				if (it == part_desc_str.begin()) continue;

				// Get the item before the current item and look
				// for an exact match
				it--;
				if (!line.compare(0, len = it->length(), *it))
				{
					switch (it - part_desc_str.begin())
					{
					case 0: // Bytes per cluster
						desc.cluster_size = parse_int(line, len);
						break;
					case 1: // Bytes per sector
						desc.sector_size = parse_int(line, len);
						break;
					case 2: // Computer name
						parse_string(line, len);
						desc.computer = line;
						break;
					case 3: // Domain
						parse_string(line, len);
						desc.domain = line;
						break;
					case 4:  // File system
						parse_string(line, len);
						desc.fs = line;
						break;
					case 5: // Free clusters
						desc.clusters_free = parse_int(line, len);
						break;
					case 6: // Installation date
						parse_string(line, len);
						desc.install_date = line;
						break;
					case 7: // NTFS version
						parse_string(line, len);
						desc.ntfs_ver = line;
						break;
					case 8: // Volume label
						parse_string(line, len);
						desc.label = line;
						break;
					case 9: // Number of accounts
						desc.accounts = parse_int(line, len);
						break;
					case 10: // Owner
						parse_string(line, len);
						desc.owner = line;
						break;
					case 11: // Sector count
						desc.sectors = parse_int(line, len);
						break;
					case 12: // Serial number
					{
						parse_string(line, len);
						auto pos = line.find_first_of(' ');
						if (pos < line.length())
						{
							desc.serials.push_back(
								make_pair(line.substr(0, pos),
									line.substr(pos + 1)));
						}
						break;
					}
					case 13: // Time zone
						parse_string(line, len);
						desc.timezone = line;
						break;
					case 14: // Total capacity
						desc.capacity = parse_int(line, len);
						break;
					case 15: // Total clusters
						desc.clusters = parse_int(line, len);
						break;
					case 16: // OS Version
						parse_string(line, len);
						desc.os_ver = line;
						break;
					case 17: // Volume GUID
						parse_string(line, len);
						desc.vol_guid = line;
						break;
					case 18: // Volume flags
						desc.flags = parse_int(line, len);
						break;
					case 19: // Windows installation
						desc.windows = get_next_line(ss);
						break;
					default:
						continue;
					}
				}
			}
			return desc;
		}

		///@}

		private:

			////////////////////////////
			// Function Pointer Types //
			////////////////////////////

			using get_volume_name_t =
				void(__stdcall *)(void* volume, wchar_t* buffer, uint32_t type);
			using output_message_t = void(__stdcall *)(
				const wchar_t* message,
				msg_flag_t flags);
			using get_case_prop_t = int64_t(__stdcall *)(void* reserved,
				caseprop_t prop_type,
				void* buffer,
				int32_t buf_size);
			using get_ev_obj_prop_t = int64_t(__stdcall *) (
				void* evidence,
				objprop_t prop_type,
				void* buffer);
			using get_volume_information_t = void(__stdcall *)(
				void* volume,
				int32_t* filesystem,
				uint32_t* bytes_per_sector,
				uint32_t* sectors_per_cluster,
				int64_t* cluster_count,
				int64_t* first_cluster_sector);
			using get_item_name_t = const wchar_t* (__stdcall *)(int32_t item);
			using get_item_size_t = int64_t(__stdcall *)(int32_t item);
			using get_item_information_t = int64_t(__stdcall *)(
				int32_t item,
				item_info_t info_type,
				bool* success);
			using get_comment_t = wchar_t* (__stdcall *)(int32_t item);
			using get_hash_value_t =
				bool(__stdcall *)(int32_t item, void* buffer);
			using get_item_type_t = type_status_t(__stdcall *)(
				int32_t item,
				wchar_t*type,
				int32_t type_len);
			using get_extracted_metadata_t =
				wchar_t*(__stdcall *)(int32_t item);
			using get_metadata_ex_t =
				void*(__stdcall *)(void* item, uint32_t* flags);
			using get_report_table_info_t = void*(__stdcall*)(
				void* reserved,
				int32_t report_table,
				report_table_flag_t* optional);
			using get_vs_prop_t = int64_t(__stdcall *)(
				vsprop_t prop_type,
				special_item_t* buffer);
			using read_t = uint32_t(__stdcall *)(
				void* handle,
				int64_t offset,
				uint8_t* buffer,
				uint32_t bytes_to_read);
			using get_size_t =
				int64_t(__stdcall *)(void* handle, uint8_t* optional);
			using get_block_t = bool(__stdcall *)(
				void* volume,
				int64_t* start_off,
				int64_t* end_off);
			using set_block_t = bool(__stdcall *)(
				void* volume,
				int64_t start_off,
				int64_t end_off);
			using get_sector_contents_t = bool(__stdcall *)(
				void* volume,
				int64_t sector,
				wchar_t* desc,
				int32_t* item_id);
			using open_item_t = void*(__stdcall *)(
				void* volume,
				int32_t item_id,
				open_item_flag_t flags);
			using close_t = void(__stdcall *)(void* handle);
			using sector_io_t = uint32_t(__stdcall *)(
				int32_t drive,
				int64_t sector,
				uint32_t count,
				void* buffer,
				sector_flag_t* flags);
			using get_first_ev_obj_t = void*(__stdcall *)(void* reserved);
			using get_next_ev_obj_t =
				void*(__stdcall *)(void* prev_evidence, void* reserved);
			using create_ev_obj_t = void*(__stdcall *)(
				evobj_type_t type,
				int32_t disk_id,
				wchar_t* path,
				void* reserved);
			using open_ev_obj_t =
				void*(__stdcall *)(void* evidence, open_evobj_flag_t flag);
			using close_ev_obj_t = void(__stdcall *)(void* evidence);
			using get_ev_obj_t = void*(__stdcall *) (uint32_t id);
			using get_ev_obj_report_table_assocs_t =
				report_assoc_item_t*(__stdcall*)(
					void* evidence, int32_t flags, int32_t* value);
			using select_volume_snapshot_t = void(__stdcall *)(void* volume);
			using get_item_count_t = uint32_t(__stdcall *)(void* volume);
			using get_file_count_t = uint32_t(__stdcall *)(int32_t directory);
			using create_item_t =
				long int(__stdcall *)(wchar_t* name, create_flag_t flags);
			using create_file_t = long int(__stdcall *)(
				wchar_t* name,
				create_flag_t flags,
				int32_t parent,
				void* source);
			using set_item_size_t =
				void(__stdcall *)(int32_t item, int64_t size);
			using get_item_ofs_t = void(__stdcall *)(
				int32_t item,
				int64_t* offset,
				int64_t* sector);
			using set_item_ofs_t = void(__stdcall *)(
				int32_t item,
				int64_t offset,
				int64_t sector);
			using set_item_information_t = bool(__stdcall *)(
				int32_t item,
				item_info_t info_type,
				int64_t info_value);
			using set_item_type_t = void(__stdcall *)(
				int32_t item,
				wchar_t*type,
				int32_t status);
			using get_item_parent_t = int32_t(__stdcall *)(int32_t item);
			using set_item_parent_t =
				void(__stdcall *)(int32_t item, int32_t parent);
			using get_report_table_assocs_t = int32_t(__stdcall *)(
				int32_t item,
				wchar_t* buffer,
				int32_t buffer_len);
			using add_to_report_table_t = int32_t(__stdcall *)(
				int32_t item,
				wchar_t* report_table,
				report_table_flag_t flags);
			using add_comment_t = bool(__stdcall *)(
				int32_t item,
				wchar_t* comment,
				replace_flag_t flag);
			using add_extracted_metadata_t = bool(__stdcall *)(
				int32_t item,
				wchar_t* metadata,
				replace_flag_t flag);
			using get_metadata_t =
				wchar_t*(__stdcall *)(int32_t item, void* handle);
			using get_raster_image_t =
				void*(__stdcall *)(struct raster_image_info_t* image_info);
			using search_t = int32_t(__stdcall *)(
				search_info_t* search_info,
				code_pages_t* code_pages);
			using add_search_term_t = int32_t(__stdcall *)(
				wchar_t* search_term,
				search_term_flag_t flags);
			using get_search_term_t =
				wchar_t*(__stdcall *)(int32_t search_term, void* reserved);
			using add_event_t =
				int32_t(__stdcall *)(event_info_t* event_info);
			using get_event_t = int32_t(__stdcall *)(
				uint32_t event,
				event_info_t* event_info);
			using create_container_t = void*(__stdcall *)(
				wchar_t* filename,
				ctr_create_flag_t flags,
				void* reserved);
			using copy_to_container_t = int32_t(__stdcall *)(
				void* container,
				void* handle,
				ctr_copy_flag_t flags,
				ctr_mode_flag_t mode,
				int64_t start_offset,
				int64_t end_offset,
				void* reserved);
			using close_container_t =
				int32_t(__stdcall *)(void* container, void* reserved);
			using get_user_input_t = int64_t(__stdcall *)(
				wchar_t* message,
				wchar_t* buffer,
				uint32_t buffer_len,
				user_input_flag_t flags);
			using show_progress_t =
				void(__stdcall *)(wchar_t* caption, progress_flag_t flags);
			using set_progress_percentage_t =
				void(__stdcall *)(uint32_t percent);
			using set_progress_description_t =
				void(__stdcall *)(wchar_t* description);
			using should_stop_t = bool(__stdcall *)(void);
			using hide_progress_t = void(__stdcall *)(void);

			//////////////////
			// Private Data //
			//////////////////

			output_message_t output_message_;
			get_case_prop_t get_case_prop_;
			get_ev_obj_prop_t get_ev_obj_prop_;
			get_volume_name_t get_volume_name_;
			get_volume_information_t get_volume_information_;
			get_item_size_t get_item_size_;
			get_item_information_t get_item_information_;
			get_comment_t get_comment_;
			get_hash_value_t get_hash_value_;
			get_item_type_t get_item_type_;
			get_extracted_metadata_t get_extracted_metadata_;
			get_metadata_ex_t get_metadata_ex_;
			get_report_table_info_t get_report_table_info_;
			get_vs_prop_t get_vs_prop_;
			read_t read_;
			get_item_name_t get_item_name_;
			get_size_t get_size_;
			get_block_t get_block_;
			set_block_t set_block_;
			get_sector_contents_t get_sector_contents_;
			open_item_t open_item_;
			close_t close_;
			sector_io_t sector_io_;
			get_first_ev_obj_t get_first_ev_obj_;
			get_next_ev_obj_t get_next_ev_obj_;
			create_ev_obj_t create_ev_obj_;
			open_ev_obj_t open_ev_obj_;
			close_ev_obj_t close_ev_obj_;
			get_ev_obj_t get_ev_obj_;
			get_ev_obj_report_table_assocs_t get_ev_obj_report_table_assocs_;
			select_volume_snapshot_t select_volume_snapshot_;
			get_item_count_t get_item_count_;
			get_file_count_t get_file_count_;
			create_item_t create_item_;
			create_file_t create_file_;
			set_item_size_t set_item_size_;
			get_item_ofs_t get_item_ofs_;
			set_item_ofs_t set_item_ofs_;
			set_item_information_t set_item_information_;
			set_item_type_t set_item_type_;
			get_item_parent_t get_item_parent_;
			set_item_parent_t set_item_parent_;
			get_report_table_assocs_t get_report_table_assocs_;
			add_to_report_table_t add_to_report_table_;
			add_comment_t add_comment_;
			add_extracted_metadata_t add_extracted_metadata_;
			get_metadata_t get_metadata_;
			get_raster_image_t get_raster_image_;
			search_t search_;
			add_search_term_t add_search_term_;
			get_search_term_t get_search_term_;
			add_event_t add_event_;
			get_event_t get_event_;
			create_container_t create_container_;
			copy_to_container_t copy_to_container_;
			close_container_t close_container_;
			get_user_input_t get_user_input_;
			show_progress_t show_progress_;
			set_progress_percentage_t set_progress_percentage_;
			set_progress_description_t set_progress_description_;
			should_stop_t should_stop_;
			hide_progress_t hide_progress_;


			/////////////////////
			// Private Methods //
			/////////////////////

			std::wstring get_hash_string(
				uint8_t* buffer, hash_type_t type)
			{
				static const wchar_t lookup[] = L"0123456789ABCDEF";
				std::wstring str = to_wstring(type);
				str += L":";
				for (auto end = buffer + hash_sizes[(int)type]; buffer < end; buffer++)
				{
					str += lookup[((*buffer & 0xF0) >> 4)];
					str += lookup[*buffer & 0x0F];
				}
				return str;
			}

			inline std::wstring get_next_line(std::wstringstream& ss)
			{
				std::wstring line;
				do {
					getline(ss, line);
					if (line.back() == '\r') line.pop_back();
				} while (!line.length() && !ss.eof());
				return line;
			}

			inline bool get_named_int(std::wstringstream& ss, std::wstring name, uint64_t&val)
			{
				auto line = get_next_line(ss);
				auto len = name.length();
				if (line.compare(0, len, name)) return false;
				line = line.substr(len, std::remove(line.begin(), line.end(), ',') - line.begin() - len);
				std::wstringstream linestream(line);
				linestream >> val;
				return true;
			}

			inline bool get_named_int(std::wstringstream& ss, std::wstring name, uint64_t&a, uint64_t& b)
			{
				auto line = get_next_line(ss);
				auto len = name.length();
				if (line.compare(0, len, name)) return false;
				line = line.substr(len, std::remove(line.begin(), line.end(), ',') - line.begin() - len);
				std::wstringstream linestream(line);
				linestream >> a;
				linestream.seekg(3, std::ios::cur);
				linestream >> b;
				return true;
			}

			inline void parse_string(std::wstring& line, size_t len)
			{
				if (line.back() == '\r') line.pop_back();
				line.erase(0, len);
			}

			inline uint64_t parse_int(std::wstring& line, size_t len)
			{
				if (line.back() == '\r') line.pop_back();
				line = line.substr(len, std::remove(line.begin(), line.end(), ',') - line.begin() - len);
				std::wstringstream linestream(line);
				uint64_t val;
				linestream >> val;
				return val;
			}
	};

	/*!
	@brief Class derived from [**std::streambuf**]
	(http://www.cplusplus.com/reference/streambuf/) for stream access to
	X-Ways items

	By default It reads data in chunks of 1024 bytes in order to improve
	performance by consolidating small reads, but happens transparently
	and is completely configurable.  It also uses an 8 byte put back area,
	again configurable.

	If you will only access the X-Ways item using unformatted stream
	operations then you may use this class instead of xways::item_stream,
	or you can just use xways::item_stream.  If you require formatted
	access then you should use xways::item_stream or some other formatted
	stream class such as [**std::istream**]
	(http://www.cplusplus.com/reference/istream/istream/) in conjunction
	with this class (the method to do this is beyond the scope of this
	documentation but can be found easily enough).

	To use this class you must first create an xways::api object and then
	also create a xways::item_streambuf object.  You may then access the
	item in the same was as you would any STL input streambuf, e.g.
	using [**std::streambuf::sgetc()**]
	(http://www.cplusplus.com/reference/streambuf/streambuf/sgetc/),
	[**std::streambuf::sgetn()**]
	(http://www.cplusplus.com/reference/streambuf/streambuf/sgetn/),
	[**std::streambuf::pubseekpos()**]
	(http://www.cplusplus.com/reference/streambuf/streambuf/pubseekpos/)
	and others.  See the documentation for [**std::streambuf**]
	(http://www.cplusplus.com/reference/streambuf/) for more
	details.

	Additionally you may also call the public methods of this class:
	size(), position(), etc. See their documentation for details.

	The following fragment shows an example of using this class:
	\includelineno "api_streambuf.cpp"

	*/
	template<typename T = char>
	class item_streambuf : public std::basic_streambuf<T> {
	public:

		/*!
		@brief Construct an item_streambuf that is not associated with an
		item

		Creates a put back area that the larger of one element and the
		number of elements requested via the @a put_back parameter.
		Creates a buffer that is the larger of the put back area size and
		the size requested via the @a buffer_size parameter, plus the put
		back area itself.

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_streambuf(
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			put_back_((std::max)(put_back * sizeof(T), sizeof(T))),
			buffer_((std::max)(buffer_size * sizeof(T),
				put_back_) + put_back_)
		{
			// Initialize all pointers to end of buffer to force
			// underflow() call on first read
			char *end = &buffer_.front() + buffer_.size();
			setg(end, end, end);
			eof_ = false;
		}

		/*!
		@brief Construct an item_streambuf that is associated with an item

		Creates a put back area that the larger of one element and the
		number of elements requested via the @a put_back parameter.
		Creates a buffer that is the larger of the put back area size and
		the size requested via the @a buffer_size parameter, plus the put
		back area itself.

		Additionally set the handle of the file, which will be used for
		subsequent operations.  The handle is assumed to be valid.

		@param[in] handle  the handle of the file to be associated with
		this item_streambuf object

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_streambuf(
			void* handle,
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			item_streambuf(buffer_size, put_back)
		{
			handle_ = handle;
		}

		/*!
		@brief Associate the item_streambuf with a item

		Sets the handle of the file, which will be used for
		subsequent operations.  The handle is assumed to be valid.

		@param[in] handle  the handle of the file to be associated with
		the item_streambuf object
		*/
		void open(void* handle) { handle_ = handle; }

		/*!
		@brief Get the size of the item associated with this
		item_streambuf

		Gets the size of the item via an X-Ways API call and then save it
		in the object to remove the need to call X-Ways more than once.

		@return  the size of the item in bytes
		*/
		uint64_t size() {
			if (!size_valid_) {
				size_ = xways::api::instance().get_size(handle_, 0);
				size_valid_ = true;
			}
			return size_;
		}

		/*!
		@brief Get the current position in the item

		Gets the current position in the item.  This will be the next
		byte to be read, which may or may not be already in the buffer.

		@return  the offset of the next byte to be read in bytes from
		the start of the item
		*/
		int64_t tellg() { return position_ - (egptr() - gptr()); }

		/*!
		@brief Get the item handle

		Gets the handle provided when the object was created or provided
		in the last call to @a open().

		@return  the handle of the item
		*/
		void* handle() { return handle_; }

		/*!
		@brief Get the eof state

		@return  the eof state
		*/
		bool eof() const { return eof_; }

	private:
		int_type underflow()
		{
			// If not at the end then just return the current character
			if (gptr()<egptr()) return traits_type::to_int_type(*gptr());

			T *base = &buffer_.front();
			T *start = base;

			// If eback() is set to the start of the buffer then this is
			// not the first fill and it is necessary to move the put_back
			// area
			if (eback() == base)
			{
				std::memmove(base, egptr() - put_back_, put_back_);
				start += put_back_;
			}

			// Adjust the read request if less than a full buffer remains
			auto req = static_cast<uint32_t>((std::min)(size() - position_,
				(uint64_t)buffer_.size() - (start - base)));

			// Read the data
			size_t n = xways::api::instance().read(handle_, position_,
				reinterpret_cast<uint8_t*>(start), req);

			// If no data can be read then assume an end of file
			if (n == 0)
			{
				eof_ = true;
				return traits_type::eof();
			}

			// Advance the current file position by the number of 
			// characters read
			position_ += n;

			// Adjust the pointers
			setg(base, start, start + n);

			// Return the current character
			return traits_type::to_int_type(*gptr());
		}

		pos_type seekoff(off_type off,
			std::ios_base::seekdir dir,
			std::ios_base::openmode which = std::ios_base::in)
		{
			// Determine the offset to the first byte of the buffer
			uint64_t begin = position_ - (egptr() - eback());

			// Determine the offset to the current position in the buffer
			uint64_t current = begin + (gptr() - eback());

			// Convert off to bytes
			off *= sizeof(T);

			// The new offset (in bytes)
			uint64_t newpos;

			// Set the reference
			switch (dir) {
			case std::ios_base::beg:
				newpos = 0;
				break;

			case std::ios_base::cur:
				newpos = current;
				break;

			case std::ios_base::end:
				newpos = size();
				break;

			default:
				return -1;
			}

			// Apply the offset
			newpos += off;

			// If the new position is not in the buffer then set the new
			// position and set the gptr() to egptr() to force an
			// underflow() call otherwise just gbump() to the new position
			if (newpos < begin || newpos >= position_) {
				position_ = newpos;
				setg(eback(), egptr(), egptr());
			}
			else {
				gbump(static_cast<int>(newpos - current));
			}

			// Reset the eof
			eof_ = false;

			return newpos;
		}

		pos_type seekpos(std::streampos pos,
			std::ios_base::openmode which = std::ios_base::in)
		{
			return seekoff(pos, std::ios_base::beg, which);
		}

		std::streamsize showmanyc()
		{
			return (size() - position_ - (egptr() - gptr())) / sizeof(T);
		}

		std::streamsize xsgetn(char* s, std::streamsize n)
		{
			// Check if the request can be satisified from the current
			// buffer
			if (static_cast<std::streamsize >
				((egptr() - gptr()) / sizeof(T)) > n) {
				// It can be so use memcpy() and gbump() the position
				memcpy(s, gptr(), static_cast<size_t>(n));
				gbump(static_cast<int>(n));
				return n;
			}
			else {
				// It can't be.  If the amount to be read is less than
				// half the size of the buffer then read into the buffer
				// and copy from that otherwise just read directly to the
				// supplied pointer - this is to avoid copying the buffer
				// if it will be almost full anyway

				// Set position_ to the current position
				position_ -= (egptr() - gptr());

				if (2 * n * sizeof(T) < buffer_.size()) {
					// Set gptr() to egptr() to force underflow() to
					// re-fill the buffer
					setg(eback(), egptr(), egptr());
					underflow();

					// Make sure there are enough characters in the buffer
					std::streamsize available =
						static_cast<std::streamsize>(
							(egptr() - gptr()) / sizeof(T));
					if (n > available) n = available;

					// memcpy() and gbump() the position
					memcpy(s, gptr(), static_cast<size_t>(n));
					gbump(static_cast<int>(n));
					return n;
				}
				else {
					// Read directly to the pointer
					uint64_t num_read = xways::api::instance().read(
						handle_,
						position_,
						reinterpret_cast<uint8_t*>(s),
						static_cast<uint32_t>(n * sizeof(T)));

					// Adjust the position
					position_ += num_read;

					// Set gptr() to egptr() to force underflow() call on
					// next read
					setg(eback(), egptr(), egptr());
					return num_read / sizeof(T);
				}
			}
		}

	private:
		// The X-Ways item handle
		void* handle_;

		// The X-Ways item id
		int32_t item_;

		// Current offset in bytes to the next byte to be read
		uint64_t position_ = 0;

		// Size of the item in bytes
		uint64_t size_;

		// Size is valid
		bool size_valid_ = false;

		// Size of the put_back area in bytes
		std::size_t put_back_;

		// Buffer
		std::vector<char> buffer_;

		// EOF bit
		bool eof_;
	};

	/*!
	@brief Class derived from [**std::basic_istream**]
	(http://www.cplusplus.com/reference/istream/istream/) for
	formatted stream access to X-Ways items

	This class wraps a xways::item_streambuf object (that can also be used
	directly if you do not require formatted input) to manage access to
	the item's data.  By default the xways::item_streambuf object will
	read data in chunks of 1024 bytes in order to improve performance by
	consolidating small reads, but happens transparently and is
	completely configurable.  It also uses an 8 byte put back area, again
	configurable.

	To use this class you must first create an xways::api object and then
	also create a xways::item_stream object.  You may then access the item
	in the same was as you would any STL input stream, e.g. using the
	stream operator [**std::istream::operator>>()**]
	(http://www.cplusplus.com/reference/istream/istream/operator%3E%3E/),
	[**std::istream::read()**]
	(http://www.cplusplus.com/reference/istream/istream/read/),
	[**std::istream::getline()**]
	(http://www.cplusplus.com/reference/istream/istream/getline/) and
	others.  See the documentation for [**std::istream**]
	(http://www.cplusplus.com/reference/istream/istream/) for more
	details.

	Additionally you may also call the public methods of this class:
	size(), position(), etc. See their documentation for details.

	The following fragment shows an example of using this class:
	\includelineno "api_stream.cpp"
	*/
	template <class T = char, class Traits = std::char_traits<T>>
	class item_stream : public std::basic_istream <T, Traits>
	{
	public:
		/*!
		@brief Construct an item_stream from an existing item_streambuf

		@param[in] sb  a pointer to an existing item_streambuf object
		*/
		explicit item_stream(item_streambuf<T>* sb) :
			basic_istream<T, Traits>(sb) {}

		/*!
		@brief Construct an item_stream from a handle

		Creates an item_streambuf object with a put back area that is
		the larger of one element and the number of elements requested
		via the @a put_back parameter.  Creates a buffer that is the
		larger of the put back area size and the size requested
		via the @a buffer_size parameter, plus the put back area itself.

		Additionally sets the handle of the file, which will be used for
		subsequent operations.  The handle is assumed to be valid.

		@param[in] handle  the handle of the file to be associated with
		this item_stream object

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_stream(
			void* handle,
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			streambuf_(handle, buffer_size, put_back),
			basic_istream<T, Traits>(&streambuf_),
			id_valid_(false) {}

		/*!
		@brief Construct an item_stream from an item id

		Creates an item_streambuf object with a put back area that is
		the larger of one element and the number of elements requested
		via the @a put_back parameter.  Creates a buffer that is the
		larger of the put back area size and the size requested
		via the @a buffer_size parameter, plus the put back area itself.

		Additionally queries X-Ways for the handle of the item, which
		will be used for subsequent operations.  The id is assumed to be
		valid.

		@param[in] id  the id of the item to be associated with
		this item_stream object

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_stream(
			int32_t id,
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			streambuf_(xways::api::instance().get_ev_obj(id),
				buffer_size, put_back),
			basic_istream<T, Traits>(&streambuf_),
			id_(id),
			id_valid_(true) {}

		/*!
		@brief Construct an item_stream from an item id and handle

		Creates an item_streambuf object with a put back area that is
		the larger of one element and the number of elements requested
		via the @a put_back parameter.  Creates a buffer that is the
		larger of the put back area size and the size requested
		via the @a buffer_size parameter, plus the put back area itself.

		Additionally sets the handle of the file, which will be used for
		subsequent operations.  The handle is assumed to be valid.

		@param[in] handle  the handle of the file to be associated with
		this item_stream object

		@param[in] id  the id of the file to be associated with	this
		item_stream object

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_stream(
			void* handle,
			int32_t id,
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			streambuf_(handle, buffer_size, put_back),
			basic_istream<T, Traits>(&streambuf_),
			id_(id),
			id_valid_(true) {}

		/*!
		@brief Construct an item_stream that is not associated with an
		item

		Creates an item_streambuf object with a put back area that is the
		larger of one element and the number of elements requested via the
		@a put_back parameter.  Creates a buffer that is the larger of the
		put back area size and the size requested via the @a buffer_size
		parameter, plus the put back area itself.

		@param[in] buffer_size  the number of elements in the buffer
		(default 1024 elements)

		@param[in] put_back  the number of elements in the put back area
		(default 8 elements)
		*/
		explicit item_stream(
			std::size_t buffer_size = 1024,
			std::size_t put_back = 8) :
			streambuf_(buffer_size, put_back),
			basic_istream<T, Traits>(&streambuf_) {}

		/*!
		@brief Associate the item_stream with an item

		Sets the handle of the file that will be used for
		subsequent operations in the backing item_streambuf object.
		The handle is assumed to be valid.  Also sets the item id in
		this object.

		@param[in] handle  the handle of the file to be associated with
		the item_stream object

		@param[in] id  the id of the file to be associated with	this
		item_stream object
		*/
		void open(void* handle, int32_t id) {
			reinterpret_cast<item_streambuf<T>*>(rdbuf())->open(handle);
			id_ = id;
			id_valid_ = true;
		}

		/*!
		@brief Associate the item_stream with an item

		Sets the handle of the file that will be used for
		subsequent operations in the backing item_streambuf object.
		The handle is assumed to be valid.

		@param[in] handle  the handle of the file to be associated with
		the item_stream object
		*/
		void open(void* handle) {
			reinterpret_cast<item_streambuf<T>*>(rdbuf())->open(handle);
		}

		/*!
		@brief Associate the item_stream with an item

		Sets the handle of the file that will be used for
		subsequent operations in the backing item_streambuf object.
		The handle is assumed to be valid.  Also sets the item id in
		this object.

		@param[in] id  the id of the file to be associated with	this
		item_stream object
		*/
		void open(int32_t id) {
			reinterpret_cast<item_streambuf<T>*>(
				rdbuf())->open(xways::api::instance().get_ev_obj(id));
			id_ = id;
			id_valid_ = true;
		}

		/*!
		@brief Get the size of the item associated with this item_stream

		Gets the size of the item by delegating the request to the backing
		item_streambuf object.

		@return  the size of the item in bytes
		*/
		uint64_t size()
		{
			return dynamic_cast<item_streambuf<T>*>(rdbuf())->size();
		}

		/*!
		@brief Get the id of the item

		Gets the id that was provided during creation of the object or on
		the last call to open() - if applicable.  Otherwise get it from
		the X-Ways API and save it to prevent the need to get it again.

		@return  the item id
		*/
		int32_t id()
		{
			if (!id_valid_) {
				id_ = static_cast<uint32_t>(
					xways::api::instance().get_ev_obj_prop(
						dynamic_cast<item_streambuf<T>*>(
							rdbuf())->handle(), xways::objprop_t::ID, NULL));
				id_valid_ = true;
			}
			return id_;
		}

		/*!
		@brief Get the current position in the item

		Gets the current position in the item by delegating the request
		to the underlying item_streambuf object.  This will be the next
		byte to be read, which may or may not be already in the buffer.

		@return  the offset of the next byte to be read in bytes from
		the start of the item
		*/
		int64_t tellg()
		{
			return dynamic_cast<item_streambuf<T>*>(rdbuf())->tellg();
		}

		/*!
		@brief Get the item name

		@return  the name
		*/
		const wchar_t* name()
		{
			return xways::api::instance().get_item_name(id());
		}

	private:
		// The item_streambuf object
		item_streambuf<T> streambuf_;

		// The X-Ways item id
		int32_t id_;

		// The item id is valid
		bool id_valid_;
	};

	/*!
	@brief Base exception class

	This is the base class from which the two main exception classes @ref
	nonfatal_error and @ref fatal_error are derived.  This class is not
	intended to be used directly.

	Constructors for this class take a message string (either **char\***
	or [**std::string**]
	(http://www.cplusplus.com/reference/string/string/)), the ID of the
	file being examined and optionally, the offset to the location causing
	the error.  Two methods are implemented, @ref what(), which returns a
	string suitable for display in the X-Ways message log window, and @ref
	where(), which returns the offset (if applicable).
	*/
	class error : public std::exception {
	protected:
		std::string message_;	//!< Error message to be returned by @ref
								//!  what()
		uint64_t offset_;		//!< Offset to be returned by @ref where()
		uint64_t id_;			//!< Item ID to be included in the message
								//!  returned by @ref what()
	public:
		/*!
		@brief Construct from a **const char\*** error message and an
		offset

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined

		@param[in] offset  the location in the file where the error
		occurred
		*/
		explicit error(const char* message, uint64_t id, uint64_t offset) :
			error(std::string(message), id, offset) {}

		/*!
		@brief Construct from a [**std::string**]
		(http://www.cplusplus.com/reference/string/string/) reference
		error message and an offset

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined

		@param[in] offset  the location in the file where the error
		occurred
		*/
		explicit error(
			const std::string & message,
			uint64_t id,
			uint64_t offset) :
			message_(
				XT_PREFIX_A": " +
				message +
				" in file with ID: " +
				std::to_string(id) +
				", at offset: " +
				std::to_string(offset)),
			id_(id),
			offset_(offset) {}

		/*!
		@brief Construct from a **const char\*** error message (no offset)

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined
		*/
		explicit error(const char* message, uint64_t id) :
			error(std::string(message), id) {}

		/*!
		@brief Construct from a [**std::string**]
		(http://www.cplusplus.com/reference/string/string/)	reference
		error message (no offset)

		@param[in] message  the error message

		@param[in] id  the ID of the file being examined
		*/
		explicit error(
			const std::string & message,
			uint64_t id) :
			message_(
				XT_PREFIX_A": " +
				message +
				" in file with ID: " +
				std::to_string(id)),
			id_(id) {}

		/*!
		@brief Construct from a [**std::wstring**]
		(http://www.cplusplus.com/reference/wstring/wstring/) reference
		error message (no id or offset)

		@param[in] message  the error message
		*/
		explicit error(
			const std::wstring & message)
		{
			std::wstring wmessage(XT_PREFIX": " + message);
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,
				wchar_t> convert;
			message_ = convert.to_bytes(wmessage);
		}

		/*!
		@brief Destructor (rethrows)
		*/
		virtual ~error() throw () {}

		/*!
		@brief Get the error message

		@return **const char\*** to the error message
		*/
		virtual const char* what() const throw ()
		{
			return message_.c_str();
		}

		/*!
		@brief Get the offset to the error location

		@return offset to the error location
		*/
		virtual uint64_t where() const throw () { return offset_; }
	};

	/*!
	@brief Derived exception class for fatal error handling

	This is the derived exception class intended to be thrown in the case
	of fatal errors.

	Constructors (inherited from the base class) take a message string
	(either **char\*** or [**std::string**]
	(http://www.cplusplus.com/reference/stringstream/stringstream/)),
	the ID of the file being examined and, optionally, the offset to the
	location causing the error.  Two methods are implemented, @ref what(),
	which returns a string suitable for display in the X-Ways message log
	window, and @ref where(), which returns the offset (if applicable).
	*/
	class fatal_error : public error { using error::error; };

	/*!
	@brief Derived exception class for non-fatal error handling

	This is the derived exception class intended to be thrown in the case
	of non-fatal errors.

	Constructors (inherited from the base class) take a message string
	(either **char\*** or [**std::string**]
	(http://www.cplusplus.com/reference/string/string/)), the ID of the
	file being examined and, optionally, the offset to the location
	causing the error.  Two methods are implemented, @ref what(), which
	returns a string suitable for display in the X-Ways message log
	window, and @ref where(), which returns the offset (if applicable).
	*/
	class nonfatal_error : public error { using error::error; };


	//////////////////////////////
	// x_tension Implementation //
	//////////////////////////////

	inline int32_t x_tension::init(
		xways::caller_info_t version,
		xways::init_flag_t flags,
		HWND main_window,
		xways::licence_info_t* licence)
	{
		return 1;
	}

	inline int32_t x_tension::done(void* reserved) { return 0; }

	inline int32_t x_tension::about(void* parent_window, void* reserved)
	{
		xways::api::instance().output_message(XT_PREFIX": About");
		MessageBoxW((HWND)parent_window, about_text, L"About " XT_PREFIX, 0);
		return 0;
	}

	inline xways::prepare_t x_tension::prepare(
		void* volume,
		void* evidence,
		xways::action_t operation,
		void* reserved)
	{
		return (xways::prepare_t)0;
	}

	inline int32_t x_tension::finalize(
		void* volume,
		void* evidence,
		uint32_t operation,
		void* reserved)
	{
		return 0;
	}

	inline int32_t x_tension::process_item(int32_t id, void* reserved)
	{
		return 0;
	}

	inline int32_t x_tension::process_item_ex(
		int32_t id, void* handle, void* reserved)
	{
		return 0;
	}

	inline int32_t x_tension::prepare_search(
		prepare_search_info_t* info, code_pages_t* code_pages)
	{
		return 0;
	}

	inline int32_t x_tension::process_search_hit(search_hit_t* info)
	{
		return 0;
	}

	inline void* x_tension::view(
		void* handle,
		int32_t id,
		void* volume,
		void* evidence,
		void* reserved,
		int64_t* size)
	{
		*size = -1;
		return 0;
	}

	inline bool x_tension::release_mem(void* buffer)
	{
		return true;
	}

	inline uint32_t x_tension::sector_io_init(drive_info_t* info)
	{
		return 0;
	}

	inline uint32_t x_tension::sector_io(void* private_data, int32_t drive,
		int64_t sector, uint32_t count, void* buffer, uint32_t flags)
	{
		return 0;
	}

	inline uint32_t x_tension::sector_io_done(void* private_data, void* reserved)
	{
		return 0;
	}

	inline x_tension::x_tension()
	{
		xways::api::instance().x_tension_ = this;
	}

	/////////////////
	// DLL Exports //
	/////////////////

	XT_API int32_t inline XT_Init(
		xways::caller_info_t version,
		xways::init_flag_t flags,
		HWND main_window,
		xways::licence_info_t* licence)
	{
		if (flags & xways::init_flag_t::QUICKCHECK) return 1;
		return xways::api::instance().x_tension_->init(version, flags, main_window, licence);
	}
#ifdef XT_DONE
	XT_API int32_t inline XT_Done(void* reserved)
	{
		return xways::api::instance().x_tension_->done(reserved);
	}
#endif
#ifdef XT_ABOUT
	XT_API int32_t inline XT_About(void* parent_window, void* reserved)
	{
		return xways::api::instance().x_tension_->about(parent_window, reserved);
	}
#endif
#ifdef XT_PREPARE
	XT_API xways::prepare_t inline XT_Prepare(
		void* volume,
		void* evidence,
		xways::action_t operation,
		void* reserved)
	{
		return xways::api::instance().x_tension_->prepare(
			volume, evidence, operation, reserved);
	}
#endif
#ifdef XT_FINALIZE
	XT_API int32_t inline XT_Finalize(
		void* volume,
		void* evidence,
		uint32_t operation,
		void* reserved)
	{
		return xways::api::instance().x_tension_->finalize(
			volume, evidence, operation, reserved);
	}
#endif
#ifdef XT_PROCESS_ITEM
	XT_API int32_t inline XT_ProcessItem(int32_t id, void* reserved)
	{
		return xways::api::instance().x_tension_->process_item(id, reserved);
	}
#endif
#ifdef XT_PROCESS_ITEM_EX
	XT_API int32_t inline XT_ProcessItemEx(
		int32_t id, void* handle, void* reserved)
	{
		return xways::api::instance().x_tension_->process_item_ex(
			id, handle, reserved);
	}
#endif
#ifdef XT_PREPARE_SEARCH
	XT_API int32_t inline XT_PrepareSearch(
		prepare_search_info_t* info, code_pages_t* code_pages)
	{
		return xways::api::instance().x_tension_->prepare_search(
			info, code_pages);
	}
#endif
#ifdef XT_PROCESS_SEARCH_HIT
	XT_API int32_t inline XT_ProcessSearchHit(search_hit_t* info)
	{
		return xways::api::instance().x_tension_->process_search_hit(info);
	}
#endif
#ifdef XT_VIEW
	XT_API inline void* XT_View(
		void* handle,
		int32_t id,
		void* volume,
		void* evidence,
		void* reserved,
		int64_t* size)
	{
		return xways::api::instance().x_tension_->view(
			handle, id, volume, evidence, reserved, size);
	}
	XT_API bool inline XT_ReleaseMem(void* buffer)
	{
		return xways::api::instance().x_tension_->release_mem(buffer);
	}
#endif
#ifdef XT_SECTOR_IO
	XT_API uint32_t inline XT_SectorIOInit(drive_info_t* info)
	{
		return xways::api::instance().x_tension_->sector_io_init(info);
	}
	XT_API uint32_t inline XT_SectorIO(void* private_data, int32_t drive,
		int64_t sector, uint32_t count, void* buffer, uint32_t flags)
	{
		return xways::api::instance().x_tension_->sector_io(
			private_data, drive, sector, count, buffer, flags);
	}
	XT_API uint32_t inline XT_SectorIODone(void* private_data, void* reserved)
	{
		return xways::api::instance().x_tension_->sector_io_done(
			private_data, reserved);
	}
#endif

	/*!
	@brief DLL main entry point

	This function is the main entry point to the DLL *for the operating
	system* and has nothing to do with X-Ways.  It will be called by the
	operating system when the DLL is loaded or unloaded.

	@param[in] module  a handle to the DLL module

	@param[in] reason  the reason code that indicates why the DLL entry-point
	function is being called

	@param[in] reserved  if @a reason is @a DLL_PROCESS_ATTACH, @a reserved is
	NULL for dynamic loads and non-NULL for static loads; if @a reason is @a
	DLL_PROCESS_DETACH, @a reserved is NULL if FreeLibrary() has been called
	or the DLL load failed and non-NULL if the process is terminating

	@return currently always TRUE to indicate success (only applicable when
	@a reason is @a DLL_PROCESS_ATTACH.
	*/
	bool inline APIENTRY DllMain(HMODULE module, uint32_t reason, void* reserved)
	{
		switch (reason)
		{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
		}
		return TRUE;
	}
}

