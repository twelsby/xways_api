// Copyright (C) 2016 Trevor Welsby
// Version 0.0
// Revision $DateTime: 2016/02/21 17:05:00 $
//! \file Test.cpp Basic test program

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include <iostream>
#include <assert.h>
#include <fstream>

#define XT_API extern "C" __declspec(dllexport)

using namespace std;

#pragma pack(push,2)

struct SearchInfo {
	LONG iSize;
	HANDLE hVolume;
	LPWSTR lpSearchTerms;
	DWORD nFlags;
	DWORD nSearchWindow;
};

struct CodePages {
	LONG iSize;
	WORD nCodePage1;
	WORD nCodePage2;
	WORD nCodePage3;
	WORD nCodePage4;
	WORD nCodePage5;
};

struct EventInfo {
	LONG iSize;
	HANDLE hEvidence;
	DWORD nEvtType;
	DWORD nFlags;
	FILETIME TimeStamp;
	LONG nItemID;
	INT64 nOfs;
	LPSTR lpDescr;
};

struct CallerInfo {
	BYTE lang, ServiceRelease;
	WORD version;
};

struct PrepareSearchInfo {
	LONG iSize;
	LPWSTR lpSearchTerms;
	DWORD nBufLen;
	DWORD nFlags;
};

struct LicenseInfo {
	DWORD nSize;
	DWORD nLicFlags;
	DWORD nUsers;
	FILETIME nExpDate;
	BYTE nLicID[16];
};

#pragma pack(pop)

#define XT_INIT_XWF 0x00000001 // X-Ways Forensics
#define XT_INIT_WHX 0x00000002 // WinHex
#define XT_INIT_XWI 0x00000004 // X-Ways Investigator
#define XT_INIT_BETA 0x00000008 // beta version
#define XT_INIT_QUICKCHECK 0x00000020 // called just to check whether the API accepts the calling application (used by v16.5 and later)
#define XT_INIT_ABOUTONLY 0x00000040 // called just to prepare for XT_About (used by v16.5 and later)

#define XT_ACTION_RUN 0 // simply run directly from the main menu, not for any particular volume, since v16.6
#define XT_ACTION_RVS 1 // volume snapshot refinement starting
#define XT_ACTION_LSS 2 // logical simultaneous search starting
#define XT_ACTION_PSS 3 // physical simultaneous search starting
#define XT_ACTION_DBC 4 // directory browser context menu command invoked
#define XT_ACTION_SHC 5 // search hit context menu command invoked

#define XT_PREPARE_CALLPI 0x01 // call for each item in the volume snapshot
#define XT_PREPARE_CALLPILATE 0x02 // call for each item in the volume snapshot after other refinement operations
#define XT_PREPARE_EXPECTMOREITEMS 0x04 // signal that more items may be added to the volume snapshot
#define XT_PREPARE_DONTOMIT 0x08 // don't omit files that the user wants to omit
#define XT_PREPARE_TARGETDIRS 0x10 // target directories not just files

#define XWF_ITEM_INFO_ORIG_ID 1
#define XWF_ITEM_INFO_ATTR 2
#define XWF_ITEM_INFO_FLAGS 3
#define XWF_ITEM_INFO_DELETION 4
#define XWF_ITEM_INFO_CLASSIFICATION 5 // e.g. extracted e-mail message, alternate data stream, etc.
#define XWF_ITEM_INFO_LINKCOUNT 6 // hard-link count
#define XWF_ITEM_INFO_FILECOUNT 11 // how many child objects exist recursively that are files
#define XWF_ITEM_INFO_CREATIONTIME 32
#define XWF_ITEM_INFO_MODIFICATIONTIME 33
#define XWF_ITEM_INFO_LASTACCESSTIME 34
#define XWF_ITEM_INFO_ENTRYMODIFICATIONTIME 35
#define XWF_ITEM_INFO_DELETIONTIME 36
#define XWF_ITEM_INFO_INTERNALCREATIONTIME 37
#define XWF_ITEM_INFO_FLAGS_SET 64 // indicates only flags that should be set, others remain unchanged
#define XWF_ITEM_INFO_FLAGS_REMOVE 65 // indicates flags that should be removed, others remain unchanged

#define XWF_SEARCH_LOGICAL 0x00000001 // logical search instead of physical search (only logical search currently available)
#define XWF_SEARCH_TAGGEDOBJ 0x00000004 // tagged objects in volume snapshot only
#define XWF_SEARCH_MATCHCASE 0x00000010 // match case
#define XWF_SEARCH_WHOLEWORDS 0x00000020 // whole words only
#define XWF_SEARCH_GREP 0x00000040 // GREP syntax
#define XWF_SEARCH_OVERLAPPED 0x00000080 // allow overlapping hits
#define XWF_SEARCH_COVERSLACK 0x00000100 // cover slack space
#define XWF_SEARCH_COVERSLACKEX 0x00000200 // cover slack/free space transition
#define XWF_SEARCH_DECODETEXT 0x00000400 // decode text in standard file types
#define XWF_SEARCH_DECODETEXTEX 0x00000800 // decode text in specified file types // not yet supported 
#define XWF_SEARCH_1HITPERFILE 0x00001000 // 1 hit per file needed only
#define XWF_SEARCH_OMITIRRELEVANT 0x00010000 // omit files classified as irrelevant
#define XWF_SEARCH_OMITHIDDEN 0x00020000 // omit hidden files
#define XWF_SEARCH_OMITFILTERED 0x00040000 // omit files that are filtered out
#define XWF_SEARCH_DATAREDUCTION 0x00080000 // recommendable data reduction
#define XWF_SEARCH_OMITDIRS 0x00100000 // omit directories
#define XWF_SEARCH_CALLPSH 0x01000000 // see below
#define XWF_SEARCH_DISPLAYHITS 0x04000000 // display search hit list when the search completes

#define XWF_CTR_OPEN 0x00000001 // opens an existing container, all other flags ignored
#define XWF_CTR_XWFS2 0x00000002 // use new XWFS2 file system
#define XWF_CTR_SECURE 0x00000004 // mark this container as to be filled indirectly/secure
#define XWF_CTR_TOPLEVEL 0x00000008 // include evidence object names as top directory level
#define XWF_CTR_INCLDIRDATA 0x00000010 // include directory data
#define XWF_CTR_FILEPARENTS 0x00000020 // allow files as parents of files
#define XWF_CTR_USERREPORTTABLES 0x00000100 // export associations with user-created report table
#define XWF_CTR_SYSTEMREPORTTABLES 0x00000200 // export associations with system-created report tables (currently requires 0x100)
#define XWF_CTR_ALLCOMMENTS 0x00000800 // pass on comments
#define XWF_CTR_OPTIMIZE1 0x00001000 // optimize for > 1,000 items
#define XWF_CTR_OPTIMIZE2 0x00002000 // optimize for > 50,000 items
#define XWF_CTR_OPTIMIZE3 0x00004000 // optimize for > 250,000 items
#define XWF_CTR_OPTIMIZE4 0x00008000 // optimize for > 1 million items

#define XWF_CASEPROP_TITLE 0x01
#define XWF_CASEPROP_EXAMINER 0x03
#define XWF_CASEPROP_FILE 0x05
#define XWF_CASEPROP_DIR 0x06

typedef LONG(__stdcall *fptr_XT_Init)(DWORD nVersion, DWORD nFlags, HANDLE hMainWnd, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_Done)(void* lpReserved);
typedef LONG(__stdcall *fptr_XT_About)(HANDLE hParentWnd, void* lpReserved);
typedef PVOID(__stdcall *fptr_XT_View)(HANDLE hItem, LONG nItemID, HANDLE hVolume, HANDLE hEvidence, PVOID lpReserved, PINT64 nResSize);
typedef BOOL(__stdcall *fptr_XT_ReleaseMem)(PVOID lpBuffer);
typedef LONG(__stdcall *fptr_XT_Prepare)(HANDLE hVolume, HANDLE hEvidence, DWORD nOpType, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_Finalize)(HANDLE hVolume, HANDLE hEvidence, DWORD nOpType, void* lpReserved);
typedef LONG(__stdcall *fptr_XT_ProcessItemEx)(LONG nItemID, HANDLE hItem, void* lpReserved);

fptr_XT_Init XT_Init;
fptr_XT_Done XT_Done;
fptr_XT_About XT_About;
fptr_XT_View XT_View;
fptr_XT_ReleaseMem XT_ReleaseMem;
fptr_XT_Prepare XT_Prepare;
fptr_XT_Finalize XT_Finalize;
fptr_XT_ProcessItemEx XT_ProcessItemEx;

wstring filename;
wstring path;
wstring itemtype;
HANDLE handle = (HANDLE)0xAA;
LONG item = 0xBB;
HANDLE volume = (HANDLE)0xDD;
HANDLE evidence = (HANDLE)0xEE;

LONG RetrieveFunctionPointers()
{
	HMODULE Hdl = LoadLibraryW(wstring(path + L"\\XT_VIEW_64.DLL").c_str());

	XT_Init = (fptr_XT_Init)GetProcAddress(Hdl, "XT_Init");
	XT_Done = (fptr_XT_Done)GetProcAddress(Hdl, "XT_Done");
	XT_About = (fptr_XT_About)GetProcAddress(Hdl, "XT_About");
	XT_View = (fptr_XT_View)GetProcAddress(Hdl, "XT_View");
	XT_ReleaseMem = (fptr_XT_ReleaseMem)GetProcAddress(Hdl, "XT_ReleaseMem");
	XT_Prepare = (fptr_XT_Prepare)GetProcAddress(Hdl, "XT_Prepare");
	XT_Finalize = (fptr_XT_Finalize)GetProcAddress(Hdl, "XT_Finalize");
	XT_ProcessItemEx = (fptr_XT_ProcessItemEx)GetProcAddress(Hdl, "XT_ProcessItemEx");

	return 1;
}

int wmain(int argc, wchar_t *argv[])
{
		if (argc < 2) {
		cout << "Usage: test.exe filename type\n";
		return 0;
	}

	filename = argv[1];
	itemtype = argv[2];
	
	path.resize(MAX_PATH);
	path.resize(GetModuleFileNameW(NULL, &path[0], (DWORD)path.size()));
	path = path.substr(0, path.find_last_of(L"/\\"));

	INT64 size;
	LONG lret;
	PVOID pret;
	BOOL bret;

	RetrieveFunctionPointers();

	LicenseInfo licence = { sizeof(LicenseInfo),0x31,1,{0,0},{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}};
	lret = XT_Init(18, XT_INIT_XWF, NULL, &licence);
	assert(("XT_Init did not return 1", lret == 1));

	lret = XT_About(NULL,NULL);
	assert(("XT_About did not return 1", lret == 0));

	pret = XT_View(handle, item, volume, evidence, NULL, &size);
	wcout << L"XT_View returned 0x" << hex << uppercase << pret << L" with size=" << dec << (int64_t)size << L"\n";
	assert(("XT_View returned a null pointer but size > 0", pret != 0 || (int64_t)size<0));

	if (pret) {
		ofstream viewfile(path + L"\\" + filename + L"_view.htm");
		viewfile << (char*)pret;
		bret = XT_ReleaseMem(pret);
	}

	lret = XT_Prepare(volume, evidence, XT_ACTION_RVS, 0);
	wcout << L"XT_Prepare (RVS) returned " << lret << L"\n";
	assert(("XT_Prepare (RVS) returned a negative number", lret >= 0));

	lret = XT_ProcessItemEx(item, handle, 0);
	assert(("XT_ProcessItemEx did not return 0", lret == 0));

	lret = XT_Finalize(handle, evidence, XT_ACTION_RVS, 0);
	wcout << L"XT_Finalise (RVS) returned " << lret << L"\n";
	assert(("XT_Finalise (RVS) did not return 0 or 1", lret == 0 || lret == 1));

	lret = XT_Prepare(volume, evidence, XT_ACTION_DBC, 0);
	wcout << L"XT_Prepare (DBC) returned " << lret << L"\n";
	assert(("XT_Prepare (DBC) returned a negative number", lret >= 0));

	lret = XT_ProcessItemEx(item, handle, 0);
	assert(("XT_ProcessItemEx did not return 0", lret == 0));

	lret = XT_Finalize(handle, evidence, XT_ACTION_DBC, 0);
	wcout << L"XT_Finalise (DBC) returned " << lret << L"\n";
	assert(("XT_Finalise (DBC) did not return 0 or 1", lret == 0 || lret == 1));

	lret = XT_Done(NULL);
	assert(("XT_Done did not return 0", lret == 0));

	return 0;
}

XT_API DWORD XWF_Read(HANDLE hVolumeOrItem, INT64 Offset, uint8_t* lpBuffer, DWORD nNumberOfBytesToRead)
{
	assert(("XWF_Read called with unexpected handle", hVolumeOrItem == handle));
	assert(("XWF_Read called with NULL lpBuffer", lpBuffer != NULL));
	ifstream file(path + L"\\" + filename, ios::binary);
	file.seekg(Offset);
	file.read((char*)lpBuffer, nNumberOfBytesToRead);
	return (DWORD)file.gcount();
}

XT_API const wchar_t* XWF_GetItemName(LONG nItemID)
{
	assert(("XWF_GetItemName called with unexpected item id", nItemID == item));
	return filename.c_str();
}

XT_API INT64 XWF_GetCaseProp(LPVOID pReserved, LONG nPropType, PVOID pBuffer, LONG nBufSize)
{
	assert(("XWF_GetCaseProp pReserved not NULL", pReserved == NULL));
	assert(("XWF_GetCaseProp called with NULL pBuffer", pBuffer != NULL));
	assert(("XWF_GetCaseProp called with nBufSize <= 0", nBufSize > 0));
	wstring file;
	switch (nPropType) {
	case XWF_CASEPROP_TITLE:
		wcscpy_s((wchar_t*)pBuffer, nBufSize, L"Test Case");
		return 10;
	case XWF_CASEPROP_EXAMINER:
		wcscpy_s((wchar_t*)pBuffer, nBufSize, L"John User");
		return 10;
	case XWF_CASEPROP_FILE:
		file = (path + L"Test.xfc");
		wcscpy_s((wchar_t*)pBuffer, nBufSize, file.c_str());
		return file.length()+1;
	case XWF_CASEPROP_DIR:
		wcscpy_s((wchar_t*)pBuffer, nBufSize, path.c_str());
		return path.length()+1;
	}
	assert(("XWF_GetCaseProp called with bad property type (nPropType)", 0));
	return -1;
}

XT_API INT64 XWF_GetSize(HANDLE hVolumeOrItem, BYTE* lpOptional)
{
	assert(("XWF_GetSize called with unexpected handle", hVolumeOrItem == handle));
	assert(("XWF_GetSize called with unexpected lpOptional (!= 0, 1 or 2)", lpOptional == (BYTE*)0 || lpOptional == (BYTE*)1 || lpOptional == (BYTE*)2));
	struct _stat stat_buf;
	int rc = _wstat((path + L"\\" + filename).c_str(), &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}

XT_API DWORD XWF_GetItemType(LONG nItemID, wchar_t*lpTypeDescr, LONG nBufferLen)
{
	assert(("XWF_GetItemType called with unexpected item id", nItemID == item));
	assert(("XWF_GetItemType called with NULL lpTypeDescr", lpTypeDescr != NULL));
	assert(("XWF_GetItemType called with nBufferLen <=0", nBufferLen > 0));
	wcscpy_s((wchar_t*)lpTypeDescr, nBufferLen, itemtype.c_str());
	return 3;
}

XT_API void XWF_OutputMessage(const wchar_t* lpMessage, DWORD nFlags)
{
	assert(("XWF_OutputMessage called with NULL lpMessage", lpMessage != NULL));
	if (nFlags & 0x04) cout << (char*)lpMessage << "\n";
	else wcout << lpMessage << "\n";
}

XT_API long XWF_CreateFile(wchar_t* lpName, DWORD flags, LONG nParentItemID, PVOID pSourceInfo)
{
	assert(("XWF_CreateFile called with NULL lpName", lpName != NULL));
	assert(("XWF_CreateFile called with unexpected flags (!= 0x04)", flags == 0x04));
	assert(("XWF_CreateFile called with unexpected nParentItemID", nParentItemID == item));
	assert(("XWF_CreateFile called with NULL pSourceInfo", pSourceInfo != NULL));
	ifstream file((wchar_t*)pSourceInfo);
	if (file.fail()) return -1;
	else return 0x88;
}

XT_API void XWF_GetVolumeName(HANDLE hVolume, wchar_t* lpString, DWORD nType) { assert(("XWF_GetVolumeName not implemented",0)); }
XT_API void XWF_GetVolumeInformation(HANDLE hVolume, LPLONG lpFileSystem, DWORD* nBytesPerSector, DWORD* nSectorsPerCluster, INT64* nClusterCount, INT64* nFirstClusterSectorNo) { assert(("XWF_GetVolumeInformation not implemented", 0)); }
XT_API BOOL XWF_GetSectorContents(HANDLE hVolume, INT64 nSectorNo, wchar_t* lpDescr, LPLONG lpItemID) { assert(("XWF_GetSectorContents not implemented", 0)); return 0; }
XT_API void XWF_SelectVolumeSnapshot(HANDLE hVolume) { assert(("XWF_SelectVolumeSnapshot not implemented", 0)); };
XT_API DWORD XWF_GetItemCount(HANDLE hVolume) { assert(("XWF_GetItemCount not implemented", 0)); return 0; }
XT_API long int XWF_CreateItem(wchar_t* lpName, DWORD flags) { assert(("XWF_CreateItem not implemented", 0)); return 0; }
XT_API INT64 XWF_GetItemSize(LONG nItemID) { assert(("XWF_GetItemSize not implemented", 0)); return 0; }
XT_API void XWF_SetItemSize(LONG nItemID, INT64 size) { assert(("XWF_SetItemSize not implemented", 0)); }
XT_API void XWF_GetItemOfs(LONG nItemID, INT64* lpDefOfs,INT64* lpStartSector) { assert(("XWF_GetItemOfs not implemented", 0)); }
XT_API void XWF_SetItemOfs(LONG nItemID, INT64 nDefOfs, INT64 nStartSector) { assert(("XWF_SetItemOfs not implemented", 0)); }
XT_API INT64 XWF_GetItemInformation(LONG nItemID,LONG InfoType, LPBOOL lpSuccess) { assert(("XWF_GetItemInformation not implemented", 0)); return 0; }
XT_API BOOL XWF_SetItemInformation(LONG nItemID,LONG InfoType, INT64 nInfoValue) { assert(("XWF_SetItemInformation not implemented", 0)); return 0; }
XT_API void XWF_SetItemType(LONG nItemID, wchar_t*lpTypeDescr,LONG nTypeStatus) { assert(("XWF_SetItemType not implemented", 0)); }
XT_API LONG XWF_GetItemParent(LONG nItemID) { assert(("XWF_GetItemParent not implemented", 0)); return 0; }
XT_API void XWF_SetItemParent(LONG nChildItemID, LONG nParentItemID) { assert(("XWF_SetItemParent not implemented", 0)); }
XT_API LONG XWF_GetReportTableAssocs(LONG nItemID,wchar_t* lpBuffer, LONG nBufferLen) { assert(("XWF_GetReportTableAssocs not implemented", 0)); return 0; }
XT_API LONG XWF_AddToReportTable(LONG nItemID,wchar_t* lpReportTableName, DWORD nFlags) { assert(("XWF_AddToReportTable not implemented", 0)); return 0; }
XT_API wchar_t* XWF_GetComment(LONG nItemID) { assert(("XWF_GetComment not implemented", 0)); return 0; }
XT_API BOOL XWF_AddComment(LONG nItemID, wchar_t* lpComment,DWORD nFlags) { assert(("XWF_AddComment not implemented", 0)); return 0; }
XT_API void XWF_ShowProgress(wchar_t* lpCaption, DWORD nFlags) { assert(("XWF_ShowProgress not implemented", 0)); }
XT_API void XWF_SetProgressPercentage(DWORD nPercent) { assert(("XWF_SetProgressPercentage not implemented", 0)); }
XT_API void XWF_SetProgressDescription(wchar_t* lpStr) { assert(("XWF_SetProgressDescription not implemented", 0)); }
XT_API BOOL XWF_ShouldStop(void) { assert(("XWF_ShouldStop not implemented", 0)); return 0; }
XT_API void XWF_HideProgress(void) { assert(("XWF_HideProgress not implemented", 0)); }
XT_API HANDLE XWF_OpenItem(HANDLE hVolume,LONG nItemID, DWORD nFlags) { assert(("XWF_OpenItem not implemented", 0)); return 0; }
XT_API void XWF_Close(HANDLE hVolumeOrItem) { assert(("XWF_Close not implemented", 0)); }
XT_API HANDLE XWF_CreateEvObj(DWORD nType, LONG nDiskID, LPWSTR lpPath, PVOID pReserved) { assert(("XWF_CreateEvObj not implemented", 0)); return 0; }
XT_API INT64 XWF_GetVSProp(LONG nPropType, PVOID pBuffer) { assert(("XWF_GetVSProp not implemented", 0)); return 0; }
XT_API LONG XWF_Search(SearchInfo* SInfo, CodePages* CPages) { assert(("XWF_Search not implemented", 0)); return 0; }
XT_API HANDLE XWF_CreateContainer(LPWSTR lpFileName,DWORD nFlags, LPVOID pReserved) { assert(("XWF_CreateContainer not implemented", 0)); return 0; }
XT_API LONG XWF_CopyToContainer(HANDLE hContainer, HANDLE hItem, DWORD nFlags, DWORD nMode, INT64 nStartOfs, INT64 nEndOfs, LPVOID pReserved) { assert(("XWF_CopyToContainer not implemented", 0)); return 0; }
XT_API LONG XWF_CloseContainer(HANDLE hContainer, LPVOID pReserved) { assert(("XWF_CloseContainer not implemented", 0)); return 0; }
XT_API BOOL XWF_GetBlock(HANDLE hVolume, INT64* lpStartOfs, INT64* lpEndOfs) { assert(("XWF_GetBlock not implemented", 0)); return 0; }
XT_API BOOL XWF_SetBlock(HANDLE hVolume, INT64 nStartOfs, INT64 nEndOfs) { assert(("XWF_SetBlock not implemented", 0)); return 0; }
XT_API HANDLE XWF_GetFirstEvObj(LPVOID pReserved) { assert(("XWF_GetFirstEvObj not implemented", 0)); return 0; }
XT_API HANDLE XWF_GetNextEvObj(HANDLE hPrevEvidence, LPVOID pReserved) { assert(("XWF_GetNextEvObj not implemented", 0)); return 0; }
XT_API HANDLE XWF_OpenEvObj(HANDLE hEvidence, DWORD nFlags) { assert(("XWF_OpenEvObj not implemented", 0)); return 0; }
XT_API VOID XWF_CloseEvObj(HANDLE hEvidence) { assert(("XWF_CloseEvObj not implemented", 0)); }
XT_API INT64 XWF_GetEvObjProp(HANDLE hEvidence, DWORD nPropType, PVOID pBuffer) { assert(("XWF_GetEvObjProp not implemented", 0)); return 0; }
XT_API LPWSTR XWF_GetExtractedMetadata(LONG nItemID) { assert(("XWF_GetExtractedMetadata not implemented", 0)); return 0; }
XT_API LPWSTR XWF_GetMetadata(LONG nItemID, HANDLE hItem) { assert(("XWF_GetMetadata not implemented", 0)); return 0; }
XT_API BOOL XWF_AddExtractedMetadata(LONG nItemID, LPWSTR lpComment, DWORD nFlagsHowToAdd) { assert(("XWF_AddExtractedMetadata not implemented", 0)); return 0; }
XT_API BOOL XWF_GetHashValue(LONG nItemID, LPVOID lpBuffer) { assert(("XWF_GetHashValue not implemented", 0)); return 0; }
XT_API LONG XWF_AddEvent(struct EventInfo* Evt) { assert(("XWF_AddEvent not implemented", 0)); return 0; }
XT_API LPVOID XWF_GetReportTableInfo(LPVOID pReserved, LONG nReportTableID, PLONG lpOptional) { assert(("XWF_GetReportTableInfo not implemented", 0)); return 0; }
XT_API LPVOID XWF_GetEvObjReportTableAssocs(HANDLE hEvidence, LONG nFlags, PLONG lpValue) { assert(("XWF_GetEvObjReportTableAssocs not implemented", 0)); return 0; }
